#!/bin/sh

# Adapted from https://toedter.com/2018/06/02/heroku-docker-deployment-update/
# Modified by Leo Blondel
# Last edit 03/24/2020

# Variables that are set in settings are:
# - ALGOLIA_APP_ID=YOUR_APP_ID
# - ALGOLIA_API_TOKEN=YOUR_APP_TOKEN
# - BACKEND_URL=https://url.of.the./backend
# - FRONTEND_URL=https://url.of.the./frontend

# First we build the docker image, using the ENV var set by the environment variables
apk add python python-dev py-pip build-base libffi-dev openssl-dev
pip install docker-compose
docker-compose build
docker tag web registry.heroku.com/$APP/web

#Then we login to Heroku
echo $HEROKU_TOKEN | docker login -u _ --password-stdin registry.heroku.com

# And push the new docker image to the heroku registry
docker push registry.heroku.com/$APP/web

apk add --no-cache curl

# Finally we trigger the deployement of the new docker image on Heroku
imageId=$(docker inspect registry.heroku.com/$APP/web --format={{.Id}})
payload='{"updates":[{"type":"web","docker_image":"'"$imageId"'"}]}'

curl -n -X PATCH https://api.heroku.com/apps/$APP/formation \
-d "$payload" \
-H "Content-Type: application/json" \
-H "Accept: application/vnd.heroku+json; version=3.docker-releases" \
-H "Authorization: Bearer $HEROKU_TOKEN"
