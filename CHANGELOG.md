Please view this file on the master branch, on stable branches it's out of date.

### 2020-03-30
- New: New search page, that will replace projects/challenges/users/groups pages
- New: You can now decide to receive mail notification or not
- Tweak: filter now show sdgs cards instead of sdg name
- Tweak: Search page improvements (horizontal scrollable nav bar)
- Tweak: Search page style improvements for mobile
- Tweak: on search page, added "no result" in case of no result
- Tweak: now get need members from /members api, instead of from the single need api
- Tweak: new placeholdder for search page search bar
- Tweak: add translations for show more filters button
- Fix: members add modal not searching users
- Fix: mentions not working on post and quill editor
- Fix: not able to add skills to objects anymore
- Fix: skills tag spaging issue

### 2020-03-26
- New: Now can add hooks on projects that trigger on certain events
- Tweak: Added loading indicator for members/participants modal
- Tweak: Add loading indicator to single post and need
- Tweak: always use/redirect program and challenge links to human readable url instead of id
- Tweak: use small banner url when calling for small banner image
- Tweak: Bigger logo url for post/need/comment creator
- Fix: now have right object name when sharing an object through mail

### 2020-03-23
- New: added german language!
- New: added participants modal in program header
- Tweak: optimize loading time, by getting objets members/participannts only once the page has fully loaded
- Tweak: improve user header and needs card 
- Tweak: harmonize challenge & program headers
- Tweak: allow admins to delete posts of their objects (posts/needs..)
- Tweak: improve design for needs cards and their single page
- Tweak: remove rules read confirmation when submitting project to a challenge
- Tweak: more space for needs on mobile
- Tweak: add link to project on single need
- Fix: some links overflowing on needs description and post/comment input field

### 2020-03-18
- New: page to see a single need
- New: added share buttons to need object
- New: add featured program card on homepage
- Tweak: add needs & projects tab on challenge and program pages
- Tweak: added share button on single project, group, challenge, program & need card
- Tweak: different design depending on where the need comes from
- Tweak: new dropdown menu for need: to edit, delete & copy its link
- Tweak: Better share buttons modal position
- Tweak: use small profile logo url when we don't need good quality
- Fix: get project clap state in projects page
- Fix: hide load button if feed has less than 5 posts

### 2020-03-16
- New: projects page with ability to search and filter easily
- Tweak: improve image size management for projects/communities banner images
- Tweak: Code improvements/harmonization
- Fix: bug when contacting someone from a popup (dekstop only)
- Fix: inviting members to object not working

### 2020-03-06
- New: Show meta data of first link of a post
- New: Ability to copy any post link
- New: New page to see a single post
- New: Ability to report posts & comments
- New: Now can remove user avatar or banner to reset it to default
- Tweak: Hide nb of users on people page + bigger font size for filters
- Tweak: Force to show all sdgs on complete profile page
- Tweak: Add icons to user menu dropdown
- Tweak: Added confirmation message on banner/avatar removal
- Tweak: in members modal from projects/group/challenges pages, separate members into tabs depending on their type
- Tweak: default hits for searchpage is now 15
- Tweak: Rename all search index urls to plural (ex: ?i=Project -> ?i=Projects)
- Fix: alignments & translation fixes
- Fix: Clear filters not working on mobile

### 2020-02-17
- New: Mentioning now display name/title instead of short name/title
- Tweak: Different design for mention when displaying or editing
- Tweak: Added a confirmation message box on the signin page, when validating your account via mail
- Tweak: Code structure: reorder and remove some files/folders
- Tweak: Apply new mention system to quill js editor
- Tweak: Update changelog
- Fix: Remove margin bottom on all authentification pages (to make it full height)

### 2020-01-24
- New: added ability to mention users and projects within QuillJS editor
- Tweak: improve mentioning on posts and comments with a list displaying suggested user/project image, name and nickname/shortname
- Tweak: new style for header searchbar results, now display user/project image
- Fix: image size now saving correctly on quill js editor

### 2020-01-14
- New: Users page, blazing fast, with search bar, filters and pagination
- New: Infinite loading in projects & groups page + in all feeds
- New: Modal of people who clapped a post
- Tweak: Improved design for list of users in modals (followers, following, members..)
- Tweak: Now load 12 items at a time (page projects & groups)
- Tweak: Separate objects into tabs, inside user following modal (https://gitlab.com/JOGL/JOGL/issues/247)
- Tweak: Improved profile page header on mobiles
- Fix: Post content inside () being deleted on submit
- Tweak: Sticky nav bar to the top, on user profile page
- Tweak: Show only 8 sdgs by default on an object edition (and can toggle to view more/mess)

### 2019-12-07
- New: Refreshed design for the "need" object
- New: added 12 new default profile images
- New: on all feeds : get only a few elements from the api & add a load more button to view more
- Tweak: change emails "from name" to "JOGL - Just One Giant Lab" (instead of JOGL) + "from email" to "noreply@" (instead of hello@)
- Tweak: changed max size for banners and profile image from 10Mo to 4Mo
- Tweak: allow only png/jpg as banner or profile images
- Tweak: remove unnecessary objects from most api calls (speed improvement)
- Tweak: now order projects/groups/needs in the backend (so no need to filter them on front anymore)
- Tweak: added pagination in consequence (to get 5 most recent projets/groups.. for ex)
- Fix: sort comments by id, so have most recent on top
- Fix: fixed list component (ul/li) not working in quill js editor

### 2019-12-02
- New: Add notification when someone posts on your challenge or program
- New: Added more field on the project object (to guide you detailing it even more if you want)
- Tweak: All quill js links will now open on new window (https://gitlab.com/JOGL/JOGL/issues/158)
- Tweak: on all about pages, detect links outside of <a> tag and linkify them
- Tweak: improve user profile header’s style on really small devices
- Tweak: don't trigger email if you commented on your profile
- Tweak: don't trigger email if you comment on a post in which you are mentioned
- Tweak: now redirect to correct tabs depending on hash of a profile page
- Tweak: in emails, change wording community to group
- Tweak: replace due date by 'urgent' in the need
- Tweak: in the confirmation email, add full confirmation link , if the button doesn’t work
- Tweak: change email content if user mentioned someone in a post or pot’s comment on his profile
- Tweak: don't trigger email if you posted or commented in an object you are admin/owner
- Tweak: change email content when someone you follow post in their own profile (else they are posting in other object you follow)
- Fix: Home feed not working if you followed deleted users/projects..
- Fix: bug when switching to another tab in an object edition page

### 2019-11-21
- New: Added needs in menu and display latest needs on homepage
- New: tabs on mobile homepage, to switch between the "feed" and "what's new" sections
- Tweak: specify on a project header if it's participating to a challenge
- Tweak: open upload to all file types!
- Tweak: Make user bio go full width if user is not connected (on desktop)
- Tweak: add '...' if text is longer than 2 lines (on members cards, and on projects/groups small cards on homepage)
- Tweak: Style improvements for really small devices (header menu + homepage)
- Fix: On the 'someone followed you' mail, redirect the button to the person who followed you, not you.

### 2019-11-19
- New: New notification emails: when someone follows you, when someone post in an object you follow (user, project, challenge..).
- New: homepage (when connected) loads only 10 posts and you can load more with button (improve performance)
- Tweak: Speed improvements through many pages
- Tweak: now specify which user mentioned or posted about you in notification emails.
- Tweak: Design enchancements on needs page
- Tweak: view all user's skills when clicking on the "+.." on the user's profile header
- Tweak: better display documents on feed posts
- Fix: signin modal not closing when clicking the button
- Fix: language switcher not displaying selected language

### 2019-11-18
- New: You can now contact users on the platform! (if they chose to be publicly contacted)
- New: Brand new notification emails design!
- New: New design when listing members on the platform (people page, member modal popups..) + can contact users from modals as well.
- New: added 8 default profile images that are chosen randomly on signup (if you don't upload a profile picture)
- New: Now trigger a "please sign in" modal on any 401 error (except on sign in page)
- New: Add a page with the needs from all the projects (/needs)
- New: Now can edit and add documents to an old post
- New: Now can delete documents on your project/challenge
- New: New people page that shows only 100 users at a time and have a "load more button"
- Tweak: Those pages now load much faster:  projects, challenges, communities and homepage.
- Tweak: Revised wording, subject, and content for all email notifications
- Tweak: Dynamic hash in url that goes to matching tab, and update when you select another tab
- Tweak: Added smooth scrolling when jumping to other tab, sections..
- Tweak: More precise error messages on signin + display button to resend confirmation link if you didn’t validate your account
- Tweak: Improve the modal "Add Project"
- Tweak: Automatically follow a need when creating it
- Fix: On the homepage feed, posts from « needs » object now redirect to the need’s tab section of its project page
- Fix: various fixes on the feed
- Fix: various minor fixes

### 2019-10-18
- New: Add changelog
- New: User can now download their data on the settings page
- New: new design with fewer and sticky tabs for normal and edition pages of the following object: project, user, groups, programs & challenges
- New: Dynamic no result message
- New: Remove followers and members tab (project, groups, users), now accessible by clicking the numbers in header
- Tweak: only display city and country for address
- Tweak: revised settings page design
- Tweak: code optimization
- Tweak: better design for document cards
- Tweak: now anyone can post or comment on a need
- Tweak: hide documents tab is user is not admin and they are no docs
- Tweak: added needs in the project edition page
- Tweak: anyone can comment a need (no need to click I'll help)
- Tweak: Better wording for need object and other places
- Tweak: Display number of posts and comments on the need card
- Tweak: Display confirmation message on member role change

### 2019-10-11 ([MR 167](https://gitlab.com/JOGL/frontend-v0.1/merge_requests/167))
- New: some fields for challenges and program now have french translation
- New: you can now archive or delete a project  (https://gitlab.com/JOGL/JOGL/issues/195))
- New: insert emoji directly within our text editor  (https://gitlab.com/JOGL/JOGL/issues/200)
- Tweak: show need count on projects cards
- Tweak: added a button to show/hide some fields in the edition pages
- Tweak: don’t show draft challenges anywhere, show them only when published  (https://gitlab.com/JOGL/JOGL/issues/226)
- Tweak: code optimisation and cleaning
- Tweak: show more results in search bar and better readability
- Tweak: add gitlab link to contribute to the code
- Tweak: add MIT license to the code  (https://gitlab.com/JOGL/JOGL/issues/241)
- Fix: problem that prevent changing role to owner in french langage
- Fix: pending users not working + unability to join projects (err 403)
- Fix: minor problems


### 2019-09-12 ([MR 156](https://gitlab.com/JOGL/frontend-v0.1/merge_requests/156))
- New: introducing the searchbar on JOGL (searching into skills, names, descriptions... of projects and people) 🔎  (https://gitlab.com/JOGL/JOGL/issues/39)
- New: you can now filter people, projects and groups by alphabetical order, by dates or by popularity
- New: Need object  (https://gitlab.com/JOGL/JOGL/issues/64) / (https://gitlab.com/JOGL/JOGL/issues/65) / (https://gitlab.com/JOGL/JOGL/issues/67)
- Tweak: add short bio to user profile
- Tweak: new header for challenges
- Tweak: participate dropdown on challenge header
- Tweak: better navigation on mobile devices
- Tweak: new design for search bar and mobile dropdown menu
- tweak: add link to create project & link to rules on add project modal
- Fix: some security issues
- Fix: minor problems

### 2019-08-19
- New: Add a chatbot!
- New: ability to drag and drop + copy/paste + resize images in the quill text editor
- Tweak: Add translations + revised the pages: data, terms/conditions, ethic/pledge.
- Tweak: save the page you were before signing in
- Fix security issues
- Fix: minor improvements

### 2019-07-31
- New: add language dropdown on header + help link on user menu
- Tweak: show comments by default and add button to show more than 4
- Tweak: translation ethic pledge page
- Tweak: little design enhancement of profile page
- translate home intro + enhance style for mobile
- Fix: mentions that made the whole text a link

### 2019-07-24
- Tweak: improve design of creation and edition page (responsive)
- Tweak: some pages have now dynamic tab titles
- Tweak: all website now translated
- Tweak: at some places on website, show only few skills and interests, and add a button to view more
