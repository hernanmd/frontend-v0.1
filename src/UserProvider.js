import React, { createContext, Component } from "react";
import Api from "./Api";

export const UserContext = createContext({
  checkUserTokens: () => {},
  connected: false,
  logout: () => {},
  signIn: () => {},
  user: undefined,
});

export default class UserProvider extends Component {
  state = {
    checkUserTokens: () => this.checkUserTokens(),
    connected: false,
    logout: () => this.logout(),
    signIn: (email, password) => this.signIn(email, password),
    user: undefined,
  };

  checkUserTokens() {
    return new Promise((resolve, reject) => {
      //Control if user is connected and always available
      if (localStorage.getItem("access-token")) {
        Api.get("/api/users/validate_token")
          .then((res) => {
            // console.log("Validating tokens");
            if (res.status === 401) {
              // console.log("Tokens are not valid");
              this.logout();
              reject("Tokens are not valid");
            }
            if (res.status === 200) {
              // console.log("Valid tokens !");
              this.setState({ connected: true, user: res.data.data });
              // console.log("Connected user: ", res.data.data );
              resolve();
            }
          })
          .catch((error) => {
            // console.log("UserService error (checkUserTokens) : ", error);
            reject(error);
          });
      } else {
        reject();
      }
    });
  }

  frontLogout() {
    // console.log("Delete localStorage");
    localStorage.removeItem("access-token");
    localStorage.removeItem("client");
    localStorage.removeItem("uid");
    localStorage.removeItem("userId");
    // console.log("Delete user connected");
    this.setState({ connected: false, user: undefined, error: undefined });
  }

  /*   getUser(){
    return new Promise((resolve, reject) => {
      const userId = localStorage.getItem("userId");
      Api.get("/api/users/" + userId)
      .then(res=>{
        console.log("User got : ", res.data);
        this.setState({user: res.data});
        resolve(res.data);
      })
      .catch(error=>{
        console.log("UserService error (getUser) : ", error);
        reject(error);
      })
    });
  } */

  /*   isConnected(){
    console.log("Start isConnected");
    if( localStorage.getItem("access-token") &&
        localStorage.getItem('uid') &&
        localStorage.getItem('client') ){
      this.setState({connected: true});
      return true;
    } else {
      this.frontLogout();
      this.setState({connected: false});
      return false;
    }
  } */

  logout() {
    // console.log("Start logout");
    Api.delete("/api/users/sign_out")
      .then((res) => {
        // console.log("Logout completed : ", res);
        this.frontLogout();
        // console.log("Removed tokens and deauth!");
      })
      .catch((error) => {
        if (error.response.status === 404) {
          this.frontLogout();
          // console.log("Expired or wrong token removed and deAuth");
        } else {
          // console.log("UserService error (logout) : ", error.response);
        }
      });
  }

  signIn(email, password) {
    return new Promise((resolve, reject) => {
      // console.log("Start login");
      Api.post("/api/users/sign_in", { email, password })
        .then((res) => {
          localStorage.setItem("access-token", res.headers[Object.keys(res.headers)[0]]);
          localStorage.setItem("uid", res.headers.uid);
          localStorage.setItem("client", res.headers.client);
          localStorage.setItem("userId", res.data.data.id);
          this.setState({
            connected: true,
            user: res.data.data,
          });
          resolve();
        })
        .catch((error) => {
          const { response: { data: { errors = [] } = {} } = {} } = error;
          // console.error(errors);
          this.setState({ error: errors[0] });
          reject(errors);
        });
    });
  }

  render() {
    return <UserContext.Provider value={this.state}>{this.props.children}</UserContext.Provider>;
  }
}
