import axios from "axios";
import $ from "jquery";

var url = process.env.REACT_APP_ADDRESS_BACK;
// var url = "https://jogl-backend.herokuapp.com/";
// var url = "http://localhost:3001"

const api = axios.create({
  //  baseURL: process.env.BACKEND_URL,
  baseURL: url,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json"
  }
});

api.interceptors.response.use(
  response => response,
  error => {
    const { status } = error.response;
    if (status === 401) {
      localStorage.removeItem("access-token");
      localStorage.removeItem("client");
      localStorage.removeItem("uid");
      localStorage.removeItem("userId");
      if (document.location.pathname != "/signin") {
        // if 401 error on all pages except signin
        $("#signInModal").modal("show"); // trigger signinModal
      }
    }
    return Promise.reject(error);
  }
);

api.interceptors.request.use(
  config => {
    if (localStorage.getItem("access-token")) {
      const access_token = localStorage.getItem("access-token");
      const client = localStorage.getItem("client");
      const uid = localStorage.getItem("uid");

      if (access_token) {
        config.headers.common["access-token"] = access_token;
        config.headers.common["client"] = client;
        config.headers.common["uid"] = uid;
      }
    }
    return config;
  },
  error => Promise.reject(error)
);

export default api;
