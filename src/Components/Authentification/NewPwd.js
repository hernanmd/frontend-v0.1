import React, { Component, Fragment } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import { Redirect } from "react-router-dom";
import Api from "Api";
import Alert from "Components/Tools/Alert.js";

class NewPwd extends Component {
  constructor(props) {
    super(props);
    this.state = {
      password: "",
      password_confirmation: "",
      error: "",
      fireRedirect: false,
      sending: false,
      success: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  checkPassword(pwd, pwd_confirm) {
    if (pwd !== pwd_confirm) {
      this.setState({ error: "err-4001" });
      return false;
    } else if (pwd.length < 8) {
      this.setState({ error: "err-4002" });
      return false;
    } else {
      return true;
    }
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value, error: "" });
  }

  handleSubmit(event) {
    event.preventDefault();
    const { password, password_confirmation } = this.state;
    const query = new URLSearchParams(this.props.location.search);

    const headers = {
      "access-token": query.get("access-token"),
      client: query.get("client"),
      uid: query.get("uid")
    };
    if (this.checkPassword(password, password_confirmation)) {
      this.setState({ error: "", sending: true });

      Api.put("/api/users/password", { password, password_confirmation }, { headers: headers })
        .then(() => {
          this.setState({ sending: false, success: true });
          setTimeout(() => {
            this.setState({ fireRedirect: true });
          }, 3500);
        })
        .catch(error => {
          // console.log(error);
          // console.log(error.response.data);
          const errors = error.response.data.errors[0];
          this.setState({ sending: false, error: errors });
        });
    }
  }

  render() {
    const { error, fireRedirect, sending, success } = this.state;
    const { intl } = this.props;
    var errorMessage = error.includes("err-") ? (
      <FormattedMessage id={error} defaultMessage="password not valid" />
    ) : (
      error
    );

    if (fireRedirect) {
      return <Redirect to="/signin" />;
    }

    var formStyle, title, msg;
    formStyle = { display: "block" };
    title = {
      text: "Password reset",
      id: "auth.newPwd.title"
    };
    msg = {
      text: "Please enter a new password.",
      id: "auth.newPwd.description"
    };
    return (
      <div className="form-content">
        <div className="form-header">
          <h2 className="form-title" id="signModalLabel">
            <FormattedMessage id={title.id} defaultMessage={title.text} />
          </h2>
          <p>
            <FormattedMessage id={msg.id} defaultMessage={msg.text} />
          </p>
        </div>
        <div className="form-body" style={formStyle}>
          <form onSubmit={this.handleSubmit} className="newPwdForm">
            <div className="form-group">
              <label className="form-check-label" htmlFor="password">
                <FormattedMessage id="auth.newPwd.pwd" defaultMessage="New password" />
              </label>
              <input
                type="password"
                name="password"
                id="password"
                className="form-control"
                placeholder={intl.formatMessage({ id: "auth.newPwd.pwd.placeholder" })}
                onChange={this.handleChange}
              />
            </div>
            <div className="form-group">
              <label className="form-check-label" htmlFor="password_confirmation">
                <FormattedMessage id="auth.newPwd.pwdConfirm" defaultMessage="New password confirm" />
              </label>
              <input
                type="password"
                name="password_confirmation"
                id="password_confirmation"
                className="form-control"
                placeholder={intl.formatMessage({ id: "auth.newPwd.pwd.placeholder" })}
                onChange={this.handleChange}
              />
            </div>
            {error !== "" && <Alert type="danger" message={errorMessage} />}
            {success && (
              <Alert
                type="success"
                message={<FormattedMessage id="info-4001" defaultMessage="New Password has been save !" />}
              />
            )}

            <button className="btn btn-primary btn-block" disabled={sending ? true : false}>
              {sending && (
                <Fragment>
                  <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                  <span className="sr-only">{intl.formatMessage({ id: "general.loading" })}</span>&nbsp;
                </Fragment>
              )}
              <FormattedMessage id="auth.newPwd.btnConfirm" defaultMessage="Confirm my new password" />
            </button>
          </form>
        </div>
      </div>
    );
  }
}
export default injectIntl(NewPwd);
