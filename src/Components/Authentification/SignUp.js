import React, { Component, Fragment } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import { Link, Redirect } from "react-router-dom";
import { toAlphaNum } from "Components/Tools/Nickname.js";
import { UserContext } from "UserProvider";
import Alert from "Components/Tools/Alert";
import Api from "Api";
/*** Validators ***/
import FormValidator from "Components/Tools/Forms/FormValidator";
import signUpFormRules from "./signUpFormRules";
/*** Images/Style ***/
import Envelope from "assets/img/envelope.svg";
import Logo from "assets/img/logo.svg";
import "./Auth.scss";

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      acceptConditions: false,
      acceptLegal: false,
      mail_newsletter: false,
      mail_weekly: false,
      first_name: "",
      last_name: "",
      nickname: "",
      email: this.props.location.email_value !== "" ? this.props.location.email_value : "",
      password: "",
      password_confirmation: "",

      error: "",
      fireRedirect: false,
      sending: false,
      success: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  validator = new FormValidator(signUpFormRules);

  checkPassword(pwd, pwd_confirm) {
    if (pwd !== pwd_confirm) {
      this.setState({ valid_password_confirmation: { isInvalid: "is-invalid", message: "err-4001" } });
      return false;
    } else {
      return true;
    }
  }

  generateNickname(first_name, last_name) {
    var proposalNickname = first_name.trim() + last_name.trim();
    // console.log("proposal nickname : ", proposalNickname);
    proposalNickname = toAlphaNum(proposalNickname);
    this.setState({ nickname: proposalNickname });
    return proposalNickname;
  }

  handleChange(e) {
    var { checked, name, value } = e.target;
    var proposalNickname = undefined;
    if (name === "first_name") {
      proposalNickname = this.generateNickname(value, this.state.last_name);
    } else if (name === "last_name") {
      proposalNickname = this.generateNickname(this.state.first_name, value);
    } else if (name === "nickname") {
      value = toAlphaNum(value);
    } else if (name === "acceptConditions") {
      value = checked;
    } else if (name === "mail_newsletter") {
      value = checked;
    } else if (name === "mail_weekly") {
      value = checked;
    } else if (name === "acceptLegal") {
      value = checked;
    }
    /* Validators start */
    const state = {};
    state[name] = value;

    //Check proposalNickname
    if (name === "first_name" || name === "last_name") {
      state["nickname"] = proposalNickname;
    }

    const validation = this.validator.validate(state);
    if (validation[name] !== undefined) {
      const stateValidation = {};
      stateValidation["valid_" + name] = validation[name];
      //Update nickname too only if firstname or nickname has been changed
      if (name === "first_name" || name === "last_name") {
        stateValidation["valid_nickname"] = validation["nickname"];
      }
      this.setState(stateValidation);
    }
    /* Validators end */
    this.setState({ [name]: value, error: "" });
  }

  handleSubmit(event) {
    event.preventDefault();
    const {
      acceptConditions,
      acceptLegal,
      first_name,
      last_name,
      nickname,
      email,
      password,
      password_confirmation,
      mail_weekly,
      mail_newsletter,
    } = this.state;
    var randomNumber = Math.floor(Math.random() * 12) + 1; // with 12 = max, and 1 = min
    // each static media has a code at the end of url, so making an array of those codes for images 1 to 12
    var imagesCodes = [
      "2e46be09",
      "639bc585",
      "ee2eb97c",
      "71d8e3fb",
      "008eb222",
      "ec11ec0a",
      "81c9a54a",
      "284d4d91",
      "ce49c345",
      "7692ba90",
      "748860b8",
      "0271ffd0",
    ];
    // make logo_url randomly equals to one of our default image (1 to 12)
    const logo_url = `/static/media/default-user-${randomNumber}.${imagesCodes[randomNumber - 1]}.png`;
    const user = {
      first_name,
      last_name,
      nickname,
      email,
      password,
      password_confirmation,
      acceptConditions,
      acceptLegal,
      mail_newsletter,
      mail_weekly,
      logo_url,
    };
    /* Validators control before submit */
    const validation = this.validator.validate(user);
    if (validation.isValid) {
      if (this.checkPassword(password, password_confirmation)) {
        this.setState({ error: "", sending: true });
        Api.post("/api/users/", { user })
          .then((res) => {
            this.setState({ sending: false });
            // console.log(res);
            // console.log(res.data);
            // console.log(res.data.nickname);
            if (res.data.nickname) {
              this.setState({ valid_nickname: { isInvalid: "is-invalid", message: "err-4005" } });
            } else {
              this.setState({ success: true });
            }
          })
          .catch((error) => {
            // console.log(error);
            // console.log(error.response.data);
            // console.log("errorMessage", error.response.data.error);
            const errorMessage = error.response.data.error;

            this.setState({ sending: false, error: errorMessage });
          });
      }
    } else {
      const stateValidation = {};
      Object.keys(validation).forEach((key) => {
        if (key !== "isValid") {
          stateValidation["valid_" + key] = validation[key];
        }
      });
      this.setState(stateValidation);
    }
  }

  render() {
    const {
      acceptConditions,
      acceptLegal,
      mail_newsletter,
      mail_weekly,
      email,
      error,
      fireRedirect,
      nickname,
      sending,
      success,
      valid_acceptConditions,
      valid_acceptLegal,
      valid_first_name,
      valid_last_name,
      valid_nickname,
      valid_email,
      valid_password,
      valid_password_confirmation,
    } = this.state;
    var errorMessage = error.includes("err-") ? (
      <FormattedMessage id={error} defaultMessage="An error has occured" />
    ) : (
      error
    );

    /* const btnDisabled = sending ? true : !this.checkFormIsComplete(); */
    let userContext = this.context;
    const { intl } = this.props;

    if (userContext.connected) {
      this.setState({ fireRedirect: true });
    }
    if (fireRedirect) {
      this.setState({ fireRedirect: false });
      return <Redirect to={"/"} />;
    }

    var formStyle, messageStyle, title, msg;
    if (!success) {
      formStyle = { display: "block" };
      messageStyle = { display: "none" };
      title = {
        text: "Welcome to JOGL",
        id: "signUp.title",
      };
      msg = {
        text: "Empowering humanity to solve global challenges.",
        id: "signUp.description",
      };
    } else {
      formStyle = { display: "none" };
      messageStyle = { display: "block" };
      title = {
        text: "Thanks for registering. Please check your emails.",
        id: "signup.confTitle",
      };
      msg = {
        text: "To complete your sign up, click the verification link in the confirmation email sent to you.",
        id: "signup.confMsg",
      };
    }

    return (
      <div className="auth-form row align-items-center">
        <div className="col-12 col-lg-5 leftPannel d-flex align-items-center justify-content-center">
          <Link to="/">
            <img src={Logo} className="logo" alt="JoGL icon" />
          </Link>
        </div>
        <div className="col-12 col-lg-7 rightPannel">
          <div className="form-content">
            <div className="form-header signup">
              <h2 className="form-title" id="signModalLabel">
                <FormattedMessage id={title.id} defaultMessage={title.text} />
              </h2>
              <p>
                <FormattedMessage id={msg.id} defaultMessage={msg.text} />
              </p>
            </div>

            <div className="form-body" style={formStyle}>
              <div className="goToSignUp signup">
                <span>
                  <FormattedMessage id="signUp.alreadyAccount" defaultMessage="Already have an account?" />
                </span>
                <span className="goToSignUp--signup">
                  <Link to="/signin" className="nav-link">
                    <FormattedMessage id="general.signIn" defaultMessage="Sign in" />
                  </Link>
                </span>
              </div>

              <form onSubmit={this.handleSubmit} className="row">
                <div className="form-group col-12 col-md-6">
                  <label className="form-check-label" htmlFor="email">
                    <FormattedMessage id="signUp.firstname" defaultMessage="First name" />
                  </label>
                  <div className="input-group">
                    <input
                      type="text"
                      name="first_name"
                      id="first_name"
                      className={
                        "form-control " +
                        (valid_first_name ? (!valid_first_name.isInvalid ? "is-valid" : "is-invalid") : "")
                      }
                      placeholder={intl.formatMessage({ id: "signUp.firstname.placeholder" })}
                      onChange={this.handleChange.bind(this)}
                    />
                    {valid_first_name ? (
                      valid_first_name.message !== "" ? (
                        <div className="invalid-feedback">
                          <FormattedMessage id={valid_first_name.message} defaultMessage="Value is not valid" />
                        </div>
                      ) : null
                    ) : null}
                  </div>
                </div>

                <div className="form-group col-12 col-md-6">
                  <label className="form-check-label" htmlFor="email">
                    <FormattedMessage id="signUp.lastname" defaultMessage="Last Name" />
                  </label>
                  <div className="input-group">
                    <input
                      type="text"
                      name="last_name"
                      id="last_name"
                      className={
                        "form-control " +
                        (valid_last_name ? (!valid_last_name.isInvalid ? "is-valid" : "is-invalid") : "")
                      }
                      placeholder={intl.formatMessage({ id: "signUp.lastname.placeholder" })}
                      onChange={this.handleChange.bind(this)}
                    />
                    {valid_last_name ? (
                      valid_last_name.message !== "" ? (
                        <div className="invalid-feedback">
                          <FormattedMessage id={valid_last_name.message} defaultMessage="Value is not valid" />
                        </div>
                      ) : null
                    ) : null}
                  </div>
                </div>

                <div className="form-group col-12 col-md-6">
                  <label className="form-check-label" htmlFor="email">
                    <FormattedMessage id="signUp.nickname" defaultMessage="Nickname" />
                  </label>
                  <div className="input-group">
                    <input
                      type="text"
                      name="nickname"
                      id="nickname"
                      className={
                        "form-control " +
                        (valid_nickname ? (!valid_nickname.isInvalid ? "is-valid" : "is-invalid") : "")
                      }
                      placeholder={intl.formatMessage({ id: "signUp.nickname.placeholder" })}
                      onChange={this.handleChange.bind(this)}
                      value={nickname}
                    />
                    {valid_nickname ? (
                      valid_nickname.message !== "" ? (
                        <div className="invalid-feedback">
                          <FormattedMessage id={valid_nickname.message} defaultMessage="Value is not valid" />
                        </div>
                      ) : null
                    ) : null}
                  </div>
                </div>

                <div className="form-group col-12 col-md-6">
                  <label className="form-check-label" htmlFor="email">
                    <FormattedMessage id="signIn.email" defaultMessage="Email" />
                  </label>
                  <div className="input-group">
                    <input
                      type="email"
                      name="email"
                      id="email"
                      value={email}
                      placeholder={intl.formatMessage({ id: "signIn.email.placeholder" })}
                      className={
                        "form-control " + (valid_email ? (!valid_email.isInvalid ? "is-valid" : "is-invalid") : "")
                      }
                      onChange={this.handleChange.bind(this)}
                    />
                    {valid_email ? (
                      valid_email.message !== "" ? (
                        <div className="invalid-feedback">
                          <FormattedMessage id={valid_email.message} defaultMessage="Value is not valid" />
                        </div>
                      ) : null
                    ) : null}
                  </div>
                </div>

                <div className="form-group col-12 col-md-6">
                  <label className="form-check-label" htmlFor="password">
                    <FormattedMessage id="signIn.pwd" defaultMessage="Password" />
                  </label>
                  <div className="input-group">
                    <input
                      type="password"
                      name="password"
                      id="password"
                      className={
                        "form-control " +
                        (valid_password ? (!valid_password.isInvalid ? "is-valid" : "is-invalid") : "")
                      }
                      placeholder={intl.formatMessage({ id: "signIn.pwd.placeholder" })}
                      onChange={this.handleChange.bind(this)}
                    />
                    {valid_password ? (
                      valid_password.message !== "" ? (
                        <div className="invalid-feedback">
                          <FormattedMessage id={valid_password.message} defaultMessage="Value is not valid" />
                        </div>
                      ) : null
                    ) : null}
                  </div>
                </div>

                <div className="form-group col-12 col-md-6">
                  <label className="form-check-label" htmlFor="password_confirmation">
                    <FormattedMessage id="signUp.pwdConfirm" defaultMessage="Password Confirmation" />
                  </label>
                  <div className="input-group">
                    <input
                      type="password"
                      name="password_confirmation"
                      id="password_confirmation"
                      className={
                        "form-control " +
                        (valid_password_confirmation
                          ? !valid_password_confirmation.isInvalid
                            ? "is-valid"
                            : "is-invalid"
                          : "")
                      }
                      placeholder={intl.formatMessage({ id: "signUp.pwdConfirm.placeholder" })}
                      onChange={this.handleChange.bind(this)}
                    />
                    {valid_password_confirmation ? (
                      valid_password_confirmation.message !== "" ? (
                        <div className="invalid-feedback">
                          <FormattedMessage
                            id={valid_password_confirmation.message}
                            defaultMessage="Value is not valid"
                          />
                        </div>
                      ) : null
                    ) : null}
                  </div>
                </div>

                {error !== "" && <Alert type="danger" message={errorMessage} />}

                <div className="form-check">
                  <div className="input-group">
                    <input
                      className="form-check-input"
                      id="mail_newsletter"
                      name="mail_newsletter"
                      onChange={this.handleChange.bind(this)}
                      type="checkbox"
                      checked={mail_newsletter}
                    />
                    <label className="form-check-label" htmlFor="mail_newsletter">
                      <FormattedMessage
                        id="signUp.mail_newsletter"
                        defaultMessage="Do you want to subscribe to the JOGL monthly newsletter?"
                      />
                    </label>
                  </div>
                </div>

                <div className="form-check">
                  <div className="input-group">
                    <input
                      className="form-check-input"
                      id="mail_weekly"
                      name="mail_weekly"
                      onChange={this.handleChange.bind(this)}
                      type="checkbox"
                      checked={mail_weekly}
                    />
                    <label className="form-check-label" htmlFor="mail_weekly">
                      <FormattedMessage
                        id="signUp.mail_weekly"
                        defaultMessage="Do you want to receive weekly updates of JOGL items you follow?"
                      />
                    </label>
                  </div>
                </div>

                <div className="form-check accept">
                  <div className="input-group">
                    <input
                      className={
                        "form-check-input " +
                        (valid_acceptConditions ? (!valid_acceptConditions.isInvalid ? "" : "is-invalid") : "")
                      }
                      id="acceptConditions"
                      name="acceptConditions"
                      onChange={this.handleChange.bind(this)}
                      type="checkbox"
                      ischecked={acceptConditions.toString()}
                    />
                    <label className="form-check-label d-inline" htmlFor="acceptConditions">
                      <FormattedMessage
                        id="signUp.termsOfService"
                        defaultMessage={`En créant votre compte, vous acceptez nos {termsOfService}, {dataPolicy} et notre {ethicsPledge}*`}
                        values={{
                          termsOfService: (
                            <a href="/terms" target="_blank" className="nav-link">
                              <FormattedMessage id="singUp.termsOfServiceLinkText" defaultMessage="terms of services" />
                            </a>
                          ),
                          dataPolicy: (
                            <a href="/data" target="_blank" className="nav-link">
                              <FormattedMessage id="footer.data" defaultMessage="User data Policy" />
                            </a>
                          ),
                          ethicsPledge: (
                            <a href="/ethics-pledge" target="_blank" className="nav-link">
                              <FormattedMessage id="signUp.ethicsPledgeTextLink" defaultMessage="Ethics pledge" />
                            </a>
                          ),
                        }}
                      />
                    </label>
                    {valid_acceptConditions ? (
                      valid_acceptConditions.message !== "" ? (
                        <div className="invalid-feedback text-left">
                          <FormattedMessage id={valid_acceptConditions.message} defaultMessage="Value is not valid" />
                        </div>
                      ) : null
                    ) : null}
                  </div>
                </div>

                <div className="form-check accept">
                  <div className="input-group">
                    <input
                      className={
                        "form-check-input " +
                        (valid_acceptLegal ? (!valid_acceptLegal.isInvalid ? "" : "is-invalid") : "")
                      }
                      id="acceptLegal"
                      name="acceptLegal"
                      onChange={this.handleChange.bind(this)}
                      type="checkbox"
                      ischecked={acceptLegal.toString()}
                    />
                    <label className="form-check-label" htmlFor="acceptLegal">
                      <span>
                        <FormattedMessage id="signUp.legalConf" defaultMessage="I am legally major in my country" />
                      </span>
                    </label>
                    {valid_acceptLegal ? (
                      valid_acceptLegal.message !== "" ? (
                        <div className="invalid-feedback text-left">
                          <FormattedMessage id={valid_acceptLegal.message} defaultMessage="Value is not valid" />
                        </div>
                      ) : null
                    ) : null}
                  </div>
                </div>

                <button type="submit" className="btn btn-primary btn-block">
                  {sending && (
                    <Fragment>
                      <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                      <span className="sr-only">{intl.formatMessage({ id: "general.loading" })}</span>&nbsp;
                    </Fragment>
                  )}
                  <FormattedMessage id="signUp.btnCreate" defaultMessage="Create my JoGL account" />
                </button>
              </form>
            </div>
            <div className="form-message" style={messageStyle}>
              <img src={Envelope} alt="Message sent envelope" />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default injectIntl(SignUp);
SignUp.contextType = UserContext;
