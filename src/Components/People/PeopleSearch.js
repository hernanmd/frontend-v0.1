import React, { Component } from "react";
import { FormattedMessage } from "react-intl";

export default class PeopleSearch extends Component {
  render() {
    return (
      <div className="row justify-content-end peopleSearch">
        <div className="col-12 text-right">
          <button className="btn btn-sm btn-secondary btn-action">
            <FormattedMessage id="searchBar.mainBtn" defaultMessage="Search & Filter & Sort" />
          </button>
        </div>
      </div>
    );
  }
}
