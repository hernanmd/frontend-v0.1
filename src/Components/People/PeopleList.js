import React, { Component } from "react";
import { injectIntl } from "react-intl";
import PeopleSearch from "./PeopleSearch";
import ListComponent from "../Tools/ListComponent";
import Loading from "Components/Tools/Loading";
import Api from "Api";
import "./PeopleList.scss";

class PeopleList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listPeople: this.props.listPeople,
      searchBar: this.props.searchBar,
      listMembers: [],
      loading: true,
    };
  }

  static get defaultProps() {
    return {
      listPeople: [],
      colMax: 2,
      filter: false,
    };
  }

  componentDidMount() {
    const { itemId, itemType } = this.props;
    if (itemId) {
      this.getMembers(itemId, itemType);
    } else this.setState({ gotUsers: true });
  }

  componentWillReceiveProps(nextProps) {
    const { itemId, itemType } = nextProps;
    if (itemId) this.getMembers(itemId, itemType);
  }

  getMembers(itemId, itemType) {
    this.setState({ loading: true });
    Api.get("/api/" + itemType + "/" + itemId + "/members")
      .then((res) => {
        var listUsers = [];
        res.data.users.map((user) => {
          return listUsers.push(user);
        });
        this.setState({ listMembers: listUsers, loading: false, gotUsers: true });
      })
      .catch((error) => {
        // console.log(error);
        this.setState({ loading: false });
      });
  }

  render() {
    if (!this.state.gotUsers) {
      return <Loading active={this.state.loading} height="150px"></Loading>;
    }
    const { intl, filter, listPeople, itemType, searchBar, colMax } = this.props;
    const { listMembers } = this.state;
    var peoples = listPeople.length !== 0 ? listPeople : listMembers;
    if (filter) {
      var memberName = itemType === "challenges" || itemType ===  "programs" ? "entity.card.participants" : "general.members";
      var activeType = itemType === "challenges" || itemType ===  "programs" ? "members" : "creators";
      setTimeout(() => {
        document.querySelector(`a[href*="#${activeType}"]`).classList.add("active");
        document.querySelector(`#${activeType}`).classList.add("active");
      }, 500);
      return (
        <div className="peopleList">
          <nav className="nav modal-nav-tabs container-fluid">
            <a className="nav-item nav-link" href="#creators" data-toggle="tab">
              {intl.formatMessage({ id: "member.role.creators" })}
            </a>
            <a className="nav-item nav-link" href="#admins" data-toggle="tab">
              {intl.formatMessage({ id: "member.role.admins" })}
            </a>
            <a className="nav-item nav-link" href="#members" data-toggle="tab">
              {intl.formatMessage({ id: memberName })}
            </a>
          </nav>

          <div className="tabContainer">
            <div className="tab-content container-fluid">
              <div className="tab-pane" id="creators">
                <ListComponent itemType="people" filter="creators" data={peoples} colMax={colMax} />
              </div>
              <div className="tab-pane" id="admins">
                <ListComponent itemType="people" filter="admins" data={peoples} colMax={colMax} />
              </div>
              <div className="tab-pane" id="members">
                <ListComponent itemType="people" filter="members" data={peoples} colMax={colMax} />
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div className="peopleList">
          {searchBar && <PeopleSearch />}
          <ListComponent itemType="people" data={listPeople} colMax={colMax} />
        </div>
      );
    }
  }
}
export default injectIntl(PeopleList);
