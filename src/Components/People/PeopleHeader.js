import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import "../Main/Similar.scss";

export default class PeopleHeader extends Component {
  render() {
    return (
      <div className="peopleHeader">
        <h1>
          <FormattedMessage id="people.header.title" defaultMessage="Explore people" />
        </h1>
        <p>
          <FormattedMessage
            id="people.header.description"
            defaultMessage="Meet wonderful people that will help you breaking challenges."
          />
        </p>
        <hr />
      </div>
    );
  }
}
