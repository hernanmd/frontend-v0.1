import React, { Component } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
// import FormSkillsComponent from "Components/Tools/Forms/FormSkillsComponent";
import PostInputMentions from "Components/Tools/Forms/PostInputMentions";
// import Chips from "react-chips";
import Api from "Api";
import $ from "jquery";
import "./MembersModal.scss";

class MembersModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      emailInput: "",
      nicknameInput: "",
      disabledBtn: false,
      inviteSend: false,
      missInput: false,
      error: false,
      usersArray: [],
    };

    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleChangeNickname = this.handleChangeNickname.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  static get defaultProps() {
    return {
      itemType: "",
      onChange: (value) => console.log("onChange doesn't exist to update " + value),
      id: "skills",
      content: [],
      placeholder: "",
    };
  }

  handleChange(event) {
    this.props.onChange(this.props.id, event);
    // this.setState({ emailInput: "", nicknameInput: "", missInput: false});
  }

  handleChangeEmail(event) {
    this.setState({ emailInput: event.target.value, nicknameInput: "", missInput: false });
  }

  handleChangeNickname(content) {
    // console.log(content);
    var users = content.split("((@:");
    var usersArray = [];
    users.map(function (user) {
      if (user) {
        var userId = parseInt(user.split(":")[0]); // split valur with ":", and id is [0]
        usersArray.push(userId);
      }
    });
    this.setState({ emailInput: "", nicknameInput: content, missInput: false, usersArray: usersArray });
    // console.log("usersArray", this.state.usersArray);
  }

  handleSubmit(event) {
    event.preventDefault();
    if (
      this.props.itemType === "projects" ||
      this.props.itemType === "communities" ||
      this.props.itemType === "challenges" ||
      this.props.itemType === "programs"
    ) {
      if (this.props.itemId) {
        if (this.state.emailInput !== "" || this.state.nicknameInput !== "") {
          this.setState({ disabledBtn: true });

          var params = {};
          if (this.state.emailInput !== "") {
            params = { stranger_email: this.state.emailInput };
          } else {
            params = { user_ids: this.state.usersArray };
          }
          // console.log("params", params);
          Api.post("/api/" + this.props.itemType + "/" + this.props.itemId + "/invite", params)
            .then((res) => {
              // console.log(res);
              this.setState({ inviteSend: true });
              setTimeout(() => {
                //close
                $("#closeModal").click();
                this.resetState();
              }, 2000);
            })
            .catch((error) => {
              // console.log(error);
              this.setState({ error: true });
              setTimeout(() => {
                this.setState({ disabledBtn: false, error: false });
              }, 8000);
            });
        } else {
          this.setState({ missInput: true });
          setTimeout(() => {
            this.setState({ missInput: false });
          }, 4000);
        }
      } else {
        console.warn("itemId is missing");
      }
    } else {
      console.warn("itemType not compatible");
    }
  }

  resetState() {
    // console.log("resetState applied");
    this.setState({
      emailInput: "",
      nicknameInput: "",
      disabledBtn: false,
      inviteSend: false,
      missInput: false,
      error: false,
    });
  }

  render() {
    const disabledBtn = this.state.disabledBtn;
    var { intl } = this.props;

    // const {
    //   id,
    //   title,
    //   placeholder} = this.props;

    // var content = this.props.content.map((skill) => {
    //   if(skill.skill_name){
    //     return skill.skill_name;
    //   } else {
    //     return skill;
    //   }
    // });

    // var content = nicknameInput

    $("#membersModal").on("shown.bs.modal", function (e) {
      // console.log("HEY");
    });
    return (
      <div className="modal fade" id="membersModal" tabIndex="-1" role="dialog" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="membersModalLabel">
                <FormattedMessage id="member.invite.title" defaultMessage="Invite a member" />
              </h5>
              <button type="button" id="closeModal" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <form onSubmit={this.handleSubmit}>
                <div className="form-group">
                  <label htmlFor="byEmail">
                    <FormattedMessage id="member.invite.mail.label" defaultMessage="Email address :" />
                  </label>
                  <input
                    type="email"
                    className={"form-control " + (this.state.missInput && "is-invalid")}
                    id="byEmail"
                    placeholder={intl.formatMessage({ id: "member.invite.mail.placeholder" })}
                    value={this.state.emailInput}
                    onChange={this.handleChangeEmail}
                  />
                  {this.state.missInput && (
                    <div className="invalid-feedback">
                      <FormattedMessage id="member.invite.mail.empty" defaultMessage="Type an email here" />
                    </div>
                  )}
                  <small id="byEmailInfo" className="form-text text-muted">
                    <FormattedMessage
                      id="member.invite.mail.text"
                      defaultMessage="Enter the email address of the person you want to invite."
                    />
                  </small>
                </div>
                <div className="text-center or">
                  <FormattedMessage id="member.invite.or" defaultMessage="Or" />
                </div>
                <div className="form-group">
                  <label htmlFor="byNickname">
                    <FormattedMessage id="member.invite.nickname.label" defaultMessage="JOGL's Nickname :" />
                  </label>
                  <div className="input-group">
                    <PostInputMentions
                      content={this.state.nicknameInput}
                      onChange={this.handleChangeNickname}
                      placeholder={[["attach.project"], ["Add users"]]}
                    />
                    {this.state.missInput && (
                      <div className="invalid-feedback">
                        <FormattedMessage
                          id="member.invite.nickname.empty"
                          defaultMessage="or type a JOGL's nickname here"
                        />
                      </div>
                    )}
                  </div>
                  <small id="byNicknameInfo" className="form-text text-muted">
                    <FormattedMessage
                      id="member.invite.nickname.text"
                      defaultMessage="Enter the JOGL's nickname of the person you want to invite."
                    />
                  </small>
                </div>
                <div className="text-center btnZone">
                  <button type="submit" className="btn btn-primary btn-block" disabled={disabledBtn}>
                    {this.state.inviteSend ? (
                      <FormattedMessage id="member.invite.btnSendEnded" defaultMessage="Invitation sent" />
                    ) : (
                      <FormattedMessage id="member.invite.btnSend" defaultMessage="Send invitation" />
                    )}
                  </button>
                </div>
                {this.state.error && (
                  <div className="alert alert-danger" role="alert">
                    <FormattedMessage id={"err-"} defaultMessage="An error has occurred" />
                  </div>
                )}
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default injectIntl(MembersModal);
