import React, { Component, Fragment } from "react";
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";
import Api from "Api";
import DropdownRole from "../Tools/DropdownRole";
import defaultImg from "assets/img/default/default-user.png";
import "./MemberCard.scss";

export default class MemberCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: "",
      sending: false,
      member: this.props.member,
    };
  }

  static get defaultProps() {
    return {
      callBack: () => {
        console.log("Missing callback");
      },
      itemId: undefined,
      itemType: undefined,
      member: undefined,
    };
  }

  getRole() {
    const { member } = this.props;
    var actualRole = "member";
    if (!member.owner && !member.admin && !member.member) {
      actualRole = "pending";
    }
    if (member.member) {
      actualRole = "member";
    }
    if (member.owner) {
      actualRole = "owner";
    } else if (member.admin) {
      actualRole = "admin";
    }
    return actualRole;
  }

  acceptMember(newRole) {
    const { member } = this.state;
    const { itemId, itemType } = this.props;
    const role = this.getRole();
    const jsonToSend = {
      user_id: member.id,
      previous_role: role,
      new_role: newRole,
    };
    this.setState({ sending: true });
    Api.post("/api/" + itemType + "/" + itemId + "/members", jsonToSend)
      .then((res) => {
        this.setState({ sending: false });
        this.props.callBack();
      })
      .catch((error) => {
        this.setState({ sending: false });
        console.log(error);
      });
  }

  removeMember() {
    const { itemId, itemType, member } = this.props;
    this.setState({ sending: true, error: "" });
    Api.delete("/api/" + itemType + "/" + itemId + "/members/" + member.id)
      .then((res) => {
        // console.log(res);
        this.setState({ sending: false });
        this.props.callBack();
      })
      .catch((error) => {
        // console.log(error);
        this.props.callBack();
        this.setState({ sending: false, error: <FormattedMessage id="err-" defaultMessage="An error has occured" /> });
      });
  }

  render() {
    const { sending } = this.state;
    const { itemId, itemType } = this.props;

    if (this.props.member !== undefined) {
      var member = this.props.member;
      const role = this.getRole();

      var imgTodisplay = defaultImg;
      if (member.logo_url_sm !== "" && member.logo_url_sm !== null && member.logo_url_sm !== undefined) {
        imgTodisplay = member.logo_url_sm;
      }

      const bgLogo = {
        backgroundImage: "url(" + imgTodisplay + ")",
        backgroundSize: "cover",
        backgroundPosition: "center",
      };
      return (
        <div>
          <div className="row memberCard" key={member.id}>
            <div className="col-3 col-sm-2 col-lg-1 zoneLogo">
              <div className="logoMember" style={bgLogo}></div>
            </div>
            <div className="col-9 col-sm-6 zoneInfoMember">
              <Link to={"/user/" + member.id}>
                {member.first_name} {member.last_name}
              </Link>
              <br />
              <span className="role">
                <FormattedMessage id="member.as" defaultMessage="As " />
                <FormattedMessage id={"member.role." + role} defaultMessage="Member" />
              </span>
            </div>
            <div className="col-12 col-sm-4 text-left">
              {role === "pending" ? (
                <Fragment>
                  <button
                    type="button"
                    className="btn btn-outline-success"
                    disabled={sending === "accept" ? true : false}
                    onClick={() => this.acceptMember("member")}
                  >
                    {sending === "accept" && (
                      <Fragment>
                        <span
                          className="spinner-border spinner-border-sm text-center"
                          role="status"
                          aria-hidden="true"
                        ></span>
                        &nbsp;
                      </Fragment>
                    )}
                    <FormattedMessage id="general.accept" defaultMessage="Accept" />
                  </button>
                  <button
                    type="button"
                    className="btn btn-outline-danger"
                    disabled={sending === "remove" ? true : false}
                    onClick={() => this.removeMember()}
                  >
                    {sending === "remove" && (
                      <Fragment>
                        <span
                          className="spinner-border spinner-border-sm text-center"
                          role="status"
                          aria-hidden="true"
                        ></span>
                        &nbsp;
                      </Fragment>
                    )}
                    <FormattedMessage id="general.reject" defaultMessage="Reject" />
                  </button>
                </Fragment>
              ) : (
                <Fragment>
                  <DropdownRole
                    actualRole={role}
                    callBack={this.props.callBack}
                    itemId={itemId}
                    itemType={itemType}
                    listRole={["admin", "member", "owner"]}
                    member={member}
                  />
                  {role !== "owner" && (
                    <button type="button" className="delete" disabled={sending} onClick={() => this.removeMember()}>
                      {sending ? (
                        <Fragment>
                          <span
                            className="spinner-border spinner-border-sm text-center"
                            role="status"
                            aria-hidden="true"
                          ></span>
                          &nbsp;
                        </Fragment>
                      ) : (
                        "X "
                      )}
                      <FormattedMessage id="member.remove" defaultMessage="remove" />
                    </button>
                  )}
                </Fragment>
              )}
            </div>
            <div className="col-12">
              <div className={`roleChangedMessage alert member${member.id} alert-success`} role="alert">
                <FormattedMessage id="member.role.changed" defaultMessage="role was updated" />
              </div>
            </div>
          </div>
          <hr />
        </div>
      );
    } else {
      return null;
    }
  }
}
