import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { FormattedMessage, injectIntl } from "react-intl";
import Logo from "assets/img/logo_img.png";
import defaultProjectImg from "assets/img/default/default-project.jpg";
import defaultUserImg from "assets/img/default/default-user.png";
import UserMenu from "../User/UserMenu";
import "./Header.scss";
import Api from "Api";
import $ from "jquery";
// import { connectStateResults } from "react-instantsearch/connectors";
// import {
//   InstantSearch,
//   Hits,
//   SearchBox,
// 	Index,
// 	Configure,
// } from 'react-instantsearch-dom';
// import algoliasearch from 'algoliasearch/lite';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listPrograms: [],
      lang: "en",
    };
  }

  static get defaultProps() {
    return {
      userConnected: false,
    };
  }

  changeLanguage(newLanguage) {
    localStorage.setItem("language", newLanguage);
    window.location.reload();
  }

  componentDidMount() {
    Api.get("/api/programs")
      .then((res) => {
        var listPrograms = res.data;
        // console.log(res.data);
        this.setState({ listPrograms: listPrograms });
      })
      .catch((error) => {
        // console.log(error);
      });
    // collapse menu + reset searchbar when clicking on a menu link
    var navMain = $("#navbarNav");
    navMain.on("click", "a", null, function () {
      navMain.collapse("hide");
    });
    // when clicking anywhere on page..
    $(document).on("click", function (e) {
      var menu_opened = $("#navbarNav").hasClass("show");
      if (!$(e.target).closest("#navbarNav").length && !$(e.target).is("#navbarNav") && menu_opened === true) {
        $("#navbarNav").collapse("toggle"); // collapse mobile menu if open..
      }
      // + reset searchbar
      // if(document.querySelector(".search--mobile input").value !== "" || document.querySelector(".search--desktop input").value !== "") {
      // 	document.querySelector(".search--mobile .ais-SearchBox-reset").click()
      // 	document.querySelector(".search--desktop .ais-SearchBox-reset").click()
      // }
    });

    // manage lang dropdown
    var userLang = navigator.language || navigator.userLanguage;
    if (localStorage.getItem("language")) {
      // if user is signed-in or has selected a lang
      if (localStorage.getItem("language") === "fr") {
        $(".dropdown-item.fr").addClass("checked");
        this.setState({ lang: "fr" });
      } else if (localStorage.getItem("language") === "en") {
        $(".dropdown-item.en").addClass("checked");
        this.setState({ lang: "en" });
      } else {
        $(".dropdown-item.de").addClass("checked");
        this.setState({ lang: "de" });
      }
    } else {
      // if user is not signed-in or it's his first time
      if (userLang === "fr-FR" || userLang === "fr") {
        $(".dropdown-item.fr").addClass("checked");
        this.setState({ lang: "fr" });
      } else if (localStorage.getItem("language") === "de") {
        $(".dropdown-item.de").addClass("checked");
        this.setState({ lang: "de" });
      } else {
        $(".dropdown-item.de").addClass("checked");
        this.setState({ lang: "de" });
      }
    }
  }

  render() {
    const { intl } = this.props;
    var { listPrograms } = this.state;
    var connectClass = this.props.userConnected ? "connected" : "notConnected";

    return (
      <header className={`navContainer ${connectClass}`}>
        <nav className="nav container-fluid navbar navbar-expand-lg navbar-light d-flex justify-content-left">
          <Link to="/" className="navbar-brand">
            <img src={Logo} className="logo" alt="JoGL icon" />
          </Link>
          <div className="menu">
            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#navbarNav"
              aria-controls="navbarNav"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav mainMenu">
                {/* start - Algolia Instant Search */}
                <div className="search--desktop">
                  <Link to="/search" className="nav-link">
                    <i className="fas fa-search"></i>
                    <FormattedMessage id="header.search" defaultMessage="Search" />
                  </Link>
                </div>
                {/* end - Algolia Instant Search */}
                <li className="nav-item">
                  <Link to="/search/?i=Members" className="nav-link">
                    <FormattedMessage id="general.members" defaultMessage="People" />
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="/search/?i=Needs" className="nav-link">
                    <FormattedMessage id="entity.card.needs" defaultMessage="Needs" />
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="/search/?i=Projects" className="nav-link">
                    <FormattedMessage id="general.projects" defaultMessage="Projects" />
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="/search/?i=Groups" className="nav-link">
                    <FormattedMessage id="communities" defaultMessage="Communities" />
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to="/search/?i=Challenges" className="nav-link">
                    <FormattedMessage id="challenges" defaultMessage="Challenges" />
                  </Link>
                </li>
                <li className="nav-item dropdown">
                  <span
                    className="nav-link dropdown-toggle"
                    id="navbarDropdown"
                    role="button"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <FormattedMessage id="programs" defaultMessage="Programs" />
                  </span>
                  <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                    {listPrograms.reverse().map((program, index) => {
                      return (
                        // browse through programs (from newsest to oldest)
                        <Fragment key={index}>
                          <Link to={`/program/${program.short_title}`} className="dropdown-item">
                            {program.title}
                          </Link>
                          <div className="dropdown-divider"></div>
                        </Fragment>
                      );
                    })}
                    {/* <Link to="/program/coimmune" className="dropdown-item">Co-Immune</Link> */}
                  </div>
                </li>
                {!this.props.userConnected ? ( // if user is connected, show usermenu on header, not in dropdown
                  <UserMenu />
                ) : null}
              </ul>
              <div className="dropdown-divider"></div>
              <ul className="navbar-nav ml-auto">
                {this.props.userConnected ? ( // show create action only when user is connected
                  <li className="nav-item dropdown create">
                    <span
                      className="nav-link dropdown-toggle"
                      id="navbarDropdown"
                      role="button"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      <FormattedMessage id="entity.form.btnCreate" />
                    </span>
                    {/* <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown"> */}
                    <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                      <Link to="/project/create" className="dropdown-item">
                        <FormattedMessage id="header.createProject" />
                      </Link>
                      <div className="dropdown-divider"></div>
                      <Link to="/community/create" className="dropdown-item">
                        <FormattedMessage id="header.createGroup" />
                      </Link>
                    </div>
                  </li>
                ) : (
                  ""
                )}
                <li className="nav-item dropdown langSelect">
                  <span
                    className="nav-link dropdown-toggle"
                    id="langDropdown"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <i className="fas fa-globe"></i> {this.state.lang}
                  </span>
                  <div className="dropdown-menu" aria-labelledby="langDropdown">
                    <a className="dropdown-item en" onClick={() => this.changeLanguage("en")}>
                      English <i className="fas fa-check"></i>
                    </a>
                    <div className="dropdown-divider"></div>
                    <a className="dropdown-item fr" onClick={() => this.changeLanguage("fr")}>
                      Français <i className="fas fa-check"></i>
                    </a>
                    <div className="dropdown-divider"></div>
                    <a className="dropdown-item de" onClick={() => this.changeLanguage("de")}>
                      Deutsch <i className="fas fa-check"></i>
                    </a>
                  </div>
                </li>
              </ul>
            </div>
          </div>
          {/* start - Algolia Instant Search */}
          <div className="search--mobile">
            <Link to="/search" className="nav-link">
              <i className="fas fa-search"></i>
              <FormattedMessage id="header.search" defaultMessage="Search a" />
            </Link>
          </div>
          {/* end - Algolia Instant Search */}
          <UserMenu />
        </nav>
      </header>
    );
  }
}
export default injectIntl(Header);
