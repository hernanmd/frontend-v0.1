import React, { Component } from "react";
import { Link } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import defaultProgramImg from "assets/img/default/default-program.jpg";
import defaultLogoImg from "assets/img/default/default-user.png";
import BtnClap from "../Tools/BtnClap";
import "../Main/Cards.scss";

export default class ProgramCard extends Component {
  static get defaultProps() {
    return {
      program: undefined,
    };
  }

  renderProgramTeam(users) {
    return (
      <div className="programTeam">
        <span>
          <FormattedMessage id="general.members" defaultMessage="Program team" /> ({users.length}):{" "}
        </span>
        <span className="imgList">
          {users.map((user, index) => {
            var logoUrl = !user.logo_url_sm ? defaultLogoImg : user.logo_url_sm;
            const logoStyle = {
              backgroundImage: "url(" + logoUrl + ")",
            };
            if (index < 6) {
              // console.log("users: ", users);
              return (
                <Link to={"/user/" + user.id} key={index}>
                  <div className="peopleImg" style={logoStyle}></div>
                </Link>
              );
            } else if (index === 6) {
              return <div className="moreMembers">+{users.length - 6}</div>;
            }
            return "";
          })}
        </span>
      </div>
    );
  }

  render() {
    if (this.props.program !== undefined) {
      var { id, title, short_title, banner_url, users } = this.props.program;

      if (banner_url === undefined || banner_url === null || banner_url === "") {
        banner_url = defaultProgramImg;
      }
      const programImgStyle = {
        backgroundImage: "url(" + banner_url + ")",
      };
      return (
        <div className="card cardProgram" key={id}>
          <Link to={"/program/" + short_title}>
            <div style={programImgStyle} className="programImg" />
          </Link>
          <BtnClap itemType="programs" itemId={id} clapState={this.props.program.has_clapped} />
          <div className="card-content">
            <Link to={"/program/" + short_title}>
              <h5 className="card-title">{title}</h5>
            </Link>
          </div>
          <div className="cardfooter">
            <hr />
            {users !== undefined && users.length > 0 && this.renderProgramTeam(users)}
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}
