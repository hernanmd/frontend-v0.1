import React, { Component } from "react";
import ProgramsHeader from "./ProgramsHeader";
import ListComponent from "../Tools/ListComponent";
import "./ProgramLists.scss";

export default class ProgramList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listPrograms: this.props.listPrograms,
      searchBar: this.props.searchBar,
    };
  }

  static get defaultProps() {
    return {
      listPrograms: [],
      searchBar: true,
      colMax: 3,
    };
  }

  render() {
    return (
      <div className="programsList">
        {this.props.searchBar && <ProgramsHeader />}
        <ListComponent itemType="program" data={this.props.listPrograms} colMax={this.props.colMax} />
      </div>
    );
  }
}
