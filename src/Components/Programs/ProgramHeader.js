import React, { Component } from "react";
import { FormattedMessage, FormattedDate, injectIntl } from "react-intl";
import { Link } from "react-router-dom";
import defaultImg from "assets/img/default/default-program.jpg";
import BtnFollow from "../Tools/BtnFollow";
import ShareBtns from "../Tools/ShareBtns/ShareBtns";
import { renderChalProgStatsModal } from "Components/Tools/utils/utils.js";
import "./ProgramHeader.scss";

class ProgramHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      has_followed: this.props.program.has_followed,
      finishedLoading: false,
    };
  }

  static get defaultProps() {
    return {
      lang: "en",
    };
  }

  editBtn() {
    if (this.props.program.is_admin) {
      return (
        <Link to={"/program/" + this.props.program.short_title + "/edit"}>
          <i className="fa fa-edit" />
          <FormattedMessage id="entity.form.btnAdmin" defaultMessage="Edit" />
        </Link>
      );
    }
  }

  componentDidMount() {
    // click directly on "follow program btn" when opening link that has parameter ?follow=true
    var urlParams = new URLSearchParams(window.location.search);
    if (urlParams.get("follow") == "true") document.querySelector(".zoneBtnActions .btn:nth-child(2)").click();
    this.setState({ finishedLoading: true });
  }

  exploreChallenges() {
    document.querySelector(".nav-tabs .nav-link.challenges").click(); // click on challenges tab
    var element = document.querySelector(`.nav-tabs`); // get active tab content
    const y = element.getBoundingClientRect().top + window.pageYOffset - 160; // calculate it's top value and remove 140 of offset
    window.scrollTo({ top: y, behavior: "smooth" }); // scroll to tab
    if (!this.state.has_followed) {
      document.querySelector(".zoneBtnActions .btn:nth-child(2)").click(); // also click on follow btn (if user is not already following)
      this.setState({ has_followed: false });
    } else {
      this.setState({ has_followed: true });
    }
  }

  render() {
    const {
      banner_url,
      launch_date,
      // end_date,
      // follower_count,
      has_followed,
      id,
      // status,
      members_count,
      title,
      title_fr,
      short_description,
      short_description_fr,
    } = this.props.program;

    const { intl } = this.props;
    var bannerUrl = banner_url ? banner_url : defaultImg;

    return (
      <div className="programHeader">
        <img src={bannerUrl} width="100%" />
        <div className="programHeader--title">
          <h1 className="text-center">
            {/* display different field depending on language */}
            {"{ " + (this.props.lang === "fr" && title_fr ? title_fr : title) + " }"}
          </h1>
          {this.editBtn()}
        </div>
        {/* <p>#{short_title}</p> */}
        <div className="row programHeader--info">
          <div className="col-12 col-md-3 order-md-1">
            <p className="info">
              {/* <strong><FormattedMessage id="entity.info.launch_date" defaultMessage="Launch" />: </strong> */}
              <strong>
                <FormattedMessage id="entity.info.launch_date" defaultMessage="Launch" />
                <br />
              </strong>
              {launch_date ? (
                <FormattedDate value={launch_date} year="numeric" month="long" day="2-digit" />
              ) : (
                <FormattedMessage id="general.noDate" defaultMessage="No date" />
              )}
              {/* <span className="status">{intl.formatMessage({id:`entity.info.status.${status}`})}</span> */}
            </p>
          </div>
          <div className="col-12 col-md-6 order-first order-md-2 short_desc text-center">
            <p>{this.props.lang === "fr" && short_description_fr ? short_description_fr : short_description}</p>
            <ShareBtns type="program" />
          </div>
          <div className="zoneBtnActions col-12 col-md-3 order-md-3">
            <BtnFollow
              followState={has_followed}
              itemType="programs"
              itemId={id}
              textFollow={<FormattedMessage id="program.info.btnFollow" defaultMessage="Follow program" />}
              textUnfollow={<FormattedMessage id="general.unfollow" defaultMessage="Unfollow program" />}
            />
            <button className="btn btn-primary btn-md" type="button" onClick={this.exploreChallenges.bind(this)}>
              {intl.formatMessage({ id: "challenge.info.btnParticipate" })}
            </button>
            <div className="programStats text-right">
              <span className="text" data-toggle="modal" data-target="#participantsModal">
                <strong>{members_count ? members_count : 0}</strong>&nbsp;
                <FormattedMessage id="entity.info.participants" defaultMessage="Participants" />
                {members_count > 1 ? "s" : ""}
              </span>
            </div>
          </div>
          <ShareBtns type="program" device="mobile" />
        </div>
        {members_count > 0 &&
          this.state.finishedLoading &&
          renderChalProgStatsModal("participantsModal", "programs", this.props.program) /* if whole page has loaded */}
      </div>
    );
  }
}
export default injectIntl(ProgramHeader);
