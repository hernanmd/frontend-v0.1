import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import { UserContext } from "UserProvider";
import ProgramForm from "./ProgramForm";
import Api from "Api";

export default class ProgramCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newProgram: {
        title: "",
        short_title: "",
        logo_url: "",
        description: "",
        short_description: "",
        creator_id: Number(localStorage.getItem("userId")),
        status: "draft",
        interests: [],
        skills: [],
        banner_url: "",
        is_private: false,
      },
      programIsCreated: null,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(key, content) {
    var updateProgram = this.state.newProgram;
    updateProgram[key] = content;
    this.setState({ newProgram: updateProgram });
  }

  handleSubmit() {
    const newProgram = this.state.newProgram;
    // console.log("PROJET A CREER", newProgram);
    Api.post("/api/programs/", { program: newProgram })
      .then((res) => {
        // console.log("PROJECT CREATED");
        // console.log(res);
        this.setState({ programIsCreated: res.data.data });
      })
      .catch((error) => {
        // console.log(error);
      });
  }

  render() {
    // let userContext = this.context;
    var { newProgram, userInProgram } = this.state;
    // console.log(this.state);
    if (this.state.programIsCreated) {
      return <Redirect to={"/programs"} />;
    }
    // redirect program creation to homepage (TEMPORARY FIX!)
    var forbidden = true;
    if (forbidden) {
      return <Redirect to={"/"} />;
    }
    return (
      <div className="programCreate container-fluid">
        <h1>
          <FormattedMessage id="program.create.title" defaultMessage="Create a new program" />
        </h1>
        <ProgramForm
          mode="create"
          program={newProgram}
          userInProgram={userInProgram}
          handleChange={this.handleChange}
          handleSubmit={this.handleSubmit}
        />
      </div>
    );
  }
}
ProgramCreate.contextType = UserContext;
