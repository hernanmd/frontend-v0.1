import React, { Component, Fragment } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import { Link } from "react-router-dom";
import FormDefaultComponent from "Components/Tools/Forms/FormDefaultComponent";
import FormImgComponent from "Components/Tools/Forms/FormImgComponent";
import FormWysiwygComponent from "Components/Tools/Forms/FormWysiwygComponent";
import FormDropdownComponent from "Components/Tools/Forms/FormDropdownComponent";
import DefaultImgProgram from "assets/img/default/default-program.jpg";
import "./ProgramForm.scss";

class ProgramForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      program: this.props.program,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  static get defaultProps() {
    return {
      mode: "create",
      program: {
        id: 0,
        title: "",
        short_title: "",
        description: "",
        claps_count: 0,
        follower_count: 0,
        members_count: 0,
        launch_date: null,
        end_date: null,
      },
    };
  }

  handleChange(key, content) {
    this.props.handleChange(key, content);
    //console.log("Form handleChange : " + key + " " + content);
  }

  handleSubmit(event) {
    event.preventDefault();
    /* const { program } = this.state; */
    this.props.handleSubmit(/* program */);
  }

  renderBtnsForm() {
    var urlBack = "/programs";
    var textAction = "Create";
    if (this.props.mode === "edit") {
      urlBack = "/program/" + this.props.program.short_title;
      textAction = "Update";
    }

    return (
      <div className="row programFormBtns">
        <Link to={urlBack}>
          <button type="button" className="btn btn-outline-primary">
            <FormattedMessage id="entity.form.btnCancel" defaultMessage="Cancel" />
          </button>
        </Link>
        <button type="submit" className="btn btn-primary" style={{ marginRight: "10px" }}>
          <FormattedMessage id={"entity.form.btn" + textAction} defaultMessage={textAction} />
        </button>
      </div>
    );
  }

  render() {
    var { program, intl } = this.props;
    return (
      <form onSubmit={this.handleSubmit} className="programForm">
        <FormDefaultComponent
          id="title"
          content={program.title}
          title={intl.formatMessage({ id: "entity.info.title", defaultMessage: "Title" })}
          placeholder={intl.formatMessage({ id: "program.form.title.placeholder" })}
          onChange={this.handleChange.bind(this)}
          mandatory={true}
        />
        <FormDefaultComponent
          id="title_fr"
          content={program.title_fr}
          title={intl.formatMessage({ id: "entity.info.title_fr", defaultMessage: "Title" })}
          placeholder={intl.formatMessage({ id: "program.form.title.placeholder" })}
          onChange={this.handleChange.bind(this)}
          mandatory={true}
        />
        <FormDefaultComponent
          id="short_title"
          content={program.short_title}
          title={intl.formatMessage({ id: "entity.info.short_name", defaultMessage: "Short Name" })}
          placeholder={intl.formatMessage({ id: "program.form.short_title.placeholder" })}
          onChange={this.handleChange.bind(this)}
          prepend="#"
          mandatory={true}
          pattern={/[A-Za-z0-9]/g}
        />
        <FormDefaultComponent
          id="short_description"
          content={program.short_description}
          title={intl.formatMessage({ id: "entity.info.short_description", defaultMessage: "Short description" })}
          placeholder={intl.formatMessage({ id: "program.form.short_description.placeholder" })}
          onChange={this.handleChange.bind(this)}
          mandatory={true}
        />
        <FormDefaultComponent
          id="short_description_fr"
          content={program.short_description_fr}
          title={intl.formatMessage({ id: "entity.info.short_description_fr", defaultMessage: "Short description" })}
          placeholder={intl.formatMessage({ id: "program.form.short_description.placeholder" })}
          onChange={this.handleChange.bind(this)}
          mandatory={true}
        />
        <FormWysiwygComponent
          id="description"
          content={program.description}
          title={intl.formatMessage({ id: "entity.info.description", defaultMessage: "Description" })}
          placeholder={intl.formatMessage({ id: "program.form.description.placeholder" })}
          onChange={this.handleChange.bind(this)}
        />
        <FormWysiwygComponent
          id="description_fr"
          content={program.description_fr}
          title={intl.formatMessage({ id: "entity.info.description_fr", defaultMessage: "Description" })}
          placeholder={intl.formatMessage({ id: "program.form.description.placeholder" })}
          onChange={this.handleChange.bind(this)}
        />
        <FormWysiwygComponent
          id="faq"
          content={program.faq}
          title={intl.formatMessage({ id: "entity.info.faq", defaultMessage: "faq" })}
          onChange={this.handleChange.bind(this)}
        />
        <FormWysiwygComponent
          id="faq_fr"
          content={program.faq_fr}
          title={intl.formatMessage({ id: "entity.info.faq_fr", defaultMessage: "faq" })}
          onChange={this.handleChange.bind(this)}
        />
        {/* <FormWysiwygComponent
					id="enablers"
					content={program.enablers}
					title={intl.formatMessage({ id:"entity.info.enablers", defaultMessage:"enablers" })}
					onChange={this.handleChange.bind(this)} />
				<FormWysiwygComponent
					id="enablers_fr"
					content={program.enablers_fr}
					title={intl.formatMessage({ id:"entity.info.enablers_fr", defaultMessage:"enablers" })}
					onChange={this.handleChange.bind(this)} /> */}
        <FormWysiwygComponent
          id="ressources"
          content={program.ressources}
          title={intl.formatMessage({ id: "entity.info.ressources", defaultMessage: "ressources" })}
          onChange={this.handleChange.bind(this)}
        />
        <FormImgComponent
          id="banner_url"
          content={program.banner_url}
          title={intl.formatMessage({ id: "program.info.banner_url", defaultMessage: "Program banner" })}
          imageUrl={program.banner_url}
          itemId={program.id}
          type="banner"
          itemType="programs"
          defaultImg={DefaultImgProgram}
          onChange={this.handleChange.bind(this)}
        />
        <FormDropdownComponent
          id="status"
          content={program.status}
          title={intl.formatMessage({ id: "entity.info.status", defaultMessage: "Status" })}
          list={["draft", "soon", "active", "completed"]}
          onChange={this.handleChange.bind(this)}
        />
        <FormDefaultComponent
          id="launch_date"
          content={program.launch_date ? program.launch_date.substr(0, 10) : null}
          title={intl.formatMessage({ id: "entity.info.launch_date", defaultMessage: "Launch date" })}
          onChange={this.handleChange.bind(this)}
          type="date"
        />
        <FormDefaultComponent
          id="end_date"
          content={program.end_date ? program.end_date.substr(0, 10) : null}
          title={intl.formatMessage({ id: "entity.info.end_date", defaultMessage: "End date" })}
          onChange={this.handleChange.bind(this)}
          type="date"
        />
        {this.renderBtnsForm()}
      </form>
    );
  }
}
export default injectIntl(ProgramForm);
