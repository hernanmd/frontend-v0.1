import React, { Component } from "react";
import { Link } from "react-router-dom";
import ListComponent from "../Tools/ListComponent";
import { FormattedMessage } from "react-intl";

export default class CommunityList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listCommunities: this.props.listCommunities,
      searchBar: this.props.searchBar,
    };
  }

  static get defaultProps() {
    return {
      listCommunities: [],
      searchBar: true,
      colMax: 3,
      displayHome: undefined,
      cardFormat: undefined,
    };
  }

  render() {
    const { colMax, listCommunities, displayHome, cardFormat } = this.props;
    return (
      <div className="communityList">
        {displayHome && (
          <h3>
            <FormattedMessage id="communities.latest" defaultMessage="Latest communities" />
          </h3>
        )}
        <ListComponent itemType="community" data={listCommunities} colMax={colMax} cardFormat={cardFormat} />
        {displayHome && (
          <Link to="/communities" className="viewMoreRecents">
            <FormattedMessage id="home.viewMoreRecent" defaultMessage="View more" />
          </Link>
        )}
      </div>
    );
  }
}
