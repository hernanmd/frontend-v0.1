import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";
import { UserContext } from "UserProvider";

export default class CommunitySearch extends Component {
  render() {
    let userContext = this.context;
    return (
      <div className="row justify-content-end communitySearch">
        <div className="col-12 text-right">
          {/* <button className="btn btn-sm btnSearch">
          <FormattedMessage id="entity.searchBar.mainBtn" defaultMessage="Search & Filter & Sort" />
        </button> */}
          {userContext.connected && (
            <Link to="/community/create">
              <button className="btn btn-sm btn-primary btn-action">
                <FormattedMessage id="communities.searchBar.btnAddCommunity" defaultMessage="Add a community" />
              </button>
            </Link>
          )}
        </div>
      </div>
    );
  }
}
CommunitySearch.contextType = UserContext;
