import React, { Component } from "react";
import { Link } from "react-router-dom";
import defaultImg from "assets/img/default/default-group.jpg";
import BtnClap from "../Tools/BtnClap";
import { renderOwnerNames, renderTeam } from "Components/Tools/utils/utils.js";
import "../Main/Cards.scss";

export default class CommunityCard extends Component {
  static get defaultProps() {
    return {
      community: undefined,
      source: "api",
    };
  }

  render() {
    if (this.props.community !== undefined) {
      var {
        id,
        title,
        banner_url,
        short_description,
        creator,
        users_sm,
        has_clapped,
        claps_count,
        members_count,
      } = this.props.community;
      if (banner_url === undefined || banner_url === null || banner_url === "") {
        banner_url = defaultImg;
      }
      const communityImgStyle = {
        backgroundImage: "url(" + banner_url + ")",
      };

      if (this.props.cardFormat !== "compact") {
        return (
          <div className="card cardCommunity" key={id}>
            <Link to={"/community/" + id}>
              <div style={communityImgStyle} className="communityImg" />
            </Link>
            <BtnClap
              itemType="communities"
              itemId={id}
              clapState={has_clapped}
              source={this.props.source}
              clapCount={claps_count}
            />
            <div className="card-content">
              <Link to={"/community/" + id}>
                <h5 className="card-title">{title}</h5>
              </Link>
              <p className="card-desc">{short_description}</p>
              {users_sm !== undefined && users_sm.length > 0 && renderOwnerNames(users_sm, creator)}
              {/* <InfoSkillsComponent place="entity_header" limit="3" content={skills} /> */}
              {/* <InfoRessourcesComponent place="entity_header" limit="3" content={ressources} /> */}
            </div>
            <div className="cardfooter">
              <hr />
              {users_sm !== undefined && users_sm.length > 0 && renderTeam(users_sm, "community", id, members_count)}
            </div>
          </div>
        );
      } else {
        return (
          <div className="card cardCommunity cardCommunitySmall" key={id}>
            <Link to={"/community/" + id}>
              <div style={communityImgStyle} className="communityImg" />
            </Link>
            <BtnClap itemType="communities" itemId={id} clapState={has_clapped} clapCount={claps_count} />
            <div className="card-content">
              <Link to={"/community/" + id}>
                <h5 className="card-title">{title}</h5>
              </Link>
              <p className="card-desc">{short_description}</p>
            </div>
          </div>
        );
      }
    } else {
      return null;
    }
  }
}
