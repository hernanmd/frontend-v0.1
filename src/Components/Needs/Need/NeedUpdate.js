import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import Api from "Api";
import NeedDocsManagement from "./NeedDocsManagement";
import NeedForm from "./NeedForm";
import NeedWorkers from "./NeedWorkers";
import "../Needs.scss";

export default class NeedUpdate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      description: this.props.description,
      documents: [],
      error: "",
      isCreating: false,
      need: this.props.need,
      uploading: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  static get defaultProps() {
    return {
      changeMode: () => console.log("Missing function"),
      need: undefined,
      mode: "",
      refresh: () => console.log("Missing function"),
    };
  }

  componentDidMount() {
    this.setState({ documents: this.props.need.documents });
  }

  handleChange(key, content) {
    var updateNeed = this.state.need;
    updateNeed[key] = content;
    this.setState({ need: updateNeed, error: "" });
  }

  handleChangeDoc(documents) {
    this.handleChange("documents", documents);
  }

  handleSubmit() {
    const { need } = this.state;
    this.setState({ uploading: true });
    Api.patch("/api/needs/" + need.id, { need: need })
      .then((res) => {
        // console.log(res);
        if (need.documents.length > 0) {
          // console.log(res.data)
          const itemId = res.data.id;
          // console.log("NEEDID", itemId);
          const itemType = "needs";
          const type = "documents";
          if (itemId) {
            var bodyFormData = new FormData();
            Array.from(need.documents).forEach((file) => {
              // console.log("file : ", file);
              bodyFormData.append(type + "[]", file);
            });
            var config = {
              headers: { "Content-Type": "multipart/form-data" },
            };

            Api.post("/api/" + itemType + "/" + itemId + "/documents", bodyFormData, config)
              .then((res) => {
                // console.log(res);
                // console.log(res.data);
                if (res.status === 200) {
                  // console.log("Documents uploaded !");
                  this.refresh();
                } else {
                  // console.log("An error has occured");
                  this.setState({
                    uploading: false,
                    error: <FormattedMessage id="err-" defaultMessage="An error has occured" />,
                  });
                }
              })
              .catch((error) => {
                // console.log(error);
                // console.log(error.response.data);
                this.setState({
                  uploading: false,
                  error: error.response.data.status + " : " + error.response.data.error,
                });
              });
          } else {
            // console.log("Unable to upload files (No Id defined)");
            this.refresh();
          }
        } else {
          this.refresh();
        }
      })
      .catch((error) => {
        // console.log(error);
        // console.log("An error has occured");
        this.setState({
          uploading: false,
          error: <FormattedMessage id="err-" defaultMessage="An error has occured" />,
        });
      });
  }

  refresh() {
    this.setState({
      documents: [],
      isCreating: false,
      need: undefined,
      valid_content: undefined,
      // valid_end_date: undefined,
      valid_skills: undefined,
      valid_ressources: undefined,
      valid_title: undefined,
      uploading: false,
    });
    this.props.refresh();
    this.props.changeMode("card");
  }

  render() {
    const { need, refresh } = this.state;
    // put need card back to different state depending if it's in an object, or as a single need page
    const cancelMode = this.props.cancelMode ? "details" : "card";
    if (need === undefined) {
      return null;
    } else {
      return (
        <div className="needUpdate">
          {this.state.error && (
            <div className="alert alert-danger" role="alert">
              <FormattedMessage id={"err-"} defaultMessage="An error has occurred" />
            </div>
          )}
          <NeedForm
            action="update"
            cancel={() => this.props.changeMode(cancelMode)}
            need={need}
            handleChange={this.handleChange}
            handleSubmit={this.handleSubmit}
            uploading={this.state.uploading}
          />
          <NeedWorkers need={need} mode="update" refresh={refresh} />
          <NeedDocsManagement
            need={need}
            refresh={refresh}
            handleChange={this.handleChangeDoc.bind(this)}
            mode="update"
          />
        </div>
      );
    }
  }
}
