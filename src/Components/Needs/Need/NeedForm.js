import React, { Component, Fragment } from "react";
import { FormattedMessage } from "react-intl";
import { UserContext } from "UserProvider";
import NeedBtnStatus from "./NeedBtnStatus";
import FormSkillsComponent from "Components/Tools/Forms/FormSkillsComponent";
import FormRessourcesComponent from "Components/Tools/Forms/FormRessourcesComponent";
/*** Validators ***/
import FormValidator from "Components/Tools/Forms/FormValidator";
import needFormRules from "./needFormRules";
/*** Images/Style ***/
import "./NeedForm.scss";

export default class NeedForm extends Component {
  validator = new FormValidator(needFormRules);

  static get defaultProps() {
    return {
      action: "create",
      cancel: () => console.log("Missing function"),
      handleChange: () => console.log("Missing function"),
      handleSubmit: () => console.log("Missing function"),
      need: undefined,
      uploading: false,
      refresh: () => console.log("Missing function"),
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      isChecked: this.props.need.is_urgent,
    };
  }

  handleChange(event, value) {
    const key = event === "skills" ? event : event.target.id;
    // const key = event === "ressources" ? event : event.target.id;
    // console.log(key, value);
    const content = event === "skills" ? value : event.target.value;
    // const content = event === "ressources" ? value : event.target.value;
    /* Validators start */
    const state = {};
    state[key] = content;
    const validation = this.validator.validate(state);
    if (validation[key] !== undefined) {
      const stateValidation = {};
      stateValidation["valid_" + key] = validation[key];
      this.setState(stateValidation);
    }
    /* Validators end */
    this.props.handleChange(key, content);
  }

  handleChange_ressource(event, value) {
    // const key = event === "skills" ? event : event.target.id;
    const key = event === "ressources" ? event : event.target.id;
    // console.log(key, value);
    // const content = event === "skills" ? value : event.target.value;
    const content = event === "ressources" ? value : event.target.value;
    /* Validators start */
    const state = {};
    state[key] = content;
    const validation = this.validator.validate(state);
    if (validation[key] !== undefined) {
      const stateValidation = {};
      stateValidation["valid_" + key] = validation[key];
      this.setState(stateValidation);
    }
    /* Validators end */
    this.props.handleChange(key, content);
  }

  handleChangeToggle(event) {
    this.setState({
      isChecked: !this.state.isChecked,
    });
    const key = event.target.id;
    const content = !this.state.isChecked;
    this.props.handleChange(key, content);
  }

  handleSubmit(event) {
    event.preventDefault();
    /* Validators control before submit */
    const validation = this.validator.validate(this.props.need);
    // console.log(validation);
    if (validation.isValid) {
      this.props.handleSubmit();
    } else {
      const stateValidation = {};
      Object.keys(validation).forEach((key) => {
        if (key !== "isValid") {
          stateValidation["valid_" + key] = validation[key];
        }
      });
      this.setState(stateValidation);
    }
  }

  renderInvalid(valid_obj) {
    if (valid_obj) {
      if (valid_obj.message !== "") {
        return (
          <div className="invalid-feedback">
            <FormattedMessage id={valid_obj.message} defaultMessage="Value is not valid" />
          </div>
        );
      }
    }
    return null;
  }

  render() {
    const { action, need, uploading } = this.props;
    const { valid_content, valid_skills, valid_title, isChecked } = this.state ? this.state : "";
    var submitBtnText = action === "create" ? "Create" : "Update";
    return (
      <div className="needForm">
        <div className="form-row">
          <div className="input-group col-12 col-md-6">
            <FormattedMessage id="need.title" defaultMessage="Need Title">
              {(placeholder) => (
                <input
                  className={
                    "form-control " + (valid_title ? (!valid_title.isInvalid ? "is-valid" : "is-invalid") : "")
                  }
                  id="title"
                  name="title"
                  onChange={this.handleChange.bind(this)}
                  placeholder={placeholder}
                  type="text"
                  value={need.title}
                />
              )}
            </FormattedMessage>
            {this.renderInvalid(valid_title)}
          </div>
          <div className="input-group col-12 col-md-6 inputUrgent">
            <div className="isUrgent">
              <input
                type="checkbox"
                name="checkbox"
                id="is_urgent"
                checked={isChecked}
                onChange={this.handleChangeToggle.bind(this)}
              />
              <label htmlFor="is_urgent">
                <FormattedMessage id="need.isUrgent" />
              </label>
            </div>
          </div>
          <div className="input-group col-12">
            <FormattedMessage id="need.content" defaultMessage="Detail your need">
              {(placeholder) => (
                <textarea
                  className={
                    "form-control " + (valid_content ? (!valid_content.isInvalid ? "is-valid" : "is-invalid") : "")
                  }
                  id="content"
                  onChange={this.handleChange.bind(this)}
                  placeholder={placeholder}
                  rows={10}
                  value={need.content}
                />
              )}
            </FormattedMessage>
            {this.renderInvalid(valid_content)}
          </div>
          <div className="input-group col-12">
            <FormattedMessage id="general.skills.placeholder">
              {(skills) => (
                <FormSkillsComponent
                  className={
                    "form-control " + (valid_skills ? (!valid_skills.isInvalid ? "is-valid" : "is-invalid") : "")
                  }
                  content={need.skills}
                  errorCodeMessage={valid_skills ? valid_skills.message : ""}
                  id="skills"
                  isValid={valid_skills ? !valid_skills.isInvalid : undefined}
                  onChange={this.handleChange.bind(this)}
                  placeholder={skills}
                  type="need"
                  title={<FormattedMessage id="need.skills.title" defaultMessage="Expected skills" />}
                />
              )}
            </FormattedMessage>
            {this.renderInvalid(valid_skills)}
          </div>
          <div className="input-group col-12">
            <FormattedMessage id="general.ressources.placeholder">
              {(ressources) => (
                <FormRessourcesComponent
                  className={"form-control"}
                  content={need.ressources}
                  id="ressources"
                  onChange={this.handleChange_ressource.bind(this)}
                  placeholder={ressources}
                  type="need"
                  title={<FormattedMessage id="need.ressources.title" defaultMessage="Expected ressources" />}
                />
              )}
            </FormattedMessage>
          </div>
        </div>
        <div className="actionBar">
          <button
            className="btn btn-primary"
            disabled={uploading ? true : false}
            onClick={this.handleSubmit.bind(this)}
            type="submit"
          >
            {uploading && (
              <Fragment>
                <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true"></span>
                &nbsp;
              </Fragment>
            )}
            <FormattedMessage id={"post.create.btn" + submitBtnText} defaultMessage={submitBtnText} />
          </button>
          {this.context.connected ? need.is_owner ? <NeedBtnStatus need={need} /> : null : null}
          <button
            type="button"
            className="btn btn-outline-primary cancel"
            onClick={() => {
              this.props.cancel();
            }}
            disabled={uploading}
          >
            <FormattedMessage id="entity.form.btnCancel" defaultMessage="Cancel" />
          </button>
        </div>
      </div>
    );
  }
}
NeedForm.contextType = UserContext;
