import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import Api from "Api";

export default class NeedBtnStatus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      need: this.props.need,
      sending: false,
    };
  }

  static get defaultProps() {
    return {
      need: undefined,
      refresh: () => console.log("Missing function"),
    };
  }

  componentWillReceiveProps(nextProps) {
    const { need } = nextProps;
    this.setState({ need });
  }

  changeStatus() {
    const { need } = this.state;
    var newNeed = { ...need };
    this.setState({ sending: true });
    // console.log("status", newNeed.status);
    if (newNeed.status === null || newNeed.status === "active") {
      newNeed.status = "completed";
    } else {
      newNeed.status = "active";
    }
    // console.log("Need to send : ", newNeed.status);
    Api.patch("/api/needs/" + newNeed.id, { need: newNeed })
      .then((res) => {
        this.setState({ need: newNeed, sending: false });
        if (this.props.refresh !== undefined) {
          // this.props.refresh();
        }
        document.querySelector(".btn.cancel").click();
      })
      .catch((error) => {
        // console.log(error);
        this.setState({ sending: false });
      });
  }

  render() {
    const { need, sending } = this.state;
    if (need && need.is_owner) {
      return (
        <button className="btn btn-warning btnStatus" onClick={() => this.changeStatus()}>
          {need.status === "completed" ? (
            sending ? (
              <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true">
                {" "}
                &nbsp;
              </span>
            ) : (
              <FormattedMessage id="need.card.reopen" defaultMessage="Reopen" />
            )
          ) : sending ? (
            <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true">
              {" "}
              &nbsp;
            </span>
          ) : (
            <FormattedMessage id="need.card.close" defaultMessage="Close" />
          )}
        </button>
      );
    }
    return null;
  }
}
