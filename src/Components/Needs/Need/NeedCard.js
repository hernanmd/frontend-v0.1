import React, { Component, Fragment } from "react";
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";
import { UserContext } from "UserProvider";
import BtnFollow from "Components/Tools/BtnFollow";
import BtnJoin from "Components/Tools/BtnJoin";
import ShareBtns from "Components/Tools/ShareBtns/ShareBtns";
import NeedDelete from "./NeedDelete";
import NeedWorkers from "./NeedWorkers";
import Feed from "Components/Feed/Feed";
import NeedDocsManagement from "./NeedDocsManagement";
import InfoSkillsComponent from "Components/Tools/Info/InfoSkillsComponent";
import InfoRessourcesComponent from "Components/Tools/Info/InfoRessourcesComponent";
import Api from "Api";
import Alert from "Components/Tools/Alert";
import { linkify, copyLink } from "Components/Tools/utils/utils.js";
import "./NeedCard.scss";

export default class NeedCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: []
    };
  }

  static get defaultProps() {
    return {
      changeMode: () => console.log("Missing function"),
      need: undefined,
      refresh: () => console.log("Missing function"),
      mode: ""
    };
  }

  changeDisplayFeed() {
    this.setState({ seeAllFeed: !this.state.seeAllFeed });
  }

  getDaysLeft(end_date) {
    const now = new Date();
    const end = new Date(end_date);
    var daysLeft = Math.round((end - now) / 1000 / 60 / 60 / 24);
    if (daysLeft < 0) {
      daysLeft = 0;
    }
    return daysLeft;
  }

  toggleMode() {
    // change mode/view of card, depending on its current state
    var { mode, changeMode } = this.props;
    mode === "details" && changeMode("card");
    mode === "card" && changeMode("details");
  }

  getFeedApi() {
    Api.get("/api/feeds/" + this.props.need.feed_id)
      .then(res => {
        this.setState({ posts: res.data });
        // console.log(res);
      })
      .catch(error => {
        // console.log(error);
      });
  }

  componentDidMount() {
    this.getFeedApi();
  }

  render() {
    const { need, changeMode, refresh, mode } = this.props;
    const { posts, seeAllFeed } = this.state;
    const contentWithLinks = linkify(need.content);
    var posts_count = posts.length;
    var comments_count = 0;
    posts.map(
      (
        post // map through all posts of need and count all comments
      ) => (comments_count += post.comments.length)
    );
    if (need === undefined) {
      return null;
    } else {
      // regular card format
      if (this.props.cardFormat !== "compact") {
        return (
          <div className={`needCard need${need.id}`}>
            <div className="needCard--header d-flex justify-content-between">
              <div className="d-block">
                <Link to={`/needs/${need.id}`}>
                  <h5>{need.title}</h5>
                </Link>
              </div>
              <div className="need-manage right d-flex flex-row">
                <div className="btn-group dropright">
                  <button
                    type="button"
                    className="btn btn-secondary dropdown-toggle"
                    data-display="static"
                    data-flip="false"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    •••
                  </button>
                  <div className="dropdown-menu dropdown-menu-right">
                    {need.is_owner && (
                      <Fragment>
                        <div onClick={() => changeMode("update")}>
                          <i className="fa fa-edit" />
                          <FormattedMessage id="feed.object.update" defaultMessage="Update" />
                        </div>
                        <NeedDelete need={need} refresh={refresh} />
                      </Fragment>
                    )}
                    <div onClick={() => copyLink(need.id, "needs")}>
                      {" "}
                      {/* on click, launch copyLink function */}
                      <i className="fa fa-link" />
                      <FormattedMessage id="need.copyLink" defaultMessage="Copy need Link" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* "CopyLink" confirmation alert (hidden by default) */}
            <Alert
              id="copyConfirmation"
              postId={need.id}
              type="success"
              message={<FormattedMessage id="need.copyLink.conf" defaultMessage="The need link has been copied" />}
            />
            <div className="d-flex">
              <ShareBtns type="need" needId={need.id} />
              {(need.is_urgent || need.status === "completed") && (
                <div className={`statusTag ${need.is_urgent} ${need.status} justify-content`}>
                  {need.status === "completed" && <FormattedMessage id="need.completed" defaultMessage="Complited" />}
                  {need.is_urgent && need.status !== "completed" && (
                    <FormattedMessage id="need.urgent" defaultMessage="Urgent" />
                  )}
                </div>
              )}
            </div>
            <div className="needProject">
              <FormattedMessage id="needsPage.project" defaultMessage="Project:" />
              <Link to={`/project/${need.project.id}#needs`}>
                {need.project.title} <i className="fa fa-external-link-alt" aria-hidden="true" />
              </Link>
            </div>
            <div className="needCard--content">
              {mode == "card" && (
                <div className="needContent">
                  {need.content.substring(0, 100)} {need.content.length > 100 && "..."}
                </div>
              )}
              {mode == "details" && (
                <div className="needContent" dangerouslySetInnerHTML={{ __html: contentWithLinks }} />
              )}
              {mode == "card" && <InfoSkillsComponent content={need.skills} place="entity_header" limit="3" />}
              {mode == "card" && <InfoRessourcesComponent content={need.ressources} place="entity_header" limit="3" />}
              {mode == "details" && (
                <Fragment>
                  <InfoSkillsComponent content={need.skills} place="entity_header" />
                  <InfoRessourcesComponent content={need.ressources} place="entity_header" />
                  <NeedWorkers need={need} mode="card" />
                  <NeedDocsManagement need={need} mode="details" />
                  <div className="needFeed">
                    <h6>
                      <FormattedMessage id="need.feed.title" defaultMessage="Discussion and results" />
                    </h6>
                    <div className="zoneFeed" style={!seeAllFeed ? { height: "350px" } : {}}>
                      <Feed
                        displayCreate={this.context.connected ? true : false}
                        feedId={need.feed_id}
                        isAdmin={need.is_owner}
                      />
                    </div>
                    <button className="btn btn-primary btnSee" onClick={() => this.changeDisplayFeed()}>
                      {seeAllFeed ? (
                        <FormattedMessage id="need.feed.seeLess" defaultMessage="See Less" />
                      ) : (
                        <FormattedMessage id="need.feed.seeMore" defaultMessage="See More" />
                      )}
                    </button>
                  </div>
                </Fragment>
              )}
            </div>

            <div className="needCard--footer">
              {mode == "card" && (
                <div className="needStats" onClick={() => this.toggleMode()}>
                  {posts_count} <FormattedMessage id="post.posts" defaultMessage="posts" />
                  {posts_count > 1 ? "s" : ""}
                  &nbsp;/ {comments_count} <FormattedMessage id="post.comments" defaultMessage="Comments" />
                  {comments_count > 1 ? "s" : ""}
                </div>
              )}
              <div className="d-flex justify-content-between">
                {!need.is_owner && (
                  <div className="interaction">
                    <BtnJoin
                      itemId={need.id}
                      itemType="needs"
                      joinState={need.is_member}
                      textJoin={<FormattedMessage id="need.help.willHelp" defaultMessage="I'll help" />}
                      textUnjoin={<FormattedMessage id="need.help.stopHelp" defaultMessage="I can't help" />}
                    />
                    <BtnFollow followState={need.has_followed} itemType="needs" itemId={need.id} />
                  </div>
                )}
                <button className="btn btn-outline-primary btnToggleMode" onClick={() => this.toggleMode()}>
                  {mode == "card" && <FormattedMessage id="need.card.seeMore" defaultMessage="See More" />}
                  {mode == "details" && <FormattedMessage id="need.card.seeLess" defaultMessage="See Less" />}
                </button>
              </div>
            </div>
          </div>
        );
      } else
        return (
          // different component if it's need from homepage (compact format)
          <div className={`needCard row need${need.id}`}>
            <div className="col-12 cardHeader">
              <div className="d-flex">
                <span className="needTitle">
									<Link to={`/needs/${need.id}`}>
	                  <h5>{need.title}</h5>
	                </Link>
                </span>
              </div>
              <div className="needContent">
                {need.content.substring(0, 100)}
                {need.content.length > 100 && "..."}
              </div>
              <div className="needProject">
                <FormattedMessage id="needsPage.project" defaultMessage="Project:" />
                <a href={`/project/${need.project.id}#needs`} target="_blank">
                  {need.project.title} <i className="fa fa-external-link-alt" aria-hidden="true" />
                </a>
              </div>
            </div>
          </div>
        );
    }
  }
}
NeedCard.contextType = UserContext;
