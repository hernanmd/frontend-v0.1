import React, { Component } from "react";
import Api from "Api";
import NeedCreate from "Components/Needs/Need/NeedCreate";
import NeedDisplay from "Components/Needs/Need/NeedDisplay";
import Loading from "Components/Tools/Loading";
import "./Needs.scss";

export default class Needs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      needs: [],
      loading: true,
    };
  }

  static get defaultProps() {
    return {
      displayCreate: false,
      itemId: undefined,
      itemType: undefined,
    };
  }

  getNeedsApi() {
    const { itemId, itemType } = this.props;
    if (!itemId || !itemType) {
    } else {
      {
        /* call different api url depending of it's a single need, or multiple needs from object */
      }
      var apiUrl =
        itemType === "single" ? `/api/needs/${this.props.match.params.id}` : `/api/${itemType}/${itemId}/needs`;
      Api.get(apiUrl)
        .then((res) => {
          {
            /* needs have different structure if it comes from a project or challenge/program for ex. So change needs value depending on it */
          }
          var needs = itemType === "projects" || itemType === "single" ? res.data : res.data.needs;
          this.setState({ needs, loading: false });
        })
        .catch((error) => {
          // console.log(error);
        });
    }
  }

  componentDidMount() {
    this.getNeedsApi();
  }

  refresh() {
    this.getNeedsApi();
  }

  displayNeeds(needs) {
    if (needs.length !== 0) {
      return needs.reverse().map((need, index) => (
        <div className="col-12 col-lg-6" key={index}>
          <NeedDisplay need={need} refresh={this.refresh.bind(this)} />
        </div>
      ));
    }
  }

  displaySingleNeed(needs) {
    return <NeedDisplay need={needs} refresh={this.refresh.bind(this)} mode="details" />;
  }

  render() {
    const { loading } = this.state;
    if (loading) {
      return <Loading height="300px" active={loading}></Loading>;
    }
    const { displayCreate, itemId, itemType } = this.props;
    const { needs } = this.state;
    if (itemType !== "single") {
      // if need is not a single need, display all needs
      return (
        <div className="needs">
          {displayCreate && <NeedCreate project_id={itemId} refresh={this.refresh.bind(this)} />}
          <div className="row">{itemType !== "single" && this.displayNeeds(needs)}</div>
        </div>
      );
    } else {
      // else return single need
      return <div className="needs single">{this.displaySingleNeed(needs)}</div>;
    }
  }
}
