import React, { Component } from "react";
import Api from "Api";
import ChangePwd from "Components/Authentification/ChangePwd";
import UserDelete from "./Account/UserDelete";
import UserDownloadInfos from "./Account/UserDownloadInfos";
import { FormattedMessage } from "react-intl";
import "./UserSettings.scss";

export default class UserSettings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: this.props.user,
    };
  }

  static get defaultProps() {
    return {
      user: {
        id: null,
        email: "",
        first_name: "",
        last_name: "",
        nickname: "",
        age: "",
        status: "",
        category: "",
        country: "",
        city: "",
        address: "",
        phone_number: "",
        bio: "",
        affiliation: "",
        sign_in_count: 0,
        confirmed_at: "",
        interests: [],
        skills: [],
        ressources: [],
        short_bio: "",
      },
    };
  }

  componentDidMount() {
    const userId = this.props.match.params.id;
    Api.get("/api/users/" + userId)
      .then((res) => {
        // console.log("User got : ", res.data);
        this.setState({ user: res.data, userLoaded: true });
      })
      .catch((error) => {
        // console.log("ERR : ",error);
      });
  }

  render() {
    return (
      <div className="userSettings">
        <div className="container justify-content-center">
          <h3 className="title">
            <FormattedMessage id="settings.title" defaultMessage="Settings" />
          </h3>
          <hr className="separator" />
          <div className="d-flex">
            <a
              className="btn btn-primary changePwdBtn"
              data-toggle="collapse"
              href="#changePwdBtn"
              role="button"
              aria-expanded="false"
              aria-controls="collapseExample"
            >
              <FormattedMessage id="auth.changePwd.title" defaultMessage="Edit" />
            </a>
            <div className="collapse" id="changePwdBtn">
              <ChangePwd />
              <hr />
            </div>
            <UserDownloadInfos />
            <UserDelete />
          </div>
        </div>
      </div>
    );
  }
}
