import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { Redirect } from "react-router-dom";
import Api from "Api";

export default class UserDownloadInfos extends Component {
  constructor(props) {
    super(props);
    this.state = {
      accountDelete: false,
      errors: "",
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event) {
    event.preventDefault();
    Api.get("/api/users/" + localStorage.getItem("userId"))
      .then((res) => {
        var personalDatas = res.data;
        var data = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(personalDatas));
        var a = document.querySelector(".dlHiddenLink");
        a.href = "data:" + data;
        a.download = "data.json";
        a.innerHTML = "download JSON";
        a.click();
      })
      .catch((error) => {
        // console.log(error);
      });
  }

  render() {
    return (
      <div>
        <button type="button" className="btn btn-primary dlInfosBtn" onClick={this.handleClick}>
          <FormattedMessage id="settings.account.dlinfos.text" defaultMessage="Download my personal information" />
        </button>
        <a href="#" className="dlHiddenLink"></a>
      </div>
    );
  }
}
