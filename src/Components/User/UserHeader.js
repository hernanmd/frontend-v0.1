import React, { Component, Fragment } from "react";
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";
import { UserContext } from "UserProvider";
import defaultImg from "assets/img/default/default-user.png";
import BtnFollow from "../Tools/BtnFollow";
import InfoSkillsComponent from "Components/Tools/Info/InfoSkillsComponent";
import InfoRessourcesComponent from "Components/Tools/Info/InfoRessourcesComponent";
import ListFollowers from "../Tools/ListFollowers";
import UserFollowings from "Components/User/Tabs/UserFollowings";
import ModalContactForm from "../Tools/ModalContactForm";
import $ from "jquery";
import "./UserHeader.scss";

export default class UserHeader extends Component {
  componentDidMount() {
    $(".skill.more").click(function () {
      // when click on the "+..." skills
      $('a[href="#about"]').click(); // force click on about tab
      var element = document.querySelector(`.tabContainer .infoSkills`); // get skills section (about tab)
      const y = element.getBoundingClientRect().top + window.pageYOffset - 160; // calculate it's top value and remove 140 of offset
      window.scrollTo({ top: y, behavior: "smooth" }); // scroll to section
    });
    $(".ressource.more").click(function () {
      // when click on the "+..." ressources
      $('a[href="#about"]').click(); // force click on about tab
      var element = document.querySelector(`.tabContainer .infoRessources`); // get ressources section (about tab)
      const y = element.getBoundingClientRect().top + window.pageYOffset - 160; // calculate it's top value and remove 140 of offset
      window.scrollTo({ top: y, behavior: "smooth" }); // scroll to section
    });
  }

  renderNetworkModal(type, userId) {
    var networkType = type === "followersModal" ? "followers" : "following";
    // defines selector for zone to close popup when clicking inside it
    // If it's followersModal, make selector the whole modal, else make it the tab-content, so we can click on navigation tabs also
    var modalContentSelector = type === "followersModal" ? `#${type}` : `#${type} .tab-content`;
    $("body").click(function (e) {
      // on click, check
      if ($(".networkModal").hasClass("show")) {
        // if modal is open
        if (
          (e.target.class == "networkModal" || $(e.target).parents(modalContentSelector).length) && // then, if we're clicking inside the "modalContentSelector" zone
          e.target.class !== "close"
        ) {
          // and if we're not cliking on the close button
          $(".networkModal").modal("hide"); // close popup
        }
      }
    });
    return (
      <div
        className="modal fade networkModal"
        id={type}
        tabIndex="-1"
        role="dialog"
        aria-labelledby="NetworkModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h3 className="modal-title" id="NetworkModalLabel">
                <FormattedMessage id={`user.profile.tab.${networkType}`} defaultMessage="Followers" />
              </h3>
              <button type="button" id="closeDeleteModal" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              {type === "followersModal" && <ListFollowers itemId={userId} itemType="users" />}
              {type === "followingsModal" && <UserFollowings userId={userId} />}
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    var { id, logo_url, first_name, last_name, bio, short_bio, follower_count, following_count } = this.props.user;
    const user = this.props.user;
    let userContext = this.context;
    follower_count = follower_count ? follower_count : 0;
    following_count = following_count ? following_count : 0;
    if (logo_url === "" || logo_url === undefined || logo_url === null) {
      logo_url = defaultImg;
    }
    const logoStyle = {
      backgroundImage: "url(" + logo_url + ")",
    };
    return (
      <div className="userHeader--top row">
        <div className="col-lg-2 col-12 d-none d-lg-block">
          <div className="userImg" style={logoStyle}></div>
        </div>

        <div className="col-md-7 col-12 userInfos">
          <div className="infoContainer">
            <div className="userSmallImg">
              <div style={logoStyle}></div>
            </div>
            <div className="firstRow">
              <div className="nameInfos">
                <h1 className="title">{first_name + " " + last_name}</h1>
                <p className="nickname">{"@" + user.nickname}</p>
              </div>
              <InfoSkillsComponent content={user.skills} limit="3" place="entity_card" />
              <InfoRessourcesComponent content={user.ressources} limit="3" place="entity_card" />
            </div>
          </div>
          <div className="userStats">
            {userContext.user && userContext.user.id !== id && (
              <Fragment>
                <BtnFollow
                  followState={user.has_followed}
                  itemType="users"
                  classBtn="btn btn-md btn-primary"
                  itemId={user.id}
                />
                {user.can_contact !== false && <ModalContactForm itemId={user.id} itemType="user" />}
              </Fragment>
            )}
            <div>
              <span className="text" data-toggle="modal" data-target="#followersModal">
                <strong>{follower_count}</strong>&nbsp;
                <FormattedMessage id="user.profile.tab.followers" defaultMessage="Followers" />
              </span>
              <span className="text" data-toggle="modal" data-target="#followingsModal">
                <strong>{following_count}</strong>&nbsp;
                <FormattedMessage id="user.profile.following" defaultMessage="Followings" />
              </span>
            </div>
          </div>
          <p className="about">{short_bio ? short_bio : bio}</p>
        </div>

        <div className="col-lg-3 col-md-5 col-12 userActions">
          {userContext.user && userContext.user.id === id && (
            <Link to={"/user/" + user.id + "/edit"}>
              <i className="fa fa-edit" />
              <FormattedMessage id="user.profile.edit.btnEdit" defaultMessage="Edit Profile" />
            </Link>
          )}
        </div>
        {follower_count
          ? this.renderNetworkModal("followersModal", user.id)
          : "" /* trigers popup only if user has followers */}
        {following_count
          ? this.renderNetworkModal("followingsModal", user.id)
          : "" /* trigers popup only if user has followings */}
      </div>
    );
  }
}
UserHeader.contextType = UserContext;
