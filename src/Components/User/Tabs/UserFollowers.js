import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import Api from "Api";
import Loading from "Components/Tools/Loading";
import PeopleList from "Components/People/PeopleList";
import "Components/User/UserProfile.scss";

export default class UserFollowers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listFollowers: [],
      loading: true,
    };
  }

  static get defaultProps() {
    return {
      userId: "",
    };
  }

  componentDidMount() {
    this.setState({ loading: true });
    Api.get("/api/users/" + this.props.userId + "/followers")
      .then((res) => {
        var listUser = [];
        // console.log(res);
        res.data.followers.map((user) => {
          return listUser.push(user);
        });
        this.setState({ listFollowers: listUser, loading: false });
      })
      .catch((error) => {
        // console.log(error);
        this.setState({ loading: false });
      });
  }

  render() {
    const { listFollowers, loading } = this.state;

    return (
      <div className="tab-pane" id="followers">
        <h3>
          <FormattedMessage id="user.profile.tab.followers" defaultMessage="Followers" />
        </h3>
        <Loading active={loading}>
          <PeopleList searchBar={false} listPeople={listFollowers} />
        </Loading>
      </div>
    );
  }
}
