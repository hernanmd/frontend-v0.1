import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { Redirect } from "react-router-dom";
import Alert from "Components/Tools/Alert";
import Api from "Api";
import Loading from "Components/Tools/Loading";
import DocumentsManager from "../Tools/Documents/DocumentsManager";
import ProjectForm from "./ProjectForm";
import MembersList from "../Members/MembersList";
import Needs from "Components/Needs/Needs";
import Hooks from "Components/Tools/Hooks/Hooks";
import $ from "jquery";
import BtnAdd from "../Tools/BtnAdd";
import { UserContext } from "UserProvider";
import { stickyTabNav, scrollToActiveTab } from "Components/Tools/utils/utils.js";

export default class ProjectEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      failedLoad: false,
      loading: true,
      project: this.props.project,
      projectUpdated: false,
      sending: false,
      objectDelete: false,
      errors: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    if (this.props.project) {
      this.setState({ loading: false });
    } else {
      const idProject = this.props.match.params.id;
      Api.get("/api/projects/" + idProject)
        .then((res) => {
          // console.log("Project got : ", res.data);
          this.setState({ project: res.data, loading: false });
        })
        .catch((error) => {
          // console.log(error);
          this.setState({ failedLoad: true, loading: false });
        });
    }
  }

  componentWillReceiveProps(nextProps) {
    const project = nextProps.project;
    this.setState({ project });
  }

  handleChange(key, content) {
    // console.log("Edit handleChange : " + key + " " + content);
    var newProject = this.state.project;
    newProject[key] = content;
    // console.log(newProject);
    this.setState({ project: newProject });
  }

  handleSubmit() {
    const project = this.state.project;
    this.setState({ sending: true });
    Api.patch("/api/projects/" + project.id, { project })
      .then((res) => {
        // console.log(res);
        this.setState({ projectUpdated: true, sending: false });
      })
      .catch((error) => {
        // console.log(error);
        this.setState({ sending: false });
      });
  }

  deleteProj() {
    var projectid = this.state.project.id;
    Api.delete("api/projects/" + projectid + "/")
      .then((res) => {
        // console.log("project deleted");
        $("#closeDeleteModal").click();
        this.setState({ objectDelete: true });
        // console.log(res.data);
      })
      .catch((error) => {
        // console.log(error);
        this.setState({ errors: error.toString() });
      });
  }

  renderDeleteModal(delBtnTitle, delBtnText) {
    const { errors } = this.state;
    var errorMessage = errors.includes("err-") ? (
      <FormattedMessage id={errors} defaultMessage="An error has occured" />
    ) : (
      errors
    );
    return (
      // <ModalDeleteObject errors={errors}/>
      <div
        className="modal fade"
        id="deleteObjectModal"
        tabIndex="-1"
        role="dialog"
        aria-labelledby="deleteObjectModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="deleteObjectModalLabel">
                <FormattedMessage id={delBtnTitle} defaultMessage="Delete project" />
              </h5>
              <button type="button" id="closeDeleteModal" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              {errors !== "" && <Alert type="danger" message={errorMessage} />}
              <p className="deleteModalMessage">
                <FormattedMessage id={delBtnText} />
              </p>
              <div className="btnGroup">
                <button type="button" className="btn btn-outline-danger" onClick={this.deleteProj.bind(this)}>
                  <FormattedMessage id="general.yes" defaultMessage="Yes" />
                </button>
                <button type="button" className="btn btn-success" data-dismiss="modal">
                  <FormattedMessage id="general.no" defaultMessage="No" />
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const { failedLoad, loading, project, projectUpdated, sending } = this.state;
    let userContext = this.context;
    if (project || (!project && failedLoad)) {
      // check if project exist, or if it doesn't
      var urlBack = `/project/${project.id}`;
      if (failedLoad || !userContext.user || !project.is_admin || this.state.objectDelete) {
        // redirect to projects page if not connected, not admin, if project doesn't exist, or was just deleted
        return <Redirect to="/projects" />;
      }
    }
    setTimeout(function () {
      stickyTabNav("isEdit"); // make the tab navigation bar sticky on top when we reach its scroll position
      scrollToActiveTab(); // if there is a hash in the url and the tab exists, click and scroll to the tab
    }, 700); // had to add setTimeout for the function to work
    if (projectUpdated) {
      return (
        // redirect to project page after saving the edit
        <Redirect push to={urlBack + "?success=1"} />
      );
    }
    if (project) {
      var delBtnTitle = project.members_count > 1 ? "project.archive.title" : "project.delete.title";
      var delBtnText = project.members_count > 1 ? "project.archive.text" : "project.delete.text";
    }
    return (
      <Loading active={loading}>
        <div className="projectEdit container-fluid justify-content-center">
          <h1>
            <FormattedMessage id="project.edit.title" defaultMessage="Edit my project" />
          </h1>
          <a href={urlBack}>
            {" "}
            {/* go back link*/}
            <i className="fa fa-arrow-left" />
            <FormattedMessage id="project.edit.back" defaultMessage="Go back" />
          </a>
          <nav className="nav nav-tabs container-fluid">
            <a className="nav-item nav-link active" href="#basic_info" data-toggle="tab">
              <FormattedMessage id="entity.tab.basic_info" defaultMessage="Basic information" />
            </a>
            <a className="nav-item nav-link" href="#members" data-toggle="tab">
              <FormattedMessage id="general.members" defaultMessage="Members" />
            </a>
            <a className="nav-item nav-link" href="#needs" data-toggle="tab">
              <FormattedMessage id="entity.card.needs" defaultMessage="Needs" />
            </a>
            <a className="nav-item nav-link" href="#documents" data-toggle="tab">
              <FormattedMessage id="entity.tab.documents" defaultMessage="Documents" />
            </a>
            <a className="nav-item nav-link" href="#advanced" data-toggle="tab">
              <FormattedMessage id="entity.tab.advanced" defaultMessage="Advanced" />
            </a>
          </nav>
          <div className="tabContainer">
            <div className="tab-content">
              <div className="tab-pane active" id="basic_info">
                <ProjectForm
                  handleChange={this.handleChange}
                  handleSubmit={this.handleSubmit}
                  mode="edit"
                  project={project}
                  sending={sending}
                />
              </div>
              <div className="tab-pane" id="members">
                {this.props.match.params.id ? (
                  <MembersList itemType="projects" itemId={this.props.match.params.id} />
                ) : null}
              </div>
              <div className="tab-pane" id="needs">
                {project && (
                  <Needs
                    displayCreate={this.context.connected ? true : false}
                    itemId={project.id}
                    itemType="projects"
                  />
                )}
              </div>
              <div className="tab-pane" id="documents">
                {project && <DocumentsManager item={project} itemId={project.id} itemType="projects" />}
              </div>
              <div className="tab-pane" id="advanced">
                <h5>
                  <FormattedMessage id="attach.myproject.title" />
                </h5>
                {project && <BtnAdd type="project" itemType="project" showText={true} itemId={project.id} />}
                <hr />
                <h5>
                  <FormattedMessage id="hook.setup" defaultMessage="Set up hooks" />
                </h5>
                <p className="hookExplain">
                  <FormattedMessage
                    id="hook.explanation"
                    defaultMessage={`Send custom message to any of you slack's channel, on certain trigger on your project (new need, new post, new member). For this, you will need a special hook url. Please follow {tutorial} to get this url`}
                    values={{
                      tutorial: (
                        <a href="https://api.slack.com/messaging/webhooks" target="_blank">
                          <FormattedMessage id="hook.tutorial" defaultMessage="this tutorial" />
                        </a>
                      ),
                    }}
                  />
                </p>
                <div className="hooksContainer">{project && <Hooks itemId={project.id} />}</div>
                <hr />
                <h5>
                  <FormattedMessage id="project.advancedParam" defaultMessage="Advanced parameters" />
                </h5>
                <div className="deleteBtns">
                  {project && project.members_count > 1 && (
                    <p>
                      <FormattedMessage id="project.delete.explain" />
                    </p>
                  )}
                  {project && (
                    <button
                      type="button"
                      className="btn btn-danger"
                      data-toggle="modal"
                      data-target="#deleteObjectModal"
                    >
                      <FormattedMessage id={delBtnTitle} />
                    </button>
                  )}
                  {project && project.members_count > 1 && (
                    <button type="button" className="btn btn-danger" disabled>
                      <FormattedMessage id="project.delete.title" de />
                    </button>
                  )}
                  {project && this.renderDeleteModal(delBtnTitle, delBtnText)}
                </div>
              </div>
            </div>
          </div>
        </div>
      </Loading>
    );
  }
}
ProjectEdit.contextType = UserContext;
