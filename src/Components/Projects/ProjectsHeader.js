import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import "../Main/Similar.scss";

export default class ProjectsHeader extends Component {
  render() {
    return (
      <div className="projectsHeader">
        <h1>
          <FormattedMessage id="projects.header.title" defaultMessage="Explore projects" />
        </h1>
        <p>
          <FormattedMessage
            id="projects.header.description"
            defaultMessage="Discover awesome projects around the world and join teams to make the world a better place."
          />
        </p>
        <hr />
      </div>
    );
  }
}
