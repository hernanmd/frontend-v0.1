import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import Api from "Api";
import ListComponent from "Components/Tools/ListComponent";
import Loading from "Components/Tools/Loading";
import ProjectsAttachedListBar from "./ProjectsAttachedListBar";
import "./ProjectsAttachedList.scss";

export default class ProjectsAttachedList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listProjects: [],
      failedLoad: false,
      loading: true,
    };
    this.refreshList = this.refreshList.bind(this);
  }

  static get defaultProps() {
    return {
      actionBar: true,
      itemType: "",
    };
  }

  componentWillMount() {
    this.getProjects();
  }

  getProjects() {
    if (this.props.itemId) {
      Api.get("/api/" + this.props.itemType + "/" + this.props.itemId + "/projects")
        .then((res) => {
          this.setState({ listProjects: res.data.projects, loading: false, gotProjects: true });
        })
        .catch((error) => {
          // console.log("ERR : ", error);
          this.setState({ failedLoad: true, loading: false });
        });
    }
  }

  refreshList() {
    this.getProjects();
  }

  render() {
    const { loading, listProjects, gotProjects } = this.state;
    if (!gotProjects) {
      return null;
    }
    return (
      <Loading active={loading} height="150px">
        {listProjects ? (
          <div className="projectsAttachedList">
            <h3 className="title">
              <FormattedMessage id="general.projects" defaultMessage="Projects" />
            </h3>
            <div className="col-12">
              {this.props.actionBar && (
                <ProjectsAttachedListBar
                  actualList={listProjects}
                  itemType={this.props.itemType}
                  itemId={this.props.itemId}
                  refreshList={this.refreshList}
                />
              )}
              <ListComponent itemType="projectAttached" data={listProjects} callBack={this.refreshList} colMax={1} />
            </div>
          </div>
        ) : (
          <div className="errorMessage text-center">Unable to load component</div>
        )}
      </Loading>
    );
  }
}
