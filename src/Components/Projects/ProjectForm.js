import React, { Component, Fragment } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import { Link } from "react-router-dom";
/*** Form objects ***/
import FormDefaultComponent from "Components/Tools/Forms/FormDefaultComponent";
import FormTextAreaComponent from "Components/Tools/Forms/FormTextAreaComponent";
import FormImgComponent from "Components/Tools/Forms/FormImgComponent";
import FormInterestsComponent from "Components/Tools/Forms/FormInterestsComponent";
import FormSkillsComponent from "Components/Tools/Forms/FormSkillsComponent";
import FormToggleComponent from "../Tools/Forms/FormToggleComponent";
// import FormDropdownComponent from "Components/Tools/Forms/FormDropdownComponent";
import FormWysiwygComponent from "Components/Tools/Forms/FormWysiwygComponent";
/*** Validators ***/
import FormValidator from "Components/Tools/Forms/FormValidator";
import projectFormRules from "./projectFormRules";
/*** Images/Style ***/
import DefaultImgProject from "assets/img/default/default-project.jpg";

class ProjectForm extends Component {
  validator = new FormValidator(projectFormRules);

  static get defaultProps() {
    return {
      mode: "create",
      project: {
        banner_url: "",
        description: "",
        interests: [],
        followers: 0,
        members: 0,
        short_description: "",
        short_title: "",
        skills: [],
        title: "",
        status: "",
        is_private: true, // force projects to be private (TEMP @TOFIX)
      },
      sending: false,
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      isChecked: false,
    };
  }

  handleToggleChange = () => {
    if (this.state.isChecked) {
      document.querySelector(".detailed-description").style.display = "none";
      document.querySelector(".basic-description").style.display = "block";
    } else {
      document.querySelector(".detailed-description").style.display = "block";
      document.querySelector(".basic-description").style.display = "none";
    }
    this.setState({
      isChecked: !this.state.isChecked,
    });
  };

  handleChange(key, content) {
    /* Validators start */
    // console.log(content);

    const state = {};
    state[key] = content;
    const validation = this.validator.validate(state);
    if (validation[key] !== undefined) {
      const stateValidation = {};
      stateValidation["valid_" + key] = validation[key];
      this.setState(stateValidation);
    }
    /* Validators end */
    this.props.handleChange(key, content);
  }

  handleSubmit(event) {
    event.preventDefault();
    /* Validators control before submit */
    var firsterror = true;
    const validation = this.validator.validate(this.props.project);
    if (validation.isValid) {
      this.props.handleSubmit();
    } else {
      const stateValidation = {};
      Object.keys(validation).forEach((key) => {
        if (key !== "isValid") {
          if (validation[key].isInvalid && firsterror) {
            // if field is invalid and it's the first field that has error
            var element = document.querySelector(`#${key}`); // get element that is not valid
            const y = element.getBoundingClientRect().top + window.pageYOffset - 140; // calculate it's top value and remove 25 of offset
            window.scrollTo({ top: y, behavior: "smooth" }); // scroll to element to show error
            firsterror = false; // set to false so that it won't scroll to second invalid field and further
          }
          stateValidation["valid_" + key] = validation[key];
        }
      });
      this.setState(stateValidation);
    }
  }

  renderBtnsForm() {
    const { mode, project, sending } = this.props;
    var urlBack = "/projects";
    var textAction = "completeProfile.btn.next";
    if (mode === "edit") {
      urlBack = "/project/" + project.id;
      textAction = "entity.form.btnUpdate";
    }

    return (
      <div className="row projectFormBtns">
        <Link to={urlBack}>
          <button type="button" className="btn btn-outline-primary" disabled={sending}>
            <FormattedMessage id="entity.form.btnCancel" defaultMessage="Cancel" />
          </button>
        </Link>
        <button type="submit" className="btn btn-primary" disabled={sending} style={{ marginRight: "10px" }}>
          {sending && (
            <Fragment>
              <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true"></span>
              &nbsp;
            </Fragment>
          )}
          <FormattedMessage id={textAction} defaultMessage={textAction} />
        </button>
      </div>
    );
  }

  render() {
    const { valid_title, valid_short_title, valid_short_description, valid_interests, valid_skills } = this.state
      ? this.state
      : "";
    var { project, mode, intl } = this.props;

    return (
      <form onSubmit={this.handleSubmit.bind(this)} className="projectForm">
        <FormDefaultComponent
          content={project.title}
          errorCodeMessage={valid_title ? valid_title.message : ""}
          id="title"
          isValid={valid_title ? !valid_title.isInvalid : undefined}
          mandatory={true}
          onChange={this.handleChange.bind(this)}
          placeholder={intl.formatMessage({ id: "project.form.title.placeholder" })}
          title={intl.formatMessage({ id: "entity.info.title" })}
        />
        <FormDefaultComponent
          content={project.short_title}
          errorCodeMessage={valid_short_title ? valid_short_title.message : ""}
          id="short_title"
          isValid={valid_short_title ? !valid_short_title.isInvalid : undefined}
          onChange={this.handleChange.bind(this)}
          mandatory={true}
          pattern={/[A-Za-z0-9]/g}
          placeholder={intl.formatMessage({ id: "project.form.short_title.placeholder" })}
          prepend="#"
          title={intl.formatMessage({ id: "entity.info.short_name" })}
        />
        <FormTextAreaComponent
          content={project.short_description}
          errorCodeMessage={valid_short_description ? valid_short_description.message : ""}
          id="short_description"
          isValid={valid_short_description ? !valid_short_description.isInvalid : undefined}
          mandatory={true}
          maxChar={140}
          onChange={this.handleChange.bind(this)}
          placeholder={intl.formatMessage({ id: "project.form.short_description.placeholder" })}
          row={3}
          title={intl.formatMessage({ id: "entity.info.short_description" })}
        />
        {mode === "edit" && (
          <Fragment>
            {/* <div className="material-switch">
							<span>Basic</span>
							<input id="collab-type" name="collaborator-switch" type="checkbox" checked={this.state.isChecked} onChange={this.handleToggleChange}/>
							<label htmlFor="collab-type" className="label-default"></label>
							<span>Detailed</span>
						</div> */}
            {/* <div className="basic-description"> */}
            <FormWysiwygComponent
              content={project.description}
              id="description"
              onChange={this.handleChange.bind(this)}
              show={true}
              placeholder={intl.formatMessage({ id: "project.form.description.placeholder" })}
              title={intl.formatMessage({ id: "entity.info.description" })}
            />
            {/* </div> */}
            {/* <div className="detailed-description"> */}
            <a
              className="btn btn-primary collapseDetailedInfo"
              data-toggle="collapse"
              href="#DetailedAbout"
              role="button"
              aria-expanded="false"
            >
              <FormattedMessage id="project.fillDetailedInfo" defaultMessage="Edit" />
            </a>
            <div className="collapse" id="DetailedAbout">
              <FormWysiwygComponent
                content={project.desc_elevator_pitch}
                id="desc_elevator_pitch"
                onChange={this.handleChange.bind(this)}
                show={true}
                placeholder={intl.formatMessage({ id: "project.form.desc_elevator_pitch.placeholder" })}
                title={intl.formatMessage({ id: "entity.info.desc_elevator_pitch" })}
              />
              <FormWysiwygComponent
                content={project.desc_contributing}
                id="desc_contributing"
                onChange={this.handleChange.bind(this)}
                show={true}
                placeholder={intl.formatMessage({ id: "project.form.desc_contributing.placeholder" })}
                title={intl.formatMessage({ id: "entity.info.desc_contributing" })}
              />
              <FormWysiwygComponent
                content={project.desc_problem_statement}
                id="desc_problem_statement"
                onChange={this.handleChange.bind(this)}
                show={true}
                placeholder={intl.formatMessage({ id: "project.form.desc_problem_statement.placeholder" })}
                title={intl.formatMessage({ id: "entity.info.desc_problem_statement" })}
              />
              <FormWysiwygComponent
                content={project.desc_objectives}
                id="desc_objectives"
                onChange={this.handleChange.bind(this)}
                show={true}
                placeholder={intl.formatMessage({ id: "project.form.desc_objectives.placeholder" })}
                title={intl.formatMessage({ id: "entity.info.desc_objectives" })}
              />
              <FormWysiwygComponent
                content={project.desc_state_art}
                id="desc_state_art"
                onChange={this.handleChange.bind(this)}
                show={true}
                placeholder={intl.formatMessage({ id: "project.form.desc_state_art.placeholder" })}
                title={intl.formatMessage({ id: "entity.info.desc_state_art" })}
              />
              <FormWysiwygComponent
                content={project.desc_progress}
                id="desc_progress"
                onChange={this.handleChange.bind(this)}
                show={true}
                placeholder={intl.formatMessage({ id: "project.form.desc_progress.placeholder" })}
                title={intl.formatMessage({ id: "entity.info.desc_progress" })}
              />
              <FormWysiwygComponent
                content={project.desc_stakeholder}
                id="desc_stakeholder"
                onChange={this.handleChange.bind(this)}
                show={true}
                placeholder={intl.formatMessage({ id: "project.form.desc_stakeholder.placeholder" })}
                title={intl.formatMessage({ id: "entity.info.desc_stakeholder" })}
              />
              <FormWysiwygComponent
                content={project.desc_impact_strat}
                id="desc_impact_strat"
                onChange={this.handleChange.bind(this)}
                show={true}
                placeholder={intl.formatMessage({ id: "project.form.desc_impact_strat.placeholder" })}
                title={intl.formatMessage({ id: "entity.info.desc_impact_strat" })}
              />
              <FormWysiwygComponent
                content={project.desc_ethical_statement}
                id="desc_ethical_statement"
                onChange={this.handleChange.bind(this)}
                show={true}
                placeholder={intl.formatMessage({ id: "project.form.desc_ethical_statement.placeholder" })}
                title={intl.formatMessage({ id: "entity.info.desc_ethical_statement" })}
              />
              <FormWysiwygComponent
                content={project.desc_sustainability_scalability}
                id="desc_sustainability_scalability"
                onChange={this.handleChange.bind(this)}
                show={true}
                placeholder={intl.formatMessage({ id: "project.form.desc_sustainability_scalability.placeholder" })}
                title={intl.formatMessage({ id: "entity.info.desc_sustainability_scalability" })}
              />
              <FormWysiwygComponent
                content={project.desc_communication_strat}
                id="desc_communication_strat"
                onChange={this.handleChange.bind(this)}
                show={true}
                placeholder={intl.formatMessage({ id: "project.form.desc_communication_strat.placeholder" })}
                title={intl.formatMessage({ id: "entity.info.desc_communication_strat" })}
              />
              <FormWysiwygComponent
                content={project.desc_funding}
                id="desc_funding"
                onChange={this.handleChange.bind(this)}
                show={true}
                placeholder={intl.formatMessage({ id: "project.form.desc_funding.placeholder" })}
                title={intl.formatMessage({ id: "entity.info.desc_funding" })}
              />
            </div>
            {/* </div> */}
          </Fragment>
        )}
        {mode === "edit" && (
          <FormImgComponent
            type="banner"
            id="banner_url"
            imageUrl={project.banner_url}
            itemId={project.id}
            itemType="projects"
            title={intl.formatMessage({ id: "project.info.banner_url" })}
            content={project.banner_url}
            defaultImg={DefaultImgProject}
            onChange={this.handleChange.bind(this)}
          />
        )}
        <FormInterestsComponent
          content={project.interests}
          errorCodeMessage={valid_interests ? valid_interests.message : ""}
          id="interests"
          isValid={valid_interests ? !valid_interests.isInvalid : undefined}
          mandatory={true}
          onChange={this.handleChange.bind(this)}
          title={intl.formatMessage({ id: "entity.info.interests" })}
        />
        <FormSkillsComponent
          content={project.skills}
          errorCodeMessage={valid_skills ? valid_skills.message : ""}
          id="skills"
          type="project"
          isValid={valid_skills ? !valid_skills.isInvalid : undefined}
          mandatory={true}
          onChange={this.handleChange.bind(this)}
          placeholder={intl.formatMessage({ id: "general.skills.placeholder" })}
          title={intl.formatMessage({ id: "entity.info.skills" })}
        />
        {/* {mode === "edit" &&
	        <FormDropdownComponent
	          id="status"
						title={intl.formatMessage({id:'entity.info.status' })}
	          content={project.status}
						// list={["active", "archived", "draft", "completed"]}
	          list={["draft", "soon", "active", "completed"]}
	          onChange={this.handleChange.bind(this)} />
        } */}
        {mode === "edit" && (
          <FormToggleComponent
            id="is_private"
            warningMsgId="project.info.publicPrivateToggleMsg"
            title={intl.formatMessage({ id: "entity.info.is_private" })}
            choice1={<FormattedMessage id="general.public" defaultMessage="Public" />}
            choice2={<FormattedMessage id="general.private" defaultMessage="Private" />}
            colorChoice1="#27B40E"
            colorChoice2="#F9530B"
            isChecked={project.is_private}
            onChange={this.handleChange.bind(this)}
          />
        )}
        {this.renderBtnsForm()}
      </form>
    );
  }
}

export default injectIntl(ProjectForm);
