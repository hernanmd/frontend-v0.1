import React, { Component } from "react";
import { Link } from "react-router-dom";
import Api from "Api";
import { FormattedMessage } from "react-intl";
import defaultProjectImg from "assets/img/default/default-project.jpg";
// import InfoSkillsComponent from "Components/Tools/Info/InfoSkillsComponent";
import BtnClap from "../Tools/BtnClap";
import { renderOwnerNames, renderTeam } from "Components/Tools/utils/utils.js";
import "../Main/Cards.scss";

export default class ProjectCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: "",
    };
  }

  static get defaultProps() {
    return {
      project: undefined,
      source: "api",
    };
  }

  render() {
    if (this.props.project !== undefined) {
      var {
        id,
        title,
        banner_url,
        short_description,
        short_title,
        has_clapped,
        claps_count,
        creator,
        needs_count,
        members_count,
        users_sm,
      } = this.props.project;
      if (banner_url === undefined || banner_url === null || banner_url === "") {
        banner_url = defaultProjectImg;
      }
      const projectImgStyle = { backgroundImage: "url(" + banner_url + ")" };
      if (this.props.cardFormat !== "compact") {
        return (
          <div className="card cardProject" key={id}>
            <Link to={"/project/" + id}>
              <div style={projectImgStyle} className="projectImg" />
            </Link>
            <BtnClap
              itemType="projects"
              itemId={id}
              clapState={has_clapped}
              source={this.props.source}
              clapCount={claps_count}
            />
            <div className="card-content">
              <Link to={"/project/" + id}>
                <h5 className="card-title">{title}</h5>
              </Link>
              <p className="card-shortname">#{short_title}</p>
              <p className="card-desc">{short_description}</p>
              {users_sm !== undefined && users_sm.length > 0 && renderOwnerNames(users_sm, creator)}
              {/* <InfoSkillsComponent place="entity_header" limit="3" content={skills} /> */}
            </div>
            <div className="cardfooter">
              <hr />
              {users_sm !== undefined && users_sm.length > 0 && renderTeam(users_sm, "project", id, members_count)}
              <p>
                <FormattedMessage id="entity.card.needs" defaultMessage="Needs" />: {needs_count}
              </p>
            </div>
          </div>
        );
      } else {
        return (
          <div className="card cardProject cardProjectSmall" key={id}>
            <Link to={"/project/" + id}>
              <div style={projectImgStyle} className="projectImg" />
            </Link>
            <BtnClap itemType="projects" itemId={id} clapState={has_clapped} clapCount={claps_count} />
            <div className="card-content">
              <Link to={"/project/" + id}>
                <h5 className="card-title">{title}</h5>
              </Link>
              <p className="card-desc">{short_description}</p>
            </div>
          </div>
        );
      }
    } else {
      return null;
    }
  }
}
