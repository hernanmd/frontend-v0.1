import React, { Component, Fragment } from "react";
import { FormattedMessage } from "react-intl";
import Api from "Api";
import Alert from "Components/Tools/Alert";
import { Link } from "react-router-dom";
import defaultImg from "assets/img/default/default-project.jpg";
import "./ProjectAttachedCard.scss";

export default class ProjectAttachedCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sending: "",
      error: "",
      itemType: "challenges",
      project: this.props.project,
    };
  }

  static get defaultProps() {
    return {
      project: undefined,
    };
  }

  changeStatusProject(newStatus) {
    const { itemType, project } = this.state;
    this.setState({ sending: { newStatus }, error: "" });

    Api.post("/api/" + itemType + "/" + project.challenge_id + "/projects/" + project.id, { status: newStatus })
      .then((res) => {
        var newProject = this.state.project;
        newProject["challenge_status"] = newStatus;
        this.setState({ project: newProject, sending: "" });
        this.props.callBack();
      })
      .catch((error) => {
        // console.log(error);
        this.setState({ sending: "", error: <FormattedMessage id="err-" defaultMessage="An error has occured" /> });
      });
  }

  removeProject() {
    const { itemType, project } = this.state;
    this.setState({ sending: "remove", error: "" });

    Api.delete("/api/" + itemType + "/" + project.challenge_id + "/projects/" + project.id)
      .then((res) => {
        // console.log(res);
        this.setState({ sending: "" });
        this.props.callBack();
      })
      .catch((error) => {
        // console.log(error);
        this.setState({ sending: "", error: <FormattedMessage id="err-" defaultMessage="An error has occured" /> });
      });
  }

  render() {
    const { error, project, sending } = this.state;

    if (project !== undefined) {
      var imgTodisplay = defaultImg;
      if (project.banner_url_sm !== "" && project.banner_url_sm !== null && project.banner_url_sm !== undefined) {
        imgTodisplay = project.banner_url_sm;
      }

      const bgBanner = {
        backgroundImage: "url(" + imgTodisplay + ")",
        backgroundSize: "cover",
        backgroundPosition: "center",
      };

      return (
        <div>
          <div className="row projectCard" key={project.id}>
            <div className="col-3 col-sm-2 col-lg-1 zoneLogo">
              <Link to={"/project/" + project.id}>
                <div className="logoProject" style={bgBanner}></div>
              </Link>
            </div>
            <div className="col-9 col-sm-6 zoneInfo">
              <Link to={"/project/" + project.id}>
                {project.title}
                <br />
              </Link>
              <span className="role">
                <FormattedMessage id="attach.status" defaultMessage="Status : " />
                <FormattedMessage id={`entity.info.status.${project.status}`} defaultMessage="Active" />
              </span>
            </div>
            <div className="col">
              <FormattedMessage id="attach.members" defaultMessage="Members : " />
              {project.members_count}
            </div>
            <div className="col col-right">
              {project.challenge_status === "pending" ? ( // if project status is pending, display the accept/reject buttons
                <Fragment>
                  <button
                    type="button"
                    className="btn btn-outline-success"
                    disabled={sending === "accept" ? true : false}
                    onClick={() => this.changeStatusProject("accept")}
                  >
                    {sending === "accept" && (
                      <Fragment>
                        <span
                          className="spinner-border spinner-border-sm text-center"
                          role="status"
                          aria-hidden="true"
                        ></span>
                        &nbsp;
                      </Fragment>
                    )}
                    <FormattedMessage id="general.accept" defaultMessage="Accept" />
                  </button>
                  <button
                    type="button"
                    className="btn btn-outline-danger"
                    disabled={sending === "remove" ? true : false}
                    onClick={() => this.removeProject()}
                  >
                    {sending === "remove" && (
                      <Fragment>
                        <span
                          className="spinner-border spinner-border-sm text-center"
                          role="status"
                          aria-hidden="true"
                        ></span>
                        &nbsp;
                      </Fragment>
                    )}
                    <FormattedMessage id="general.reject" defaultMessage="Reject" />
                  </button>
                </Fragment>
              ) : (
                // else display the remove button
                <button
                  type="button"
                  className="delete"
                  disabled={sending === "remove" ? true : false}
                  onClick={() => this.removeProject()}
                >
                  {sending === "remove" ? (
                    <Fragment>
                      <span
                        className="spinner-border spinner-border-sm text-center"
                        role="status"
                        aria-hidden="true"
                      ></span>
                      &nbsp;
                    </Fragment>
                  ) : (
                    "X "
                  )}
                  <FormattedMessage id="attach.remove" defaultMessage="remove" />
                </button>
              )}
              {error !== "" && <Alert type="danger" message={error} />}
            </div>
          </div>
          <hr />
        </div>
      );
    } else {
      return null;
    }
  }
}
