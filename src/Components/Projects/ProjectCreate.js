import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import { Redirect } from "react-router-dom";
import ProjectForm from "./ProjectForm";
import Api from "Api";

export default class ProjectCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isCreated: null,
      newProject: {
        title: "",
        short_title: "",
        logo_url: "",
        description: "",
        short_description: "",
        creator_id: Number(localStorage.getItem("userId")),
        status: "draft",
        interests: [],
        skills: [],
        banner_url: "",
        is_private: true, // force all new projects to be private (TEMP @TOFIX)
      },
      sending: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(key, content) {
    var updateProject = this.state.newProject;
    updateProject[key] = content;
    this.setState({ newProject: updateProject });
  }

  handleSubmit() {
    const newProject = this.state.newProject;
    // console.log("PROJET A CREER", newProject);
    this.setState({ sending: true });
    Api.post("/api/projects/", { project: newProject })
      .then((res) => {
        // console.log("PROJECT CREATED");
        // console.log(res);
        this.setState({ isCreated: res.data.id, sending: false });
      })
      .catch((error) => {
        // console.log(error);
        this.setState({ sending: false });
      });
  }

  render() {
    var { isCreated, newProject, sending, userInProject } = this.state;

    if (isCreated) {
      return <Redirect push to={"/project/" + isCreated + "/edit"} />;
    } else {
      return (
        <div className="projectCreate container-fluid">
          <h1>
            <FormattedMessage id="project.create.title" defaultMessage="Create a new project" />
          </h1>
          <ProjectForm
            mode="create"
            project={newProject}
            userInProject={userInProject}
            handleChange={this.handleChange}
            handleSubmit={this.handleSubmit}
            sending={sending}
          />
        </div>
      );
    }
  }
}
