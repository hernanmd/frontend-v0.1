import React, { Component } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
// import { copyLink } from 'Components/Tools/utils/utils.js'
import "./ShareBtns.scss";

class ShareBtns extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static get defaultProps() {
    return {
      type: "",
      needId: "",
      device: "desktop",
    };
  }

  componentDidMount() {
    var { type, device, needId } = this.props;
    var containerSelector = device === "mobile" ? ".shareBtns.mobile" : ".shareBtns"; // have different selector depending on if share button has different place on mobile (and appears twice)
    if (type === "need") containerSelector = `.need${needId} .shareBtns`; // special selector for share button on need card
    var shareButton = document.querySelector(`${containerSelector} .share-button`);
    var shareDialog = document.querySelector(`${containerSelector} .share-dialog`);
    var closeButton = document.querySelector(`${containerSelector} .close-button`);

    shareButton.addEventListener("click", (event) => {
      if (navigator.share) {
        navigator
          .share({
            title: `${type}`,
            text: `Check out this ${type} on the JOGL platform: `,
            url: window.location.href,
          })
          .catch(console.error);
      } else {
        shareDialog.classList.add("is-open");
      }
    });

    closeButton.addEventListener("click", (event) => {
      shareDialog.classList.remove("is-open");
    });
  }

  openWindow(socialMedia) {
    var { type, needId } = this.props;
    var currentUrl = encodeURIComponent(window.location.href);
    if (type === "need") currentUrl = `${window.location.origin}/needs/${needId}`;
    var shareUrl;
    if (socialMedia === "facebook") shareUrl = `https://www.facebook.com/sharer/sharer.php?u=${currentUrl}`;
    if (socialMedia === "twitter")
      shareUrl = `https://twitter.com/intent/tweet/?text=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform:&hashtags=JOGL&url=${currentUrl}`;
    if (socialMedia === "linkedin")
      shareUrl = `https://www.linkedin.com/shareArticle?mini=true&url=${currentUrl}&title=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform&amp;&source=${currentUrl}`;
    if (socialMedia === "reddit")
      shareUrl = `https://reddit.com/submit/?url=${currentUrl}&resubmit=true&amp;title=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform:&amp;`;
    if (socialMedia === "whatsapp")
      shareUrl = `https://api.whatsapp.com/send?text=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform:&amp;${currentUrl}`;
    if (socialMedia === "telegram")
      shareUrl = `https://telegram.me/share/url?text=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform:&amp;url=${currentUrl}`;
    console.log(shareUrl);
    window.open(shareUrl, "share-dialog", "width=626,height=436");
  }

  render() {
    var currentUrl = encodeURIComponent(window.location.href);
    var { type, intl, device } = this.props;
    var typeId = "";
    if (type === "project") typeId = "project";
    if (type === "challenge") typeId = "challenge";
    if (type === "group") typeId = "community";
    if (type === "program") typeId = "program";
    if (type === "need") typeId = "need";
    return (
      <div className={`shareBtns ${device === "mobile" && "mobile"}`}>
        {" "}
        {/* add mobileClass selector if it's mobile share button*/}
        <div className="share-dialog">
          <header>
            <h5 className="dialog-title">
              <FormattedMessage
                id="general.shareBtns.modalTitle"
                defaultMessage={`Share this {type}`}
                values={{ type: intl.formatMessage({ id: typeId }) }}
              />
            </h5>
            <button className="close-button">
              <svg>
                <use href="#close"></use>
              </svg>
            </button>
          </header>
          <div className="targets">
            <a
              className="resp-sharing-button__link"
              href="#"
              onClick={() => this.openWindow("facebook")}
              aria-label="Share on Facebook"
            >
              <div className="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--large">
                <i className="fab fa-facebook" /> Facebook
              </div>
            </a>
            <a
              className="resp-sharing-button__link"
              href="#"
              onClick={() => this.openWindow("twitter")}
              aria-label="Share on Twitter"
            >
              <div className="resp-sharing-button resp-sharing-button--twitter resp-sharing-button--large">
                <i className="fab fa-twitter" /> Twitter
              </div>
            </a>
            <a
              className="resp-sharing-button__link"
              href="#"
              onClick={() => this.openWindow("linkedin")}
              aria-label="Share on LinkedIn"
            >
              <div className="resp-sharing-button resp-sharing-button--linkedin resp-sharing-button--large">
                <i className="fab fa-linkedin" /> LinkedIn
              </div>
            </a>
            <a
              className="resp-sharing-button__link"
              href="#"
              onClick={() => this.openWindow("reddit")}
              aria-label="Share on Reddit"
            >
              <div className="resp-sharing-button resp-sharing-button--reddit resp-sharing-button--large">
                <i className="fab fa-reddit-alien" /> Reddit
              </div>
            </a>
            <a
              className="resp-sharing-button__link"
              href="#"
              onClick={() => this.openWindow("whatsapp")}
              aria-label="Share on WhatsApp"
            >
              <div className="resp-sharing-button resp-sharing-button--whatsapp resp-sharing-button--large">
                <i className="fab fa-whatsapp" /> Whatsapp
              </div>
            </a>
            <a
              className="resp-sharing-button__link"
              href="#"
              onClick={() => this.openWindow("telegram")}
              aria-label="Share on Telegram"
            >
              <div className="resp-sharing-button resp-sharing-button--telegram resp-sharing-button--large">
                <i className="fab fa-telegram-plane" /> Telegram
              </div>
            </a>
            <a
              className="resp-sharing-button__link"
              href={`mailto:?subject=Check%20out%20this%20${type}%20on%20the%20JOGL%20platform;&body=${currentUrl}`}
              target="_self"
              rel="noopener"
              aria-label="Share by E-Mail"
            >
              <div className="resp-sharing-button resp-sharing-button--email resp-sharing-button--large">
                <i className="fa fa-envelope" /> Email
              </div>
            </a>
            {/* <a className="resp-sharing-button__link" onClick={() => copyLink(need.id, type)}>
							<div className="resp-sharing-button resp-sharing-button--link resp-sharing-button--large">
								<i className="fa fa-link" /> Copy link
							</div>
						</a> */}
          </div>
          {/* <div className="link">
				    <div className="pen-url">https://codepen.io/ayoisaiah/pen/YbNazJ</div>
				    <button className="copy-link">Copy Link</button>
				  </div> */}
        </div>
        <button className="share-button" type="button" title="Share this article">
          {/* <svg>
				    <use href="#share-icon"></use>
				  </svg> */}
          <i className="fa fa-share-alt" />
          <span>{intl.formatMessage({ id: "general.shareBtns.btnTitle" })}</span>
        </button>
        <svg className="hidden">
          <defs>
            <symbol
              id="share-icon"
              viewBox="0 0 24 24"
              fill="none"
              stroke="currentColor"
              strokeWidth="2"
              strokeLinecap="round"
              strokeLinejoin="round"
              className="feather feather-share"
            >
              <path d="M4 12v8a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2v-8"></path>
              <polyline points="16 6 12 2 8 6"></polyline>
              <line x1="12" y1="2" x2="12" y2="15"></line>
            </symbol>
            <symbol
              id="close"
              viewBox="0 0 24 24"
              fill="none"
              stroke="currentColor"
              strokeWidth="2"
              strokeLinecap="round"
              strokeLinejoin="round"
              className="feather feather-x-square"
            >
              <rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect>
              <line x1="9" y1="9" x2="15" y2="15"></line>
              <line x1="15" y1="9" x2="9" y2="15"></line>
            </symbol>
          </defs>
        </svg>
      </div>
    );
  }
}
export default injectIntl(ShareBtns);
