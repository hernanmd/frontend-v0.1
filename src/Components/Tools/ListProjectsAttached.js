import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import Api from "Api";
import Loading from "Components/Tools/Loading";
import ProjectList from "Components/Projects/ProjectList";

export default class ListProjectsAttached extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listProjects: [],
      loading: true,
    };
  }

  static get defaultProps() {
    return {
      itemId: undefined,
      itemType: undefined,
    };
  }

  componentDidMount() {
    const { itemId, itemType } = this.props;
    this.getProjectsAttached(itemId, itemType);
  }

  componentWillReceiveProps(nextProps) {
    const { itemId, itemType } = nextProps;
    this.getProjectsAttached(itemId, itemType);
  }

  getProjectsAttached(itemId, itemType) {
    this.setState({ loading: true });
    Api.get("/api/" + itemType + "/" + itemId + "/projects")
      .then((res) => {
        // don't display draft challenges in program challenges tab
        var filteredProjects = res.data.projects.filter((project) => project.challenge_status !== "pending");
        this.setState({ listProjects: filteredProjects, loading: false });
      })
      .catch((error) => {
        this.setState({ loading: false });
      });
  }

  render() {
    const { listProjects, loading } = this.state;

    return (
      <div className="listProjectsAttached">
        {/* <h3><FormattedMessage id="general.projects" defaultMessage="Projects" /></h3> */}
        <Loading active={loading}>
          <ProjectList searchBar={false} listProjects={listProjects} />
        </Loading>
      </div>
    );
  }
}
