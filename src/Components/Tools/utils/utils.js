import React, { Fragment } from "react";
import { FormattedMessage } from "react-intl";
import { Link } from "react-router-dom";
import ListFollowers from "Components/Tools/ListFollowers";
import PeopleList from "Components/People/PeopleList";
import ListProjectsAttached from "Components/Tools/ListProjectsAttached";
import defaultLogoImg from "assets/img/default/default-user.png";
import $ from "jquery";
import Api from "Api";

export function linkify(content) {
  // detect links in a text and englobe them with a <a> tag
  var urlRegex = /\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))/gm;
  return content.replace(urlRegex, function (url) {
    var addHttp = url.substr(0, 4) !== "http" ? "http://" : "";
    return '<a href="' + addHttp + url + '" target="_blank">' + url + "</a>";
  });
}

export function linkifyQuill(text) {
  // detect links oustide of <a> tag and linkify them (only for quill content)
  var exp = /((href|src)=["']|)(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gi;
  return text.replace(exp, function () {
    return arguments[1] ? arguments[0] : '<a href="' + arguments[3] + "\" target='_blank'>" + arguments[3] + "</a>";
  });
}
export function stickyTabNav(isEdit) {
  // Get the tab navigation
  var navtabs = document.querySelector(".nav-tabs");
  // Get the offset position of the tab navigation
  if (navtabs) {
    // check if nav
    var sticky = navtabs.offsetTop - 80; // equals to the offset from top of navtab - 80px (header height)
    // When the user scrolls the page, execute stickyScrollTop
    stickyScrollTop(); // launch function once
    window.onscroll = function () {
      stickyScrollTop();
    }; // redo function on scroll
    // Add the sticky class to the tab navigation when you reach its scroll position. Remove "sticky" when you leave the scroll position
    function stickyScrollTop() {
      if (window.pageYOffset >= sticky) {
        // when we arrive at the level of the tab navigation div
        navtabs.classList.add("sticky"); // add class sticky
      } else {
        navtabs.classList.remove("sticky");
      }
    }
    $(".nav-tabs .nav-item.nav-link").on("click", function () {
      window.scrollTo({ top: sticky, behavior: "smooth" }); // stickyValue is dynamically 0 or sticky depending if the tab navigation is sticky
      if (!isEdit) window.location.hash = $(this)[0].hash;
    });
  }
}

export function scrollToActiveTab() {
  // takes the hash in the url, if it match the href of a tab, then force click on the tab to access content
  if (window.location.hash && document.body.contains(document.querySelector(`a[href="${window.location.hash}"]`))) {
    $(`a[href="${window.location.hash}"]`)[0].click();
    var element = document.querySelector(`.tab-pane.active`); // get active tab content
    const y = element.getBoundingClientRect().top + window.pageYOffset - 200; // calculate it's top value and remove 140 of offset
    window.scrollTo({ top: y, behavior: "smooth" }); // scroll to tab
  }
}

export function renderStatsModal(type, objType, obj) {
  var networkType = type === "followersModal" ? "followers" : "members";
  // select links to user
  var linkToUser = `#${type} .peopleCard a`;
  $("body").click(function (e) {
    // on click, check
    if ($(".statsModal").hasClass("show")) {
      // if modal is open
      if (
        (e.target.class == "statsModal" || $(e.target).parents(linkToUser).length) && // then, check if we are clicking on a linkToUser (else we have modal overlay on the clicked page)
        e.target.class !== "close"
      ) {
        // and if we're not cliking on the close button
        $(".statsModal").modal("hide"); // close popup
      }
    }
  });
  return (
    <div
      className="modal fade statsModal"
      id={type}
      tabIndex="-1"
      role="dialog"
      aria-labelledby="statsModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h3 className="modal-title" id="statsModalLabel">
              <FormattedMessage id={`entity.tab.${networkType}`} defaultMessage="Followers" />
            </h3>
            <button type="button" id="closeDeleteModal" className="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            {type === "followersModal" && <ListFollowers itemId={obj.id} itemType={objType} />}
            {type === "entityMembersModal" && (
              <PeopleList filter={true} itemId={obj.id} itemType={objType} searchBar={false} />
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export function renderChalProgStatsModal(type, objType, obj) {
  var networkType = type === "entityProjectsModal" ? "projects" : "participants";
  // defines selector for zone to close popup when clicking inside it
  // If it's entityProjectsModal, selector is the whole project card, else make it all links to user
  var modalLinkSelector = type === "entityProjectsModal" ? `#${type}` : `#${type} .peopleCard a`;
  $("body").click(function (e) {
    // on click, check
    if ($(".statsModal").hasClass("show")) {
      // if modal is open
      if (
        (e.target.class == "statsModal" || $(e.target).parents(modalLinkSelector).length) && // then, check if we are clicking in a place (modalLinkSelector) we want to force popup closing (else we have modal overlay on the clicked page)
        e.target.class !== "close"
      ) {
        // and if we're not cliking on the close button
        $(".statsModal").modal("hide"); // close popup
      }
    }
  });
  return (
    <div
      className="modal fade statsModal"
      id={type}
      tabIndex="-1"
      role="dialog"
      aria-labelledby="statsModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h3 className="modal-title" id="statsModalLabel">
              <FormattedMessage id={`entity.tab.${networkType}`} defaultMessage="Followers" />
            </h3>
            <button type="button" id="closeDeleteModal" className="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            {type === "entityProjectsModal" && <ListProjectsAttached itemId={obj.id} itemType={objType} />}
            {type === "participantsModal" && (
              <PeopleList filter={true} itemId={obj.id} itemType={objType} searchBar={false} />
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export function renderOwnerNames(users, projectCreator) {
  var noOwner = true;
  users.map((user) => {
    if (user.owner) noOwner = false;
  }); // map through users, and set noOwner to false if there are no owners
  return (
    <p className="card-by">
      <FormattedMessage id="entity.card.by" defaultMessage="by " />
      {users
        .filter((user) => user.owner)
        .map((user, index, users) => {
          // filter to get only owners, and then map
          const rowLen = users.length;
          if (index < 6) {
            return (
              <Fragment key={index}>
                <Link to={"/user/" + user.id}>{user.first_name + " " + user.last_name}</Link>
                {/* add comma, except for last item*/}
                {rowLen !== index + 1 && <span>, </span>}
              </Fragment>
            );
          } else if (index === 6) {
            return "...";
          }
          return "";
        })}
      {noOwner && ( // if project don't have any owner, display creator as the owner
        <Link to={"/user/" + projectCreator.id}>{projectCreator.first_name + " " + projectCreator.last_name}</Link>
      )}
    </p>
  );
}

export function renderTeam(users, objType, objId, members_count) {
  var filteredUsers = users.filter((user) => user.admin || user.creator || user.member); // hide pending members
  var member_naming = objType === "challenge" ? "participants" : "members"; // different naming if it's challenge or if it's any other object type
  return (
    <div className="bubbleTeam">
      <span>
        <FormattedMessage id={`entity.card.${member_naming}`} defaultMessage={member_naming} /> ({members_count}):{" "}
      </span>
      <span className="imgList">
        {filteredUsers.map((user, index) => {
          // map
          var logoUrl = !user.logo_url ? defaultLogoImg : user.logo_url;
          const logoStyle = {
            backgroundImage: "url(" + logoUrl + ")",
          };
          if (index < 6) {
            return (
              <Link to={"/user/" + user.id} key={index}>
                <div className="peopleImg" style={logoStyle}></div>
              </Link>
            );
          }
          return "";
        })}
        {members_count > 6 && (
          <Link to={"/" + objType + "/" + objId}>
            <div className="moreMembers">+{members_count - 6}</div>
          </Link>
        )}
      </span>
    </div>
  );
}

export function formatNumber(value) {
  return Number(value).toLocaleString();
}

export function infiniteLoader(type) {
  // force click on load more button when it appears in the page
  var counter = 0; // set counter to 0
  var loadMoreBtn = $(".btn.loadBtn"),
    hT = loadMoreBtn.offset().top, // element distance from top of page
    hH = loadMoreBtn.outerHeight(), // height of element
    wH = $(window).height(); // window height
  $(window).scroll(function () {
    var wS = $(window).scrollTop(); // current y scroll position in the page (in px)
    // if type is not feed, no condition. If it's a feed, check if we are in the news/feed tab
    if (
      type != "feed" ||
      (type == "feed" && document.body.contains(document.querySelector("#news.active .btn.loadBtn")))
    ) {
      // check if button exists
      if (wS > hT + hH - wH && counter == 0) {
        // execute only if element appears in the page
        loadMoreBtn.click(); // force clicking on load button
        counter++; // incremente counter to click only once
      }
    }
  });
}

// return first link of a post (the one we want the metadata from)
export function returnFirstLink(content) {
  if (content) {
    var regexLinks = /\b((?:[a-z][\w-]+:(?:\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’]))/gm; // match all links
    const match = content.match(regexLinks);
    if (match) {
      var firstLink = match[0];
      return firstLink;
    }
  }
}

export function copyLink(objectId, objectType) {
  // function to copy post link to user clipboard, we use a tweak because the browser don't like we force copy to clipboard
  var el = document.createElement("textarea"); // create textarea element
  el.value = `${window.location.origin}/${objectType}/${objectId}`; // make its value be the post single url
  el.setAttribute("readonly", "");
  el.style.position = "absolute"; // put some style that make it not visible
  el.style.left = "-9999px";
  document.body.appendChild(el); // add element to html (body)
  el.select(); // select the textarea text
  document.execCommand("copy"); // copy content to user clipboard
  document.body.removeChild(el); // remove element
  $(`.alert-${objectId}#copyConfirmation`).show(); // display "post was copied" alert
  setTimeout(function () {
    $(`.alert-${objectId}#copyConfirmation`).hide(300);
  }, 2000); // hide it after 2sec
}

export function reportContent(contentType, postId, commentId) {
  // function to send mail with the reported post to JOGL admin
  var alertId = contentType === "post" ? "reportPostConfirmation" : "reportCommentConfirmation";
  var param =
    contentType === "post"
      ? {
          // if it's post
          object: "Post report", // mail subject
          content: `The user reported the following post: ${window.location.origin}/post/${postId}`, // mail content, with reported post linkn
        }
      : {
          // if it's comment
          object: "Comment report", // mail subject
          content: `The user reported the following comment: ${window.location.origin}/post/${postId}/comment/${commentId}`, // mail content, with reported post linkn
        };
  Api.post("/api/users/2/send_email", param) // send it to user 2, which is JOGL admin user
    .then((res) => {
      $(`.alert-${postId}#${alertId}`).show(); // // display "post was reported" alert
      setTimeout(function () {
        $(`.alert-${postId}#${alertId}`).hide(300);
      }, 2000); // hide it after 2sec
    })
    .catch((error) => {});
}
