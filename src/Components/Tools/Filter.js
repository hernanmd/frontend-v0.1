import React, { Component } from "react";
import { injectIntl } from "react-intl";

class Filter extends Component {
  static get defaultProps() {
    return {
      list: [],
      type: "default",
    };
  }

  // filter list by several parameters
  filter(type) {
    let listToSort = this.props.list;
    let listType = this.props.type;
    // first remove active class from all
    document.querySelector(".active").classList.remove("active");
    // then add active class to the clicked element
    document.querySelector(`.${type}`).classList.add("active");
    switch (type) {
      case "pop": // by popularity (claps count)
        if (listType === "people") {
          listToSort.sort((a, b) => b.follower_count - a.follower_count); // if type is people, popularity filters by followers
        } else listToSort.sort((a, b) => b.claps_count - a.claps_count); // else it filters by clap
        break;
      case "date1": // by date (old to new), compare the ids
        listToSort.sort((a, b) => a.id - b.id);
        break;
      case "date2": // by date (new to old)
        listToSort.sort((a, b) => b.id - a.id);
        break;
      case "alpha1": // by Alphabetical order (A-Z)
        if (listType === "people") listToSort.sort((a, b) => a.last_name.localeCompare(b.last_name));
        // if type is people, it filters by last name
        else listToSort.sort((a, b) => a.title.localeCompare(b.title)); // else it filters by title
        break;
      case "alpha2": // by Alphabetical order (Z-A)
        if (listType === "people") listToSort.sort((a, b) => b.last_name.localeCompare(a.last_name));
        // if type is people, it filters by last name
        else listToSort.sort((a, b) => b.title.localeCompare(a.title)); // else it filters by title
        break;
      default:
        // default
        listToSort = this.props.list;
        break;
    }
    this.setState({ listToSort });
    this.props.callback(); // send data back to parent
  }

  render() {
    const { intl, type } = this.props;
    return (
      <div className="list-filter dropdown">
        <button
          className="btn btn-secondary dropdown-toggle"
          type="button"
          id="dropdownMenuButton"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
        >
          {intl.formatMessage({ id: "projects.filter.title" })}
        </button>
        {type === "default" && ( // if filter type if default (group, project..)
          <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <span className="dropdown-item date2 active" onClick={() => this.filter("date2")}>
              {intl.formatMessage({ id: "general.filter.object.date2" })}
            </span>
            <span className="dropdown-item date1" onClick={() => this.filter("date1")}>
              {intl.formatMessage({ id: "general.filter.object.date1" })}
            </span>
            <span className="dropdown-item alpha1" onClick={() => this.filter("alpha1")}>
              {intl.formatMessage({ id: "general.filter.object.alpha1" })}
            </span>
            <span className="dropdown-item alpha2" onClick={() => this.filter("alpha2")}>
              {intl.formatMessage({ id: "general.filter.object.alpha2" })}
            </span>
            <span className="dropdown-item pop" onClick={() => this.filter("pop")}>
              {intl.formatMessage({ id: "general.filter.pop" })}
            </span>
          </div>
        )}
        {type === "people" && ( // if filter type is people (different settings)
          <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <span className="dropdown-item alpha1 active" onClick={() => this.filter("alpha1")}>
              {intl.formatMessage({ id: "general.filter.people.alpha1" })}
            </span>
            <span className="dropdown-item alpha2" onClick={() => this.filter("alpha2")}>
              {intl.formatMessage({ id: "general.filter.people.alpha2" })}
            </span>
            <span className="dropdown-item date2" onClick={() => this.filter("date2")}>
              {intl.formatMessage({ id: "general.filter.people.date2" })}
            </span>
            <span className="dropdown-item date1" onClick={() => this.filter("date1")}>
              {intl.formatMessage({ id: "general.filter.people.date1" })}
            </span>
            <span className="dropdown-item pop" onClick={() => this.filter("pop")}>
              {intl.formatMessage({ id: "general.filter.pop" })}
            </span>
          </div>
        )}
      </div>
    );
  }
}
export default injectIntl(Filter);
