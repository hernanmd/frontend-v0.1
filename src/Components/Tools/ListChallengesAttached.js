import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import Api from "Api";
import Loading from "Components/Tools/Loading";
import ChallengeList from "Components/Challenges/ChallengeList";

export default class ListChallengesAttached extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listChallenges: [],
      loading: true,
    };
  }

  static get defaultProps() {
    return {
      itemId: undefined,
      itemType: undefined,
    };
  }

  componentDidMount() {
    const { itemId, itemType } = this.props;
    this.getChallengesAttached(itemId, itemType);
  }

  componentWillReceiveProps(nextProps) {
    const { itemId, itemType } = nextProps;
    this.getChallengesAttached(itemId, itemType);
  }

  getChallengesAttached(itemId, itemType) {
    this.setState({ loading: true });
    Api.get("/api/" + itemType + "/" + itemId + "/challenges")
      .then((res) => {
        // don't display draft challenges in program challenges tab
        var filteredChallenges = res.data.challenges.filter((challenge) => challenge.status !== "draft");
        this.setState({ listChallenges: filteredChallenges, loading: false });
      })
      .catch((error) => {
        this.setState({ loading: false });
      });
  }

  render() {
    const { listChallenges, loading } = this.state;

    return (
      <div className="listChallengesAttached">
        <h3>
          <FormattedMessage id="challenges" defaultMessage="Challenges" />
        </h3>
        <Loading active={loading}>
          <ChallengeList searchBar={false} listChallenges={listChallenges} />
        </Loading>
      </div>
    );
  }
}
