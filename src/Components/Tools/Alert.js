import React from "react";

const Alert = ({ message, type, id, postId }) => (
  <div className={`w-100 alert alert-${type} alert-${postId}`} id={id} role="alert">
    {message}
  </div>
);

export default Alert;
