import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import "../ListComponent.scss"; //TODO CHECK THAT THIS IS USEFUL ?

export default class InfoMaxCharComponent extends Component {
  render() {
    const content = this.props.content;
    const maxChar = this.props.maxChar;

    if (maxChar !== undefined) {
      var actualSize = 0;
      if (content !== null) {
        actualSize = content.length;
      }
      var style = { textAlign: "right", color: "#566573" };
      if (actualSize > maxChar) {
        style = { textAlign: "right", color: "red" };
        return (
          <div className="maxChar" style={style}>
            {actualSize}/{maxChar} max.
          </div>
        );
      } else if (actualSize > maxChar * 0.8) {
        style = { textAlign: "right", color: "#D4AC0D" };
        return (
          <div className="maxChar" style={style}>
            {actualSize}/{maxChar} max.
          </div>
        );
      } else if (actualSize > maxChar * 0.7) {
        return (
          <div className="maxChar" style={style}>
            {maxChar} <FormattedMessage id="form.maxChar" defaultMessage="characters max." />
          </div>
        );
      } else {
        return null;
      }
    } else {
      return null;
    }
  }
}
