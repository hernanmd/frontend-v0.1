import React, { Component } from "react";
import TitleInfo from "Components/Tools/TitleInfo";
import "assets/css/InfoToDevelopComponent.scss";
import { FormattedMessage } from "react-intl";

export default class InfoToDevelopComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      deploy: false,
    };
  }

  static get defaultProps() {
    return {
      title: "Title",
      content: "",
      shortContent: "",
      colSizeTitle: "col-3",
      colSizeContent: "col-9",
    };
  }

  handleClick() {
    this.setState({ deploy: !this.state.deploy });
  }

  renderDeploy(deploy, content, shortContent) {
    if (!deploy && content) {
      return (
        <button className="btn btn-link btnDeploy" onClick={() => this.handleClick()}>
          <FormattedMessage id="info.toDevelop.learnMore" defaultMessage="Learn More" />
        </button>
      );
    } else if (deploy && shortContent) {
      return (
        <button className="btn btn-link btnDeploy" onClick={() => this.handleClick()}>
          <FormattedMessage id="info.toDevelop.readLess" defaultMessage="Read less" />
        </button>
      );
    } else {
      return null;
    }
  }

  render() {
    const { title, content, shortContent, colSizeTitle, colSizeContent } = this.props;
    const deploy = this.state.deploy;

    if (content || shortContent) {
      return (
        <div className="row infoToDevelopComponent">
          <TitleInfo title={title} classSize={colSizeTitle} />
          <div className={colSizeContent + " content"}>
            {shortContent && !deploy && shortContent}
            {content && deploy && content}
            {content && !shortContent && content}
            {this.renderDeploy(deploy, content, shortContent)}
          </div>
        </div>
      );
    } else {
      return (
        <div className="row infoToDevelopComponent">
          <TitleInfo title={title} classSize={colSizeTitle} />
          <div className={colSizeContent + " noContent"}>
            <FormattedMessage id="info.toDevelop.noDescription" defaultMessage="No description" />
          </div>
        </div>
      );
    }
  }
}
