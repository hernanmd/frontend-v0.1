import React, { Component } from "react";
import "./InfoDefaultComponent.scss";
import TitleInfo from "Components/Tools/TitleInfo";

export default class InfoDefaultComponent extends Component {
  static get defaultProps() {
    return {
      title: "Title",
      content: "",
      prepend: "",
      colSizeTitle: "col-12 col-sm-3 col-md-2",
      colSizeContent: "col-12 col-sm-9 col-md-10",
      containsHtml: false,
    };
  }

  render() {
    const { title, content, prepend, colSizeTitle, colSizeContent, containsHtml } = this.props;

    let titleinfo = "";

    if (title) {
      titleinfo = <TitleInfo title={title} classSize={colSizeTitle} />;
    }

    if (content) {
      return (
        <div className="row infoDefault">
          {titleinfo}
          <div className={colSizeContent + " content"}>
            {!containsHtml ? prepend + content : <div dangerouslySetInnerHTML={{ __html: content }} />}
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}
