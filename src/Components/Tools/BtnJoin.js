import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import Api from "Api";
import $ from "jquery";

export default class BtnJoin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      joinState: this.props.joinState,
    };
  }

  static get defaultProps() {
    return {
      classBtn: "btn btn-primary btn-md",
      itemType: undefined,
      itemId: undefined,
      joinState: false,
      sending: false,
      textJoin: <FormattedMessage id="general.join" defaultMessage="Join team" />,
      textUnjoin: <FormattedMessage id="general.leave" defaultMessage="Leave team" />,
    };
  }

  componentWillReceiveProps(nextProps) {
    const { joinState, itemId } = nextProps;
    this.setState({ joinState, itemId });
  }

  changeStateJoin(action) {
    const { itemId, itemType } = this.props;
    if (!itemId || !itemType) {
      // console.log("Error : missed props");
      // console.log("- itemType : ", itemType);
      // console.log("- itemId : ", itemId);
    } else {
      this.setState({ sending: true });
      Api.put("/api/" + itemType + "/" + itemId + "/" + action)
        .then((res) => {
          // console.log("JOIN CHANGED");
          // console.log(res);
          this.setState({ joinState: !this.state.joinState, sending: false });
        })
        .catch((error) => {
          // console.log(error);
          this.setState({ sending: false });
        });
    }
    // if we join a challenge, force click on projects tab
    if (itemType === "challenges" && action === "join") {
      $("#entityProjectsModal").modal("show"); // trigger projects modal
    }
  }

  render() {
    const { joinState, sending } = this.state;
    const { classBtn, style, textJoin, textUnjoin } = this.props;

    if (joinState) {
      return (
        <button onClick={() => this.changeStateJoin("leave")} className={classBtn} style={style} disabled={sending}>
          {sending ? (
            <span>
              <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true"></span>
              &nbsp; {textUnjoin}
            </span>
          ) : (
            textUnjoin
          )}
        </button>
      );
    } else {
      return (
        <button onClick={() => this.changeStateJoin("join")} className={classBtn} style={style} disabled={sending}>
          {sending ? (
            <span>
              <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true"></span>
              &nbsp; {textJoin}
            </span>
          ) : (
            textJoin
          )}
        </button>
      );
    }
  }
}
