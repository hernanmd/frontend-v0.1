import React, { Component } from "react";
import Api from "Api";
import "./DocumentCardFeed.scss";
import mimetype2fa from "./mimetypes.js";

export default class DocumentsCardFeed extends Component {
  constructor(props) {
    super(props);
    this.state = {
      document: this.props.document,
      item: "",
      postObj: "",
      user: "",
    };
  }

  static get defaultProps() {
    return {
      document: {
        content_type: "image/jpg",
        filename: "MyImage.jpg",
        url: "https://something.com/image.jpg",
      },
      isEditing: "",
    };
  }

  componentWillReceiveProps(nextProps) {
    const { document } = nextProps;
    this.setState({ document });
  }

  deleteDocument(document) {
    const { postObj } = this.props;
    Api.delete("api/posts/" + postObj.id + "/documents/" + document.id)
      .then((res) => {
        // console.log(res);
        this.props.refresh();
      })
      .catch((error) => {
        // console.log(error);
      });
  }

  // deleteDocument(document){
  // 	Api.delete('/api/posts/'+this.props.postObj.id+'/documents/'+document.id)
  //   .then((res) => {
  // 		if (res.status === 200) {
  //        this.props.refresh()
  //     } else {
  //       console.log("An error has occured");
  //     }
  //   })
  //   .catch(error => {
  //     // console.log(error);
  // 	});
  // }

  render() {
    const document = this.state.document;
    const { postObj, user, isEditing } = this.props;
    return (
      <div className="documentCardFeed col-12 col-sm-6">
        <div className="card-body row">
          <i className={"fa fa-" + mimetype2fa(document.content_type)}></i>
          {document.content_type === "image/jpeg" ? (
            // if document is a jpg image, make it hoverable with the image appearing in a popup
            <div className="hover_img">
              <a href={document.url} target="_blank" rel="noopener noreferrer">
                {document.filename}
                <span>
                  <img src={document.url} alt={document.filename} width="280" />
                </span>
              </a>
            </div>
          ) : (
            // else just display the document
            <a href={document.url} target="_blank" rel="noopener noreferrer">
              {document.filename}
            </a>
          )}
          {/* {(user.id === postObj.creator.id && isEditing) && // restrict deletion of documents only to post creator
						<i className="fa fa-times" onClick={this.deleteDocument(document)}></i>
					} */}
        </div>
      </div>
    );
  }
}
