import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import Api from "Api";
import BtnUploadFile from "Components/Tools/BtnUploadFile";
import DocumentsList from "Components/Tools/Documents/DocumentsList";
import Loading from "Components/Tools/Loading";

export default class DocumentsManager extends Component {
  constructor(props) {
    super(props);
    this.state = {
      failedLoad: false,
      item: this.props.item,
      loading: true,
    };
  }

  componentWillMount() {
    if (this.props.item) {
      // console.log("ITEM RECU");
      this.setState({ loading: false });
    } else {
      this.getItemApi();
    }
  }

  componentDidMount() {
    this.getItemApi();
  }

  refresh() {
    this.getItemApi();
  }

  getItemApi() {
    const { itemId, itemType } = this.props;
    if (!itemId || !itemType) {
      // console.log("parameters is missing");
    } else {
      this.setState({ loading: true });
      Api.get("/api/" + itemType + "/" + itemId)
        .then((res) => {
          // console.log("Item got : ", res.data);
          if (res.data) {
            this.setState({ item: res.data, loading: false });
          } else {
            this.setState({ failedLoad: true, loading: false });
          }
        })
        .catch((error) => {
          // console.log("ERR : ", error);
          this.setState({ failedLoad: true, loading: false });
        });
    }
  }

  render() {
    const { item, loading } = this.state;
    const { itemId, itemType } = this.props;
    return (
      <Loading active={loading}>
        {item && (
          <div className="documentsManager">
            {item.is_admin && (
              <BtnUploadFile
                itemId={itemId}
                itemType={itemType}
                multiple={true}
                refresh={this.refresh.bind(this)}
                text={<FormattedMessage id="info-1003" defaultMessage="Choose a file" />}
                type="documents"
                uploadNow={true}
              />
            )}
            <DocumentsList
              documents={item.documents}
              cardtype="cards"
              refresh={this.refresh.bind(this)}
              item={item}
              itemType={itemType}
            />
          </div>
        )}
      </Loading>
    );
  }
}
