import React, { Component, Fragment } from "react";
import { FormattedMessage } from "react-intl";
import MembersModal from "Components/Members/MembersModal";
import ModalAddProject from "Components/Tools/ModalAddProject";
import ModalAddChallenge from "Components/Tools/ModalAddChallenge";

export default class BtnAdd extends Component {
  static get defaultProps() {
    return {
      actualList: [],
      itemId: undefined,
      itemType: "",
      type: "",
      refreshList: () => console.log("props is missing"),
      text: "",
      showText: true,
    };
  }

  render() {
    const { actualList, itemId, itemType, refreshList, type, showText } = this.props;
    if (type === "member") {
      return (
        <Fragment>
          <button className="btn btn-md btn-primary" data-toggle="modal" data-target="#membersModal">
            <FormattedMessage id="member.btnNewMembers" defaultMessage="Add new members" />
          </button>
          <MembersModal itemType={itemType} itemId={itemId} />
        </Fragment>
      );
    } else if (type === "project") {
      return (
        <Fragment>
          <button className="btn btn-md btn-primary" data-toggle="modal" data-target="#modalAddProject">
            {showText && itemType == "challenges" && (
              <FormattedMessage id="attach.project.title" defaultMessage="Submit a project" />
            )}
            {showText && itemType == "project" && (
              <FormattedMessage id="attach.myproject.btn" defaultMessage="Submit my project" />
            )}
          </button>
          <ModalAddProject actualList={actualList} itemType={itemType} itemId={itemId} refreshList={refreshList} />
        </Fragment>
      );
    } else {
      return (
        <Fragment>
          <button className="btn btn-md btn-primary" data-toggle="modal" data-target="#modalAddChallenge">
            <FormattedMessage id="attach.challenge.button.title" defaultMessage="Submit a challenge" />
          </button>
          <ModalAddChallenge actualList={actualList} itemType={itemType} itemId={itemId} refreshList={refreshList} />
        </Fragment>
      );
    }
  }
}
