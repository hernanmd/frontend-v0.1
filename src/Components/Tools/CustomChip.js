import React from "react";
import "./CustomChip.scss";

const CustomChip = ({ value, onRemove, prepend }) => {
  return (
    <div className="customChip">
      <span className="text">
        {prepend}
        {value}
      </span>
      <span className="toRemove close" onClick={onRemove}>
        &times;
      </span>
    </div>
  );
};

export default CustomChip;
