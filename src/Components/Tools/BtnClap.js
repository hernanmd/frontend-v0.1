import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import Api from "Api";
import "./BtnClap.scss";

export default class BtnClap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clapState: this.props.clapState,
      clapCount: this.props.clapCount,
      sending: false,
    };
  }

  static get defaultProps() {
    return {
      clapState: false,
      clapCount: 0,
      itemId: undefined,
      itemType: undefined,
      type: "btn",
      source: "api",
    };
  }

  componentWillReceiveProps(nextProps) {
    const { clapNb, clapState, itemId, clapCount } = nextProps;
    this.setState({ clapNb, clapState, itemId, clapCount });
  }

  getApiClapstate() {
    const { itemId, itemType } = this.props;
    Api.get(`/api/${itemType}/${itemId}`)
      .then((res) => {
        this.setState({ clapState: res.data.has_clapped });
      })
      .catch((error) => {});
  }

  componentDidMount() {
    // if data also comes from algolia (and not only api), get clap state from api (because it's not available in algolia)
    if (this.props.source === "algolia") this.getApiClapstate();
  }

  changeStateClap(action) {
    const { itemId, itemType } = this.props;
    if (!itemId || !itemType) {
    } else {
      this.setState({ sending: true });
      const clapCount = this.state.clapState ? this.state.clapCount - 1 : this.state.clapCount + 1;
      if (action == "clap") {
        Api.put("/api/" + itemType + "/" + itemId + "/clap")
          .then((res) => {
            this.setState({ clapCount, clapState: true, sending: false });
          })
          .catch((error) => {
            this.setState({ sending: false });
          });
      } else {
        Api.delete("/api/" + itemType + "/" + itemId + "/clap")
          .then((res) => {
            this.setState({ clapCount, clapState: false, sending: false });
          })
          .catch((error) => {
            this.setState({ sending: false });
          });
      }
      if (this.props.refresh !== undefined) {
        this.props.refresh();
      }
    }
  }

  render() {
    const { clapState, sending, clapCount } = this.state;
    const { type } = this.props;
    var action = clapState ? "unclap" : "clap";

    switch (type) {
      case "text":
        return (
          <div
            onClick={() => this.changeStateClap(action)}
            className={clapState ? "btn-postcard btn text-primary" : "btn-postcard btn text-dark"}
            disabled={sending}
          >
            {sending ? (
              <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true">
                {" "}
                &nbsp;
              </span>
            ) : (
              <i className="fa fa-sign-language" />
            )}
            <FormattedMessage id="post.clap" defaultMessage="Clap" />
          </div>
        );
      default:
        return (
          <button
            onClick={() => this.changeStateClap(action)}
            className={
              clapState ? "btn-clap btn btn-sm btn-primary text-light" : "btn-clap btn btn-sm btn-secondary text-dark"
            }
            id="btnClap"
            disabled={sending}
          >
            <i className="fa fa-sign-language" />
            <span>{clapCount}</span>
            {sending && (
              <span className="spinInsideClap">
                <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true"></span>
              </span>
            )}
          </button>
        );
    }
  }
}
