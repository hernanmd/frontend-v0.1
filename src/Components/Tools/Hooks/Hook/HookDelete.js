import React, { Component } from "react";
import Api from "Api";

export default class HookDelete extends Component {
  static get defaultProps() {
    return {
      hook: undefined,
      refresh: () => console.log("Missing function"),
    };
  }

  deleteHook() {
    const { hook, refresh, hookProjectId } = this.props;
    if (hook === undefined) {
    } else {
      Api.delete(`api/projects/${hookProjectId}/hooks/${hook.id}`)
        .then((res) => {
          refresh("hardReload"); // temp fix to force page reload, to render correct list of hooks
          // refresh();
        })
        .catch((error) => {
          // console.log(error);
        });
    }
  }

  render() {
    return (
      <div onClick={this.deleteHook.bind(this)}>
        <i className="fa fa-trash postDelete" />
      </div>
    );
  }
}
