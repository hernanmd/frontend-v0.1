import React, { Component, Fragment } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import { UserContext } from "UserProvider";
import FormDefaultComponent from "Components/Tools/Forms/FormDefaultComponent";
/*** Validators ***/
import FormValidator from "Components/Tools/Forms/FormValidator";
import HookFormRules from "./HookFormRules";
/*** Images/Style ***/
import "./HookForm.scss";

class HookForm extends Component {
  validator = new FormValidator(HookFormRules);

  static get defaultProps() {
    return {
      action: "create",
      cancel: () => console.log("Missing function"),
      handleChange: () => console.log("Missing function"),
      handleSubmit: () => console.log("Missing function"),
      hook: undefined,
      refresh: () => console.log("Missing function"),
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      isCheckedPost: this.props.hook.trigger_post,
      isCheckedNeed: this.props.hook.trigger_need,
      isCheckedMember: this.props.hook.trigger_member,
    };
  }

  // handleChange(event){
  // 	console.log(event);
  //   const key = event.target.id;
  //   const content = event.target.content;
  //   this.props.handleChange(key, content);
  // }

  // handleChange(key, content){
  //   this.props.handleChange(key, content);
  // }

  handleChange(key, content) {
    /* Validators start */
    const state = {};
    state[key] = content;
    const validation = this.validator.validate(state);
    if (validation[key] !== undefined) {
      const stateValidation = {};
      stateValidation["valid_" + key] = validation[key];
      this.setState(stateValidation);
    }
    /* Validators end */
    this.props.handleChange(key, content);
  }

  handleChangeToggle(event) {
    var key = event.target.id;
    var content;
    if (key === "trigger_post") {
      content = this.state.isCheckedPost;
      this.setState({ isCheckedPost: !this.state.isCheckedPost });
    }
    if (key === "trigger_need") {
      content = this.state.isCheckedNeed;
      this.setState({ isCheckedNeed: !this.state.isCheckedNeed });
    }
    if (key === "trigger_member") {
      content = this.state.isCheckedMember;
      this.setState({ isCheckedMember: !this.state.isCheckedMember });
    }
    this.props.handleChange(key, content);
  }

  handleSubmit(event) {
    event.preventDefault();
    /* Validators control before submit */
    const validation = this.validator.validate(this.props.hook.hook_params);
    if (validation.isValid) {
      this.props.handleSubmit();
    } else {
      const stateValidation = {};
      Object.keys(validation).forEach((key) => {
        if (key !== "isValid") {
          stateValidation["valid_" + key] = validation[key];
        }
      });
      this.setState(stateValidation);
    }
  }

  // renderInvalid(valid_obj){
  // 	console.log("invalid", valid_obj);
  //   if(valid_obj){
  // 		console.log("valid?", valid_obj);
  //     if(valid_obj.message !== ""){
  // 			console.log("message", valid_obj);
  //       return (
  //         <div className="invalid-feedback">
  //           <FormattedMessage id={valid_obj.message} defaultMessage="Value is not valid" />
  //         </div>
  //       );
  //     }
  //   }
  //   return null;
  // }

  render() {
    const { action, hook, intl } = this.props;
    const { valid_hook_url, valid_username, valid_channel, isCheckedPost, isCheckedNeed, isCheckedMember } = this.state
      ? this.state
      : "";
    var submitBtnText = action === "create" ? "Create" : "Update";
    return (
      <div className="hookForm">
        <div className="formContainer">
          <div className="triggers">
            <h6>
              <FormattedMessage id="hook.triggerOn" defaultMessage="Trigger for each" />
            </h6>
            <div className="newPost">
              <input
                type="checkbox"
                name="checkbox"
                id="trigger_post"
                checked={isCheckedPost}
                onChange={this.handleChangeToggle.bind(this)}
              />
              <label htmlFor="newPost">
                <FormattedMessage id="hook.trigger.post" defaultMessage="new post" />
              </label>
            </div>
            <div className="newNeed">
              <input
                type="checkbox"
                name="checkbox"
                id="trigger_need"
                checked={isCheckedNeed}
                onChange={this.handleChangeToggle.bind(this)}
              />
              <label htmlFor="newNeed">
                <FormattedMessage id="hook.trigger.need" defaultMessage="new need" />
              </label>
            </div>
            <div className="newMember">
              <input
                type="checkbox"
                name="checkbox"
                id="trigger_member"
                checked={isCheckedMember}
                onChange={this.handleChangeToggle.bind(this)}
              />
              <label htmlFor="newMember">
                <FormattedMessage id="hook.trigger.member" defaultMessage="new member" />
              </label>
            </div>
          </div>
          <div className="parameters">
            <h6>
              <FormattedMessage id="hook.parameters" defaultMessage="Parameters" />
            </h6>
            <FormDefaultComponent
              id="hook_url"
              title={intl.formatMessage({ id: "hook.parameters.slackHookUrl", defaultMessage: "Parameters" })}
              placeholder={intl.formatMessage({
                id: "hook.parameters.slackHookUrl.placeholder",
                defaultMessage: "https://hooks.slack.com/services/...",
              })}
              content={hook.hook_params.hook_url}
              onChange={this.handleChange.bind(this)}
              isValid={valid_hook_url ? !valid_hook_url.isInvalid : undefined}
              mandatory={true}
            />
            {/* {this.renderInvalid(valid_hook_url)} */}
            <FormDefaultComponent
              id="channel"
              title={intl.formatMessage({ id: "hook.parameters.channel", defaultMessage: "Channel" })}
              placeholder={intl.formatMessage({
                id: "hook.parameters.channel.placeholder",
                defaultMessage: "#general",
              })}
              content={hook.hook_params.channel}
              onChange={this.handleChange.bind(this)}
              isValid={valid_channel ? !valid_channel.isInvalid : undefined}
              mandatory={true}
            />
            {/* {this.renderInvalid(valid_channel)} */}
            <FormDefaultComponent
              id="username"
              title={intl.formatMessage({ id: "hook.parameters.username", defaultMessage: "Username" })}
              placeholder={intl.formatMessage({
                id: "hook.parameters.username.placeholder",
                defaultMessage: "JOGL bot",
              })}
              content={hook.hook_params.username}
              onChange={this.handleChange.bind(this)}
              isValid={valid_username ? !valid_username.isInvalid : undefined}
              mandatory={true}
            />
            {/* {this.renderInvalid(valid_username)} */}
          </div>
        </div>
        <div className="actionBar">
          <button className="btn btn-primary" onClick={this.handleSubmit.bind(this)} type="submit">
            <FormattedMessage id={"post.create.btn" + submitBtnText} defaultMessage={submitBtnText} />
          </button>
          <button
            type="button"
            className="btn btn-outline-primary cancel"
            onClick={() => {
              this.props.cancel();
            }}
          >
            <FormattedMessage id="entity.form.btnCancel" defaultMessage="Cancel" />
          </button>
        </div>
      </div>
    );
  }
}
export default injectIntl(HookForm);
HookForm.contextType = UserContext;
