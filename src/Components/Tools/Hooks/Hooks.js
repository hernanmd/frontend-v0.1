import React, { Component } from "react";
import Api from "Api";
import HookCreate from "Components/Tools/Hooks/Hook/HookCreate";
import HookCard from "Components/Tools/Hooks/Hook/HookCard";
import Loading from "Components/Tools/Loading";
import "./Hooks.scss";

export default class Hooks extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hooks: [],
      loading: true,
    };
  }

  static get defaultProps() {
    return {};
  }

  getHooksApi() {
    const { itemId } = this.props;
    if (!itemId) {
    } else {
      Api.get(`/api/projects/${itemId}/hooks`)
        .then((res) => {
          // this.setState({hooks: ""});
          var hooks = res.data.reverse();
          // console.log(hooks);
          this.setState({ hooks, loading: false });
        })
        .catch((error) => {});
    }
  }

  componentDidMount() {
    this.getHooksApi();
  }

  refresh(refresh) {
    this.getHooksApi();
    refresh === "hardReload" && window.location.reload(true); // temp fix
  }

  render() {
    const { loading } = this.state;
    if (loading) {
      return <Loading height="300px" active={loading}></Loading>;
    }
    const { itemId } = this.props;
    const { hooks } = this.state;
    return (
      <div className="hooks">
        {/* <HookCreate projectId={itemId} refresh={this.refresh.bind(this)} /> */}
        {/* temp fix to refresh page on new hook creation (except if it's first hook created) */}
        {hooks.length == 0 && <HookCreate projectId={itemId} refresh={this.refresh.bind(this)} />}
        {hooks.length > 0 && <HookCreate projectId={itemId} refresh={this.refresh.bind(this, "hardReload")} />}
        <div className="row">
          {hooks.map((hook, index) => (
            <div className="col-12 col-lg-6" key={index}>
              <HookCard hook={hook} hookProjectId={itemId} mode="display" refresh={this.refresh.bind(this)} />
            </div>
          ))}
        </div>
      </div>
    );
  }
}
