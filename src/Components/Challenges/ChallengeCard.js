import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import defaultChallengeImg from "assets/img/default/default-challenge.jpg";
import defaultLogoImg from "assets/img/default/default-user.png";
import { renderOwnerNames, renderTeam } from "Components/Tools/utils/utils.js";
import BtnClap from "../Tools/BtnClap";
import "../Main/Cards.scss";

export default class ChallengeCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      challenge: this.props.challenge,
      source: "api",
    };
  }

  static get defaultProps() {
    return {
      challenge: undefined,
    };
  }

  render() {
    if (this.props.challenge !== undefined) {
      // console.log(this.props.challenge);
      var {
        id,
        title,
        title_fr,
        banner_url,
        short_description,
        short_description_fr,
        users_sm,
        has_clapped,
        claps_count,
        projects_count,
        members_count,
        program,
        short_title,
      } = this.props.challenge;
      var challenge_program = program[0] ? program[0] : program; // program[0] if we are in the program challenge tab (temp fix)
      var userLang = navigator.language || navigator.userLanguage;
      const lang =
        (localStorage.getItem("language") && localStorage.getItem("language") === "fr") ||
        (!localStorage.getItem("language") && (userLang === "fr-FR" || userLang === "fr"))
          ? "fr"
          : "en";
      if (banner_url === undefined || banner_url === null || banner_url === "") {
        banner_url = defaultChallengeImg;
      }
      const challengeImgStyle = { backgroundImage: "url(" + banner_url + ")" };
      if (this.props.cardFormat !== "compact") {
        return (
          <div className="card cardChallenge" key={id}>
            <Link to={`/challenge/${short_title}`}>
              <div style={challengeImgStyle} className="challengeImg" />
            </Link>
            <BtnClap
              itemType="challenges"
              itemId={id}
              clapState={has_clapped}
              source={this.props.source}
              clapCount={claps_count}
            />
            <div className="card-content">
              <Link to={`/challenge/${short_title}`}>
                <h5 className="card-title">{lang === "fr" && title_fr ? title_fr : title}</h5>
              </Link>
              <p className="card-shortname">
                {challenge_program &&
                challenge_program.id !== -1 && ( // if program id is !== -1 (meaning challenge is not attached to a program), display program name and link
                    <Fragment>
                      <FormattedMessage id="entity.info.program.title" defaultMessage="program" />
                      <span>
                        <Link to={"/program/" + challenge_program.short_title}>
                          {lang === "fr" && challenge_program.title_fr
                            ? challenge_program.title_fr
                            : challenge_program.title}
                        </Link>
                      </span>
                    </Fragment>
                  )}
              </p>
              <p className="card-desc">
                {lang === "fr" && short_description_fr ? short_description_fr : short_description}
              </p>
              <p className="card-desc projects_count">
                {projects_count ? projects_count : 0}{" "}
                {projects_count > 1 ? (
                  <FormattedMessage id="challenge.info.submittedProjects" defaultMessage="Projects submitted" />
                ) : (
                  <FormattedMessage id="challenge.info.submittedProjectSingle" defaultMessage="Project submitted" />
                )}
              </p>
              {users_sm !== undefined && users_sm.length > 0 && renderOwnerNames(users_sm, "")}
              {/* <InfoSkillsComponent place="entity_card" limit="3" content={skills} /> */}
            </div>
            <div className="cardfooter">
              <hr />
              {users_sm !== undefined && users_sm.length > 0 && renderTeam(users_sm, "challenge", id, members_count)}
            </div>
          </div>
        );
      } else {
        return (
          <div className="card cardChallenge cardChallengeSmall" key={id}>
            <Link to={`/challenge/${short_title}`}>
              <div style={challengeImgStyle} className="challengeImg" />
            </Link>
            <BtnClap itemType="challenges" itemId={id} clapState={has_clapped} clapCount={claps_count} />
            <div className="card-content">
              <Link to={`/challenge/${short_title}`}>
                <h5 className="card-title">{title}</h5>
              </Link>
              <p className="card-desc">{short_description}</p>
            </div>
          </div>
        );
      }
    } else {
      return null;
    }
  }
}
