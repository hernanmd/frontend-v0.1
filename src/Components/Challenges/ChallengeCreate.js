import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import { UserContext } from "UserProvider";
import Api from "Api";
import ChallengeForm from "./ChallengeForm";
import Alert from "Components/Tools/Alert";
import Loading from "Components/Tools/Loading";

export default class ChallengeCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: "",
      forbidden: false,
      isCreated: false,
      loading: true,
      newChallenge: {
        creator_id: null,
        interests: [],
        short_title: "",
        short_description: "",
        skills: [],
        status: "draft",
        title: "",
      },
      sending: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    let userContext = this.context;
    // console.log("usercontext : ", userContext);
    /* CLB - JUST FOR FRONT ONLY !!!!!!!
    var id = userContext.user ? userContext.user.id : 1; */
    this.handleChange("creator_id", userContext.user.id);

    Api.get("/api/challenges/can_create", { user: userContext.user })
      .then(() => this.setState({ loading: false }))
      .catch(() => this.setState({ loading: false, forbidden: true }));
  }

  handleChange(key, content) {
    var updateChallenge = this.state.newChallenge;
    // console.log(this.state.newChallenge);
    // console.log((this.state.newChallenge.title !== ""), this.state.newChallenge.short_description !== "", this.state.newChallenge.skills.length > 0, this.state.newChallenge.interests.length > 0);

    updateChallenge[key] = content;
    this.setState({ newChallenge: updateChallenge, error: "" });
  }

  handleSubmit() {
    const newChallenge = this.state.newChallenge;
    if (
      newChallenge.title !== "" &&
      newChallenge.short_description !== "" &&
      newChallenge.short_title !== "" &&
      newChallenge.skills.length > 0 &&
      newChallenge.interests.length > 0
    ) {
      this.setState({ sending: true });
      Api.post("/api/challenges/", newChallenge)
        .then((res) => {
          this.setState({ isCreated: res.data.short_title, sending: false });
        })
        .catch((error) => {
          this.setState({ sending: false });
          console.log(error);
        });
    } else {
      this.setState({ error: <FormattedMessage id="challenge.create" defaultMessage="Some information is missing" /> });
    }
  }

  render() {
    var { error, forbidden, isCreated, loading, newChallenge, sending } = this.state;

    if (isCreated) {
      return <Redirect push to={"/challenge/" + isCreated + "/edit"} />;
    }
    if (forbidden) {
      return <Redirect to={"/challenge/forbidden"} />;
    }

    return (
      <Loading active={loading}>
        <div className="challengeCreate container-fluid">
          <h1>
            <FormattedMessage id="challenge.create.title" defaultMessage="Create a new challenge" />
          </h1>
          <ChallengeForm
            challenge={newChallenge}
            handleChange={this.handleChange}
            handleSubmit={this.handleSubmit}
            mode="create"
            sending={sending}
          />
          {error !== "" && <Alert type="danger" message={error} />}
        </div>
      </Loading>
    );
  }
}
ChallengeCreate.contextType = UserContext;
