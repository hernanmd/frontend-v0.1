import React, { Component } from "react";
import "./ChallengesAttachedListBar.scss";
import BtnAdd from "../Tools/BtnAdd";

export default class ChallengesAttachedListBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      itemId: 0,
    };
  }

  static get defaultProps() {
    return {
      actualList: [],
      itemId: 0,
      itemType: "",
      refreshList: () => console.log("props is missing"),
    };
  }

  componentWillReceiveProps(nextProps) {
    const itemId = nextProps.itemId;
    this.setState({ itemId });
    // console.log("ITEMID", this.state.itemId)
  }

  render() {
    const { actualList, itemType, refreshList } = this.props;
    const { itemId } = this.state;
    // console.log("ChallengeListar actualList : ", actualList);
    if (itemId > 0 && itemType !== "") {
      return (
        <div className="row justify-content-end challengesAttachedListBar">
          <div className="col-12">
            <BtnAdd
              type="challenge"
              actualList={actualList}
              itemType={itemType}
              itemId={itemId}
              refreshList={refreshList}
            />
          </div>
        </div>
      );
    } else {
      return null;
    }
  }
}
