import React, { Component } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import { Redirect } from "react-router-dom";
import { UserContext } from "UserProvider";
import Api from "Api";
import ChallengeHeader from "./ChallengeHeader";
import DocumentsManager from "Components/Tools/Documents/DocumentsManager";
import Feed from "../Feed/Feed";
import ListProjectsAttached from "../Tools/ListProjectsAttached";
import InfoInterestsComponent from "Components/Tools/Info/InfoInterestsComponent";
import InfoSkillsComponent from "Components/Tools/Info/InfoSkillsComponent";
import InfoHtmlComponent from "Components/Tools/Info/InfoHtmlComponent";
import Loading from "Components/Tools/Loading";
import Needs from "Components/Needs/Needs";
import { stickyTabNav, scrollToActiveTab } from "Components/Tools/utils/utils.js";
import "Components/Main/Similar.scss";

class ChallengeDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      challenge: this.props.challenge,
      failedLoad: false,
      loading: true,
    };
  }

  getChallenge(id) {
    Api.get("/api/challenges/" + id)
      .then((res) => {
        if (res.data) {
          this.setState({ challenge: res.data, loading: false });
        } else {
          this.setState({ failedLoad: true, loading: false });
        }
      })
      .catch((error) => {
        this.setState({ failedLoad: true, loading: false });
      });
  }

  getChallenge_from_name(short_title) {
    // console.log("/api/challenges/getid/" + short_title)
    Api.get("/api/challenges/getid/" + short_title)
      .then((res) => {
        // console.log(res)
        this.getChallenge(res.data.id);
      })
      .catch((error) => {
        this.setState({ failedLoad: true, loading: false });
      });
  }

  componentWillMount() {
    if (!this.props.challenge) {
      // console.log(this.props.match.params.id, isNaN(this.props.match.params.id))
      if (isNaN(this.props.match.params.id)) {
        this.getChallenge_from_name(this.props.match.params.id);
      } else {
        this.getChallenge(this.props.match.params.id);
      }
    } else {
      this.setState({ loading: false });
    }
  }

  render() {
    const { challenge, failedLoad, loading } = this.state;
    var urlParams = new URLSearchParams(window.location.search);
    var { intl } = this.props;
    let userContext = this.context;
    var userLang = navigator.language || navigator.userLanguage;
    const lang =
      (localStorage.getItem("language") && localStorage.getItem("language") === "fr") ||
      (!localStorage.getItem("language") && (userLang === "fr-FR" || userLang === "fr"))
        ? "fr"
        : "en";
    // if fail has loaded or if status is draft & we are not admin, redirect to challenges page
    if (challenge && (failedLoad || (!challenge.is_admin && challenge.status === "draft"))) {
      return <Redirect to="/challenges" />;
    }
    if (failedLoad) {
      return <Redirect to="/challenges" />;
    }

    // make different tab active depending if user is member or not
    var newsTabClasses, aboutTabClasses, newsPaneClasses, aboutPaneClasses;
    if (challenge) {
      if (challenge.is_member || urlParams.get("tab") === "feed") {
        newsTabClasses = "nav-item nav-link active";
        aboutTabClasses = "nav-item nav-link";
        newsPaneClasses = "tab-pane active";
        aboutPaneClasses = "tab-pane";
      } else {
        newsTabClasses = "nav-item nav-link";
        aboutTabClasses = "nav-item nav-link active";
        newsPaneClasses = "tab-pane";
        aboutPaneClasses = "tab-pane active";
      }
      setTimeout(function () {
        stickyTabNav(); // make the tab navigation bar sticky on top when we reach its scroll position
        scrollToActiveTab(); // if there is a hash in the url and the tab exists, click and scroll to the tab
      }, 700); // had to add setTimeout to make it work
    }

    return (
      <Loading active={loading}>
        {challenge && (
          <div className="challengeDetails container-fluid">
            <ChallengeHeader challenge={challenge} lang={lang} />
            <nav className="nav nav-tabs container-fluid">
              <a className={newsTabClasses} href="#news" data-toggle="tab">
                <FormattedMessage id="entity.tab.news" defaultMessage="News & Update" />
              </a>
              <a className={aboutTabClasses} href="#about" data-toggle="tab">
                <FormattedMessage id="general.about" defaultMessage="About" />
              </a>
              {/* <a className="nav-item nav-link" href="#howto" data-toggle="tab"><FormattedMessage id="entity.tab.howTo" defaultMessage="How to Participate ?" /></a> */}
              {/* <a className="nav-item nav-link projects" href="#projects" data-toggle="tab"><FormattedMessage id="general.projects" defaultMessage="Projects" /></a> */}
              <a className="nav-item nav-link" href="#projects" data-toggle="tab">
                <FormattedMessage id="general.projects" defaultMessage="Projects" />
              </a>
              <a className="nav-item nav-link" href="#needs" data-toggle="tab">
                <FormattedMessage id="entity.card.needs" defaultMessage="Needs" />
              </a>
              {((challenge.documents.length !== 0 && (!challenge.is_admin || !userContext.user)) ||
                challenge.is_admin) && ( // hide documents tab is user is not admin and they are no docs
                <a className="nav-item nav-link" href="#documents" data-toggle="tab">
                  <FormattedMessage id="entity.tab.documents" defaultMessage="Documents" />
                </a>
              )}
              <a className="nav-item nav-link" href="#faq" data-toggle="tab">
                <FormattedMessage id="entity.tab.faq" defaultMessage="FAQ" />
              </a>
              {/* <a className="nav-item nav-link" href="#members" data-toggle="tab"><FormattedMessage id="entity.tab.participants" defaultMessage="Members" /></a> */}
              {/* <a className="nav-item nav-link" href="#followers" data-toggle="tab"><FormattedMessage id="entity.tab.followers" defaultMessage="Follower" /></a> */}
            </nav>
            <div className="tabContainer">
              <div className="tab-content justify-content-center container-fluid">
                <div className={newsPaneClasses} id="news">
                  {/* Show feed, and pass DisplayCreate to all connected users */}
                  {challenge.feed_id && (
                    <Feed
                      feedId={challenge.feed_id}
                      displayCreate={this.context.connected ? true : false}
                      isAdmin={challenge.is_admin}
                    />
                  )}
                  {/* {challenge.feed_id && <Feed feedId={challenge.feed_id} displayCreate={this.context.connected ? challenge.is_member ? true : false : false} isAdmin={challenge.is_admin} />} */}
                </div>
                <div className={aboutPaneClasses} id="about">
                  <InfoHtmlComponent
                    content={
                      lang === "fr" && challenge.description_fr ? challenge.description_fr : challenge.description
                    }
                  />
                  <InfoInterestsComponent
                    title={intl.formatMessage({ id: "challenge.info.interests" })}
                    content={challenge.interests}
                  />
                  <FormattedMessage id="user.profile.skills" defaultMessage="Skills">
                    {(skills) => <InfoSkillsComponent content={challenge.skills} title={skills} />}
                  </FormattedMessage>
                </div>
                <div className="tab-pane" id="projects">
                  <div className="alert alert-info" role="alert">
                    <FormattedMessage
                      id="challenge.projects.text"
                      defaultMessage="To participate to this challenge, participate to as many of its projects. Check out the projects already submitted, contribute to them or create your own!"
                    />
                  </div>
                  <ListProjectsAttached itemId={challenge.id} itemType="challenges" />
                </div>
                <div className="tab-pane" id="needs">
                  <Needs displayCreate={false} itemId={challenge.id} itemType="challenges" />
                </div>
                <div className="tab-pane" id="documents">
                  <DocumentsManager item={challenge} itemId={challenge.id} itemType="challenges" />
                </div>
                <div className="tab-pane" id="faq">
                  <InfoHtmlComponent
                    title=""
                    content={lang === "fr" && challenge.faq_fr ? challenge.faq_fr : challenge.faq}
                  />
                </div>
              </div>
            </div>
          </div>
        )}
      </Loading>
    );
  }
}
export default injectIntl(ChallengeDetails);
ChallengeDetails.contextType = UserContext;
