import React, { Component, Fragment } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { UserContext } from "UserProvider";
import AuthPage from "Pages/AuthPage";
import ChallengeForbiddenPage from "Pages/Challenge/ChallengeForbiddenPage";
import ChallengePage from "Pages/Challenge/ChallengePage";
import ChallengesPage from "Pages/Challenge/ChallengesPage";
import CommunitiesPage from "Pages/Community/CommunitiesPage";
import CommunityPage from "Pages/Community/CommunityPage";
import CompleteProfile from "Pages/CompleteProfile/CompleteProfile";
import Footer from "Components/Footer/Footer";
import Header from "Components/Header/Header";
import Home from "Pages/Home/Home";
import Loading from "Components/Tools/Loading";
import NotFound from "Pages/NotFound";
import UsersPage from "Pages/Users/UsersPage";
import ProjectPage from "Pages/Project/ProjectPage";
import ProjectsPage from "Pages/Projects/ProjectsPage";
import ProgramPage from "Pages/Program/ProgramPage";
import PostDisplay from "Components/Feed/Posts/PostDisplay";
import ScrollToTop from "Components/Tools/ScrollToTop";
import SignIn from "Components/Authentification/SignIn";
import SignUp from "Components/Authentification/SignUp";
import Terms from "Pages/Legal/Terms";
import Data from "Pages/Legal/Data";
import EthicsPledge from "Pages/Legal/EthicsPledge";
import UserPage from "Pages/Users/UserPage";
import NeedsPage from "Pages/Needs/NeedsPage";
import Needs from "Components/Needs/Needs/";
import SearchPage from "Pages/Search/SearchPage";

export default class RouterMain extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
    };
  }

  componentWillMount() {
    let userContext = this.context;
    userContext
      .checkUserTokens()
      .then(() => this.setState({ loading: false }))
      .catch(() => this.setState({ loading: false }));
  }

  render() {
    return (
      <Loading active={this.state.loading}>
        <Router>
          <UserContext.Consumer>
            {(userContext) => (
              <Fragment>
                <Header userConnected={userContext.connected} />
                <div className="main">
                  <ScrollToTop>
                    <Switch>
                      <Route path="/" exact={true} component={Home} />
                      <Route path="/auth/" component={AuthPage} />
                      <Route path="/challenge/forbidden" component={ChallengeForbiddenPage} />
                      <Route path="/challenge/:id" component={ChallengePage} />
                      <Route path="/challenges" exact={true} component={ChallengesPage} />
                      <Route path="/community/:id" component={CommunityPage} />
                      <Route path="/communities" exact={true} component={CommunitiesPage} />
                      <Route path="/complete-profile" render={() => <CompleteProfile user={userContext.user} />} />
                      <Route path="/people" exact={true} component={UsersPage} />
                      <Route path="/project/:id" component={ProjectPage} />
                      <Route path="/projects" exact={true} component={ProjectsPage} />
                      <Route path="/post/:id" exact={true} component={PostDisplay} />
                      <Route path="/program/:short_title" component={ProgramPage} />
                      <Route path="/user/:id" component={UserPage} />
                      {/* <Route path="/needs/:id" exact={true} render={() => <Needs itemType="single" itemId="id" />} /> */}
                      <Route
                        path="/needs/:id"
                        exact={true}
                        render={(props) => <Needs itemType="single" itemId="id" {...props} />}
                      />
                      <Route path="/needs" component={NeedsPage} />
                      <Route path="/search/:terms" component={SearchPage} />
                      <Route path="/search" component={SearchPage} />
                      <Route path="/signin" component={SignIn} />
                      <Route path="/signup" component={SignUp} />
                      <Route path="/terms" component={Terms} />
                      <Route path="/data" component={Data} />
                      <Route path="/ethics-pledge" component={EthicsPledge} />
                      <Route component={NotFound} />
                    </Switch>
                  </ScrollToTop>
                </div>
                <Footer />
              </Fragment>
            )}
          </UserContext.Consumer>
        </Router>
      </Loading>
    );
  }
}
RouterMain.contextType = UserContext;
