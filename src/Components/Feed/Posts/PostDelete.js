import React, { Component } from "react";
import Api from "Api";
import { FormattedMessage } from "react-intl";

export default class PostDelete extends Component {
  static get defaultProps() {
    return {
      postId: undefined,
      commentId: undefined,
      type: undefined,
      origin: "",
      feedId: "",
    };
  }

  deletePost() {
    const { postId, commentId, type, origin, feedId } = this.props;
    if (postId !== undefined) {
      var apiUrl;
      if (origin === "self") {
        apiUrl = type === "post" ? `/api/posts/${postId}` : `/api/posts/${postId}/comment/${commentId}`;
      } else {
        apiUrl = type === "post" ? `/api/feeds/${feedId}/${postId}` : `/api/posts/${postId}/comment/${commentId}`;
        // apiUrl = type === "post" ? `/api/feeds/${feedId}/${postId}` : `/api/feeds/${feedId}/${postId}/comment/${commentId}`
      }
      console.log(apiUrl);
      Api.delete(apiUrl).then((res) => {
        this.props.refresh();
      });
    } else {
    }
  }

  render() {
    const { origin } = this.props;
    return (
      <div onClick={this.deletePost.bind(this)}>
        <i className="fa fa-trash postDelete" /> <FormattedMessage id="feed.object.delete" defaultMessage="Delete" />
      </div>
    );
  }
}
