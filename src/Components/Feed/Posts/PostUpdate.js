import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import Api from "Api";
import FormPost from "./FormPost";
import { findMentions, noHTMLMentions, transformMentions } from "Components/Feed/Mentions.js";

export default class PostUpdate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: noHTMLMentions(this.props.content),
      documents: [],
      mentions: [],
      uploading: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeDoc = this.handleChangeDoc.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  static get defaultProps() {
    return {
      postId: undefined,
      content: "",
      user: "",
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ postId: nextProps.postId });
  }

  handleChange(content) {
    this.setState({ content });
  }

  handleChangeDoc(documents) {
    this.setState({ documents });
  }

  handleSubmit() {
    var mentions = findMentions(this.state.content);
    var documents = this.state.documents;
    var contentNoMentions = transformMentions(this.state.content);
    const postId = this.props.postId;
    var updateJson = {
      post: {
        content: contentNoMentions,
      },
    };
    if (mentions) {
      updateJson["post"]["mentions"] = mentions;
    }
    if (documents) {
      updateJson["post"]["documents"] = documents;
    }
    // console.log("updateJson", updateJson);
    this.setState({ uploading: true });
    Api.patch(`/api/posts/${postId}`, updateJson)
      .then((res) => {
        if (this.state.documents.length > 0) {
          const itemId = postId;
          const itemType = "posts";
          const type = "documents";
          if (itemId) {
            var bodyFormData = new FormData();
            Array.from(this.state.documents).forEach((file) => {
              bodyFormData.append(type + "[]", file);
            });

            var config = {
              headers: { "Content-Type": "multipart/form-data" },
            };

            Api.post("/api/" + itemType + "/" + itemId + "/" + type, bodyFormData, config)
              .then((res) => {
                if (res.status === 200) {
                  // console.log("Documents uploaded !");
                  this.setState({ uploading: false });
                  this.props.refresh();
                } else {
                  this.setState({
                    uploading: false,
                    error: <FormattedMessage id="err-" defaultMessage="An error has occured" />,
                  });
                }
              })
              .catch((error) => {
                // console.log(error);
                // console.log(error.response.data);
                this.setState({
                  uploading: false,
                  error: error.response.data.status + " : " + error.response.data.error,
                });
              });
          } else {
            // console.log("Unable to upload files (No PostId defined)");
          }
        }
        this.props.isEdit();
        this.props.refresh();
      })
      .catch((error) => {
        // console.log(error);
      });
  }

  render() {
    return (
      <FormPost
        action="update"
        content={this.state.content}
        documents={this.state.documents}
        handleChange={this.handleChange}
        handleChangeDoc={this.handleChangeDoc}
        handleSubmit={this.handleSubmit}
        uploading={this.state.uploading}
        user={this.props.user}
      />
    );
  }
}
