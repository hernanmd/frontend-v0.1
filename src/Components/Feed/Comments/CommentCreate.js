import React, { Component } from "react";
import Api from "Api";
import FormComment from "./FormComment";
import { findMentions, transformMentions } from "Components/Feed/Mentions.js";
import "./CommentCreate.scss";

export default class CommentCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: this.props.content,
      mentions: [],
      uploading: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  static get defaultProps() {
    return {
      postId: undefined,
      content: "",
      user: "",
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ postId: nextProps.postId });
  }

  handleChange(content) {
    this.setState({ content });
  }

  handleSubmit() {
    var mentions = findMentions(this.state.content);
    var contentNoMentions = transformMentions(this.state.content);
    const user_id = this.props.user.id;
    const postId = this.props.postId;
    var commentJson = {
      comment: {
        user_id: user_id,
        content: contentNoMentions,
      },
    };
    if (mentions) {
      commentJson["comment"]["mentions"] = mentions;
    }
    // console.log("commentJson", commentJson);
    Api.post(`/api/posts/${postId}/comment`, commentJson)
      .then((res) => {
        // console.log(res);
        this.props.refresh();
        this.setState({
          content: "",
        });
      })
      .catch((error) => {
        // console.log(error);
      });
  }

  render() {
    return (
      <FormComment
        action="create"
        postId={this.props.postId}
        content={this.state.content}
        handleChange={this.handleChange}
        handleSubmit={this.handleSubmit}
        user={this.props.user}
      />
    );
  }
}
