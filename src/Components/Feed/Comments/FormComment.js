import React, { Component } from "react";
import { FormattedMessage } from "react-intl";
import PostInputMentions from "Components/Tools/Forms/PostInputMentions";
import defaultImg from "assets/img/default/default-user.png";
import "./CommentCreate.scss";
//import "./PostCreate.scss";

const commentStyle = {
  control: {
    backgroundColor: "#F2F3F4",

    fontSize: 12,
    fontWeight: "normal",
  },

  highlighter: {
    overflow: "hidden",
  },

  input: {
    margin: 0,
  },

  "&singleLine": {
    control: {
      display: "inline-block",

      width: 130,
    },

    highlighter: {
      padding: 1,
      border: "2px inset transparent",
    },

    input: {
      padding: 1,

      border: "2px inset",
    },
  },

  "&multiLine": {
    control: {
      border: "1px solid silver",
    },

    highlighter: {
      padding: 9,
    },

    input: {
      padding: 9,
      minHeight: 30,
      outline: 0,
      border: 0,
    },
  },

  suggestions: {
    list: {
      backgroundColor: "white",
      border: "1px solid rgba(0,0,0,0.15)",
      fontSize: 10,
    },

    item: {
      padding: "5px 15px",
      borderBottom: "1px solid rgba(0,0,0,0.15)",

      "&focused": {
        backgroundColor: "#cee4e5",
      },
    },
  },
};

export default class PostComment extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  static get defaultProps() {
    return {
      content: "",
      action: "create",
      user: "", // @TODO utiliser context,
      postId: "",
      handleChange: () => console.log("Missing function"),
      handleSubmit: () => console.log("Missing function"),
    };
  }

  handleChange(content) {
    this.props.handleChange(content);
    if (this.props.action === "create") {
      // only when we create a post (not on update)
      var commentBtn = document.querySelector(`.commentCreate#post${this.props.postId} .btn-primary`); // select comment button from parent post
      content ? (commentBtn.style.display = "block") : (commentBtn.style.display = "none"); // if we start typing, show it, and hide it if it's empty
    }
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.handleSubmit();
  }

  render() {
    const { action, content, user, postId } = this.props;
    var submitBtnText = action === "create" ? "Comment" : "Update";
    var postTypeClass = action === "create" ? "postCreate commentCreate" : "commentUpdate";
    const userImg = user.logo_url_sm ? user.logo_url_sm : defaultImg;
    const userImgStyle = { backgroundImage: "url(" + userImg + ")" };
    return (
      <div className={postTypeClass} id={`post${postId}`}>
        <form onSubmit={this.handleSubmit}>
          <div className="inputBox">
            {action === "create" && ( // if post action is create, display image on the left
              <div className="userImgContainer">
                <div className="userImg" style={userImgStyle}></div>
              </div>
            )}
            <PostInputMentions
              content={content}
              onChange={this.handleChange}
              className="suggestionsContainer"
              style={commentStyle}
              placeholder={[["comment"], ["Comment.."]]}
            />
          </div>
          <button type="submit" className="btn btn-primary btn-sm">
            <FormattedMessage id={"post.comment.create.btn" + submitBtnText} defaultMessage={submitBtnText} />
          </button>
        </form>
      </div>
    );
  }
}
