import React, { Component, Fragment } from "react";
import PeopleCard from "Components/People/PeopleCard";
import ProjectCard from "Components/Projects/ProjectCard";
import NeedCard from "Components/Needs/Need/NeedCard";
import CommunityCard from "Components/Communities/CommunityCard";
import ChallengeCard from "Components/Challenges/ChallengeCard";
import { FormattedMessage, injectIntl } from "react-intl";
import { Redirect } from "react-router-dom";
import Loading from "Components/Tools/Loading";
import Alert from "Components/Tools/Alert";
import $ from "jquery";
import { UserContext } from "UserProvider";
import { Helmet } from "react-helmet";
import {
  InstantSearch,
  RefinementList,
  SortBy,
  Index,
  Pagination,
  ClearRefinements,
  ToggleRefinement,
  CurrentRefinements,
  Hits,
  Configure,
  HitsPerPage,
  Panel,
  Stats,
  SearchBox,
} from "react-instantsearch-dom";

import { ClearFiltersMobile, NoResults, ResultsNumberMobile, SaveFiltersMobile } from "./widgets";
import algoliasearch from "algoliasearch/lite";
import "../../Components/Main/AlgoliaResultsPages.scss";
import "./Search.scss";
import withURLSync from "./URLSync";
import { connectSearchBox, connectCurrentRefinements } from "react-instantsearch-dom";
// import {
//   GoogleMapsLoader,
//   GeoSearch,
//   Control,
//   Marker,
// } from 'react-instantsearch-dom-maps';

const searchClient = algoliasearch(process.env.REACT_APP_ALGOLIA_APP_ID, process.env.REACT_APP_ALGOLIA_TOKEN);

// const gmap_api_key = process.env.REACT_APP_GMAP_API_TOKEN;

// var containerRef = document.querySelector(".searchContainer")
// var headerRef = document.querySelector(".searchPageHeader")

//
// const Stats = ({ processingTimeMS, nbHits }) => (
//   <p>
//     wasup {nbHits} results in {processingTimeMS}
//     ms
//   </p>
// );
// const CustomStats = connectStats(Stats);

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchState: {},
      onSearchStateChange: null,
      index: "",
      hitsPerPage: "",
      loading: true,
      refinements: null,
      sortByItems: null,
    };

    this.onSearchStateChange = this.onSearchStateChange.bind(this);
    this.makeRefinementLists = this.makeRefinementLists.bind(this);
  }

  static get defaultProps() {
    return {
      searchState: {},
      index: "",
      hitsPerPage: 20,
      refinements: null,
      sortByItems: null,
      onSearchStateChange: function (state) {
        return null;
      },
    };
  }

  componentDidMount() {
    this.setState({ searchState: this.props.searchState });
    this.setState({ onSearchStateChange: this.props.onSearchStateChange });
    this.setState({ index: this.props.index });
    this.setState({ hitsPerPage: this.props.hitsPerPage });
    this.setState({ refinements: this.props.refinements });
    this.setState({ sortByItems: this.props.sortByItems });

    // this.stickyFilterBtn() // launch function when document has loaded
  }

  componentWillReceiveProps(nextProps) {
    nextProps.searchState && this.setState({ searchState: nextProps.searchState });
    nextProps.onSearchStateChange && this.setState({ onSearchStateChange: nextProps.onSearchStateChange });
    nextProps.index && this.setState({ index: nextProps.index });
    nextProps.hitsPerPage && this.setState({ hitsPerPage: nextProps.hitsPerPage });
    nextProps.refinements && this.setState({ refinements: nextProps.refinements });
    nextProps.sortByItems && this.setState({ sortByItems: nextProps.sortByItems });
  }

  makeRefinementLists() {
    var { intl } = this.props;
    var { index } = this.state;
    var userLang = navigator.language || navigator.userLanguage;
    var path =
      (localStorage.getItem("language") && localStorage.getItem("language") === "fr") ||
      (!localStorage.getItem("language") && (userLang === "fr-FR" || userLang === "fr"))
        ? "interests/fr" // if website or browser is in french
        : "interests"; // if english
    var refinementList = this.state.refinements.map(function (refinement, i) {
      var attribute = refinement.attribute || "";
      var sdgIdToImg = (items) =>
        items.map((item) => ({
          ...item,
          // transform interest/sdg id to its sdg image
          label:
            attribute === "interests" ? (
              <img
                src={require("assets/img/" + path + "/Interest-" + item.label + ".png")}
                className="imgInterest"
                alt={"Interest-" + item.label}
              />
            ) : (
              item.label
            ),
        }));
      var type = refinement.type || "";
      var showmore = refinement.showmore !== undefined ? refinement.showmore : true;
      var limit = refinement.limit || 10;
      var searchable = refinement.searchable !== undefined ? refinement.searchable : true;
      var titleId =
        attribute === "ressources"
          ? "user.profile.ressources"
          : attribute === "skills"
          ? "user.profile.skills"
          : attribute === "interests"
          ? "algolia.interests"
          : attribute === "project.title"
          ? "general.projects"
          : attribute === "program.title"
          ? "programs"
          : "need.isUrgentShort";
      var placeholderId =
        attribute === "ressources"
          ? "algolia.ressources.placeholder"
          : attribute === "interests"
          ? "algolia.interests.placeholder"
          : attribute === "project.title"
          ? "algolia.projects.placeholder"
          : "algolia.skills.placeholder";
      return (
        (index !== "Project" || (index === "Project" && attribute !== "ressources")) && (
          <Panel header={intl.formatMessage({ id: titleId })} className={attribute} key={i}>
            {/* <CurrentRefinements /> */}
            {type !== "toggle" && (
              <RefinementList
                attribute={attribute}
                showMore={showmore}
                limit={limit}
                searchable={searchable}
                transformItems={sdgIdToImg}
                translations={{
                  placeholder: intl.formatMessage({ id: placeholderId }),
                  noResults: intl.formatMessage({ id: "algolia.noResult" }),
                  showMore(expanded) {
                    return expanded
                      ? intl.formatMessage({ id: "general.showless", defaultMessage: "Show less" })
                      : intl.formatMessage({ id: "general.showmore", defaultMessage: "Show more" });
                  },
                }}
              />
            )}

            {type === "toggle" && (
              <ToggleRefinement
                attribute={attribute}
                label="Is urgent"
                // value={string|number|boolean}
                value={true}
                // Optional parameters
                // defaultRefinement={boolean}
              />
            )}
          </Panel>
        )
      );
    });
    return refinementList;
  }

  makeSortBy() {
    var items = this.state.sortByItems.map(function (item, i) {
      var index = item.index || this.state.index;
      var label = item.label || "Undefined";
      return { value: index, label: label };
    });
    return <SortBy className="container-option" defaultRefinement={this.state.index} items={items} />;
  }

  onSearchStateChange(searchState) {
    this.setState({ searchState });
    if (this.state.onSearchStateChange) {
      this.state.onSearchStateChange(searchState);
    }
  }

  openFilters() {
    document.querySelector(".searchPageAll").classList.add("filtering");
    window.scrollTo(0, 0);
    // window.addEventListener('keyup', this.onKeyUp);
    // window.addEventListener('click', this.onClick);
  }

  closeFilters() {
    document.querySelector(".searchPageAll").classList.remove("filtering");
    // containerRef.scrollIntoView();
    // window.removeEventListener('keyup', this.onKeyUp);
    // window.removeEventListener('click', this.onClick);
  }

  // onKeyUp(event) {
  //   if (event.key !== 'Escape') {
  //     return;
  //   }

  //   this.closeFilters();
  // }

  // onClick(event) {
  //   if (event.target !== headerRef.current) {
  //     return;
  //   }

  //   this.closeFilters();
  // }

  // stickyFilterBtn() {
  // 	var filtersBtn = $('.filters-button'),
  // 			pagination = $('.ais-Pagination'),
  // 			pagiOffset = pagination.offset().top, // element distance from top of page
  // 			btnH = filtersBtn.outerHeight(), // height of button element
  // 			pagiH = pagination.outerHeight(), // height of pagination element
  // 			offsetFix = 95, // offset distance between pagi and button elements
  // 			windH = $(window).height(); // window height
  // 	$(window).scroll(function() {
  // 		var currScrollPost = $(window).scrollTop(); // current y scroll position in the page (in px)
  // 		if (currScrollPost > (pagiOffset+pagiH+(offsetFix+btnH)-windH)){ // execute only if we get pass the pagination element
  // 			filtersBtn.addClass("sticky") // add sticky class so filter btn is sticked to bottom of page
  // 		}
  // 		else{
  // 			filtersBtn.removeClass("sticky") // remove sticky class
  // 		}
  // 	});
  // }

  render() {
    var searchState = this.state.searchState;
    var index = this.state.index;

    const hitComponent = ({ hit }) =>
      index === "User" ? (
        <PeopleCard user={hit} source="algolia" />
      ) : index === "Project" ? (
        <ProjectCard project={hit} source="algolia" />
      ) : index === "Need" ? (
        <NeedCard need={hit} mode="card" source="algolia" />
      ) : index === "Community" ? (
        <CommunityCard community={hit} source="algolia" />
      ) : (
        index === "Challenge" && <ChallengeCard challenge={hit} source="algolia" />
      );

    // var url = new URL(window.location.href);
    var { intl } = this.props;
    return (
      <InstantSearch
        indexName={index}
        searchClient={searchClient}
        searchState={searchState}
        onSearchStateChange={this.onSearchStateChange}
      >
        <div className="searchMain">
          <SearchBox translations={{ placeholder: intl.formatMessage({ id: "algolia.search.placeholder" }) }} />
        </div>
        <div className="d-flex searchContainer">
          <div className="container-wrapper">
            {/* <section className="container-filters" onKeyUp={onKeyUp}> */}
            <section className="container-filters">
              <div className="container-header">
                <p className="filters">{intl.formatMessage({ id: "algolia.filters" })}</p>

                <div className="clear-filters" data-layout="desktop">
                  <ClearRefinements
                    translations={{
                      reset: (
                        <>
                          <svg xmlns="http://www.w3.org/2000/svg" width="11" height="11" viewBox="0 0 11 11">
                            <g fill="none" fillRule="evenodd" opacity=".4">
                              <path d="M0 0h11v11H0z" />
                              <path
                                fill="#000"
                                fillRule="nonzero"
                                d="M8.26 2.75a3.896 3.896 0 1 0 1.102 3.262l.007-.056a.49.49 0 0 1 .485-.456c.253 0 .451.206.437.457 0 0 .012-.109-.006.061a4.813 4.813 0 1 1-1.348-3.887v-.987a.458.458 0 1 1 .917.002v2.062a.459.459 0 0 1-.459.459H7.334a.458.458 0 1 1-.002-.917h.928z"
                              />
                            </g>
                          </svg>
                          {intl.formatMessage({ id: "algolia.filters.clear" })}
                        </>
                      ),
                    }}
                  />
                </div>

                <div className="clear-filters">
                  <ResultsNumberMobile />
                </div>
              </div>

              <div className="container-body">{this.state.refinements && this.makeRefinementLists()}</div>
            </section>

            <div className="container-filters-footer" data-layout="mobile">
              <div className="container-filters-footer-button-wrapper">
                <ClearFiltersMobile closeFilters={this.closeFilters} />
              </div>

              <div className="container-filters-footer-button-wrapper">
                <SaveFiltersMobile onClick={this.closeFilters} />
              </div>
            </div>
          </div>
          <div className="resultsContainer">
            <header className="container-header container-options">
              {/* <div className="d-flex"> */}
              {this.state.sortByItems && this.makeSortBy()}

              {/* <Configure hitsPerPage={hitsPerPage} /> */}
              <HitsPerPage
                className="container-option"
                items={[
                  { label: `15 ${intl.formatMessage({ id: "algolia.hitsPerPage" })}`, value: 15 },
                  { label: `30 ${intl.formatMessage({ id: "algolia.hitsPerPage" })}`, value: 30 },
                  { label: `60 ${intl.formatMessage({ id: "algolia.hitsPerPage" })}`, value: 60 },
                ]}
                defaultRefinement={15}
              />
              {/* </div> */}
            </header>

            <div className={`${index}-hits`}>
              <Hits hitComponent={hitComponent} />
              {/*index === "User" &&
  	              <div className="mapHits">
  		              <GoogleMapsLoader apiKey={gmap_api_key}>
  		                {google => (
  		                  <GeoSearch google={google} enableRefine={true} enableRefineOnMapMove={true}>
  		                    {({ hits }) => (
  		                      <div>
  		                        <Control />
  		                        {hits.map(hit => (
  		                          <Marker key={hit.objectID} hit={hit} />
  		                        ))}
  		                      </div>
  		                    )}
  		                  </GeoSearch>
  		                )}
  		              </GoogleMapsLoader>
  		            </div>
  							*/}
            </div>

            <NoResults />

            <footer className="container-footer">
              <Pagination
                padding={2}
                showFirst={false}
                showLast={false}
                translations={{
                  previous: (
                    <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 10 10">
                      <g
                        fill="none"
                        fillRule="evenodd"
                        stroke="#000"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="1.143"
                      >
                        <path d="M9 5H1M5 9L1 5l4-4" />
                      </g>
                    </svg>
                  ),
                  next: (
                    <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 10 10">
                      <g
                        fill="none"
                        fillRule="evenodd"
                        stroke="#000"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="1.143"
                      >
                        <path d="M1 5h8M5 9l4-4-4-4" />
                      </g>
                    </svg>
                  ),
                }}
              />
            </footer>

            {index !== "Challenge" && (
              <aside data-layout="mobile">
                <button className="filters-button" data-action="open-overlay" onClick={this.openFilters}>
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 14">
                    <path
                      d="M15 1H1l5.6 6.3v4.37L9.4 13V7.3z"
                      stroke="#fff"
                      strokeWidth="1.29"
                      fill="none"
                      fillRule="evenodd"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                  </svg>
                  {intl.formatMessage({ id: "algolia.filters" })}
                </button>
              </aside>
            )}
          </div>
        </div>
      </InstantSearch>
    );
  }
}
export default injectIntl(Search);
// export default withURLSync(injectIntl(Search));;
// Search.contextType = UserContext;
