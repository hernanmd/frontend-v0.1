import React from "react";
import { connectStats } from "react-instantsearch-dom";
import { formatNumber } from "Components/Tools/utils/utils.js";
import { FormattedMessage } from "react-intl";

const SaveFiltersMobile = ({ nbHits, onClick }) => (
  <button className="button button-primary" onClick={onClick}>
    <FormattedMessage
      id="algolia.seeResults"
      defaultMessage={`See {number} results`}
      values={{ number: formatNumber(nbHits) }}
    />
  </button>
);

export default connectStats(SaveFiltersMobile);
