import React, { useState, Component, Fragment } from "react";
import { FormattedMessage, injectIntl } from "react-intl";
import { Redirect } from "react-router-dom";
import Loading from "Components/Tools/Loading";
import Alert from "Components/Tools/Alert";
import $ from "jquery";
import qs from "qs";
import { UserContext } from "UserProvider";
import { Helmet } from "react-helmet";
import Search from "Components/Search/Search";

// For information
// URL format of params
// ?param1=value&param2=value
// ?
// For the query aka the words typed in
// query=test
// Which page to land on
// page=1
// For the refinements it's an array
// format is refinementList[NAME_OF_INDEXED_VALUE][array_index]=value
// Example:
// refinementList[skills][0]=cloud
// refinementList[skills][1]=Big Data
// Will demux to
// refinementList: {"skills": [ "cloud", "Big Data" ]}

class SearchAll extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchState: {},
      loading: true,
      activeIndex: "Members",
      sortBy: { Members: "User", Needs: "Need", Projects: "Project", Groups: "Community", Challenges: "Challenge" },
      search: "",
      activeTab: { Members: "active", Needs: "", Projects: "", Groups: "", Challenges: "" },
    };
    this.onSearchStateChange = this.onSearchStateChange.bind(this);
    this.changeTab = this.changeTab.bind(this);
    this.urlToSearchState = this.urlToSearchState.bind(this);
    this.searchStateToURL = this.searchStateToURL.bind(this);
  }

  static get defaultProps() {
    return {
      searchState: {},
    };
  }

  urlToSearchState(search) {
    var searchState = qs.parse(search.slice(1));
    if (searchState.i) {
      this.state.activeIndex = searchState.i;
      this.changeTab(searchState.i);
    }
    searchState && this.setState({ searchState });
  }

  searchStateToURL(searchState) {
    var url = `?${qs.stringify(searchState)}`;
  }

  componentDidMount() {
    this.setState({ searchState: this.props.searchState });
    this.setState({ search: this.props.search });
    this.props.search && this.urlToSearchState(this.props.search);
  }

  componentWillReceiveProps(nextProps) {
    nextProps.searchState && this.setState({ searchState: nextProps.searchState });
    this.setState({ search: nextProps.search });
    this.urlToSearchState(nextProps.search);
  }

  onSearchStateChange(searchState) {
    this.setState({ searchState });
    // this.searchStateToURL(searchState);
  }

  changeTab(newTab) {
    // Save current
    if (this.state.searchState.sortBy) {
      this.state.sortBy[this.state.activeIndex] = this.state.searchState.sortBy;
    }
    // Reload previous
    this.state.searchState.sortBy = this.state.sortBy[newTab];
    // Set new active Index
    this.state.activeIndex = newTab;
    var newactiveTab = {};
    for (const key in this.state.activeTab) {
      if (key === newTab) {
        newactiveTab[key] = "active";
      } else {
        newactiveTab[key] = "";
      }
    }
    this.setState({ activeTab: newactiveTab });
  }

  render() {
    var { searchState, activeIndex, activeTab } = this.state;
    var { intl } = this.props;

    return (
      <div className="searchPageAll AlgoliaResulstsPages container-fluid">
        {/* <header className="header peopleHeader" ref={headerRef}> */}
        <header className="header searchPageHeader"></header>

        <nav className="nav container-fluid">
          <a
            className={"indexType " + activeTab.Members}
            href="#members"
            data-toggle="tab"
            onClick={() => this.changeTab("Members")}
          >
            <FormattedMessage id="general.members" defaultMessage="People" />
          </a>
          <a
            className={"indexType " + activeTab.Needs}
            href="#needs"
            data-toggle="tab"
            onClick={() => this.changeTab("Needs")}
          >
            <FormattedMessage id="entity.card.needs" defaultMessage="Needs" />
          </a>
          <a
            className={"indexType " + activeTab.Projects}
            href="#projects"
            data-toggle="tab"
            onClick={() => this.changeTab("Projects")}
          >
            <FormattedMessage id="general.projects" defaultMessage="Projects" />
          </a>
          <a
            className={"indexType " + activeTab.Groups}
            href="#groups"
            data-toggle="tab"
            onClick={() => this.changeTab("Groups")}
          >
            <FormattedMessage id="communities" defaultMessage="Groups" />
          </a>
          <a
            className={"indexType " + activeTab.Challenges}
            href="#challenges"
            data-toggle="tab"
            onClick={() => this.changeTab("Challenges")}
          >
            <FormattedMessage id="challenges" defaultMessage="Challenges" />
          </a>
        </nav>
        <div className="tabContainer">
          {/* <div className="tab-content justify-content-center container-fluid"> */}
          <div className="tab-content">
            <div className={"tab-pane " + activeTab.Members} id="members">
              {activeIndex == "Members" ? (
                <Search
                  searchState={searchState}
                  index="User"
                  onSearchStateChange={this.onSearchStateChange}
                  refinements={[
                    { attribute: "skills" },
                    { attribute: "ressources" },
                    { attribute: "interests", searchable: false, limit: 17 },
                  ]}
                  sortByItems={[
                    { label: intl.formatMessage({ id: "general.filter.pop" }), index: "User" },
                    { label: intl.formatMessage({ id: "general.filter.people.date2" }), index: "User_id_des" },
                    { label: intl.formatMessage({ id: "general.filter.people.date1" }), index: "User_id_asc" },
                    { label: intl.formatMessage({ id: "general.filter.people.alpha1" }), index: "User_fname_asc" },
                    { label: intl.formatMessage({ id: "general.filter.people.alpha2" }), index: "User_fname_desc" },
                  ]}
                />
              ) : (
                <div>Not Mounted</div>
              )}
            </div>
            <div className={"tab-pane " + activeTab.Needs} id="needs">
              {activeIndex == "Needs" ? (
                <Search
                  searchState={searchState}
                  index="Need"
                  onSearchStateChange={this.onSearchStateChange}
                  refinements={[
                    { attribute: "skills" },
                    { attribute: "ressources" },
                    { attribute: "project.title" },
                    { attribute: "is_urgent", type: "toggle" },
                  ]}
                  sortByItems={[
                    { label: intl.formatMessage({ id: "general.filter.pop" }), index: "Need" },
                    { label: intl.formatMessage({ id: "general.members" }), index: "Need_members" },
                    { label: intl.formatMessage({ id: "general.filter.object.date2" }), index: "Need_id_des" },
                    { label: intl.formatMessage({ id: "general.filter.object.date1" }), index: "Need_id_asc" },
                  ]}
                />
              ) : (
                <div>Not Mounted</div>
              )}
            </div>
            <div className={"tab-pane " + activeTab.Projects} id="projects">
              {activeIndex == "Projects" ? (
                <Search
                  searchState={searchState}
                  index="Project"
                  onSearchStateChange={this.onSearchStateChange}
                  refinements={[{ attribute: "skills" }, { attribute: "interests", searchable: false }]}
                  sortByItems={[
                    { label: intl.formatMessage({ id: "general.filter.pop" }), index: "Project" },
                    { label: intl.formatMessage({ id: "entity.card.needs" }), index: "Project_needs" },
                    { label: intl.formatMessage({ id: "general.members" }), index: "Project_members" },
                    { label: intl.formatMessage({ id: "general.filter.object.date2" }), index: "Project_id_des" },
                    { label: intl.formatMessage({ id: "general.filter.object.date1" }), index: "Project_id_asc" },
                    { label: intl.formatMessage({ id: "general.filter.object.alpha1" }), index: "Project_title_asc" },
                    { label: intl.formatMessage({ id: "general.filter.object.alpha2" }), index: "Project_title_desc" },
                  ]}
                />
              ) : (
                <div>Not Mounted</div>
              )}
            </div>
            <div className={"tab-pane " + activeTab.Groups} id="groups">
              {activeIndex == "Groups" ? (
                <Search
                  searchState={searchState}
                  index="Community"
                  onSearchStateChange={this.onSearchStateChange}
                  refinements={[{ attribute: "skills" }, { attribute: "ressources" }]}
                  sortByItems={[
                    { label: intl.formatMessage({ id: "general.filter.pop" }), index: "Community" },
                    { label: intl.formatMessage({ id: "general.members" }), index: "Community_members" },
                    { label: intl.formatMessage({ id: "general.filter.object.date2" }), index: "Community_id_des" },
                    { label: intl.formatMessage({ id: "general.filter.object.date1" }), index: "Community_id_asc" },
                    { label: intl.formatMessage({ id: "general.filter.object.alpha1" }), index: "Community_title_asc" },
                    {
                      label: intl.formatMessage({ id: "general.filter.object.alpha2" }),
                      index: "Community_title_desc",
                    },
                  ]}
                />
              ) : (
                <div>Not Mounted</div>
              )}
            </div>
            <div className={"tab-pane " + activeTab.Challenges} id="challenges">
              {activeIndex == "Challenges" ? (
                <Search
                  searchState={searchState}
                  index="Challenge"
                  onSearchStateChange={this.onSearchStateChange}
                  refinements={[
                    { attribute: "skills" },
                    { attribute: "interests", searchable: false },
                    { attribute: "program.title", searchable: false },
                  ]}
                  sortByItems={[
                    { label: intl.formatMessage({ id: "general.filter.pop" }), index: "Challenge" },
                    { label: intl.formatMessage({ id: "general.filter.object.date2" }), index: "Challenge_id_des" },
                    { label: intl.formatMessage({ id: "general.filter.object.date1" }), index: "Challenge_id_asc" },
                  ]}
                />
              ) : (
                <div>Not Mounted</div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default injectIntl(SearchAll);
// Search.contextType = UserContext;
