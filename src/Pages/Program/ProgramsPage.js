import React, { Component } from "react";
import Api from "Api";
import ProgramList from "Components/Programs/ProgramList";

export default class ProgramsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listPrograms: [],
    };
  }

  componentDidMount() {
    Api.get("/api/programs/")
      .then((res) => {
        var listPrograms = [];
        console.log(res);
        res.data.map((program) => {
          return listPrograms.push(program);
        });
        // console.log(listPrograms);
        this.setState({ listPrograms: listPrograms });
      })
      .catch((error) => {
        // console.log(error);
      });
  }

  render() {
    var listPrograms = this.state.listPrograms;
    return (
      <div className="container-fluid">
        <div className="justify-content-center">
          <ProgramList searchBar={true} listPrograms={listPrograms} />
        </div>
      </div>
    );
  }
}
