import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { injectIntl, FormattedMessage } from "react-intl";
import { UserContext } from "UserProvider";
import MyFeed from "Components/Feed/MyFeed";
import ProjectList from "Components/Projects/ProjectList";
import CommunityList from "Components/Communities/CommunityList";
import NeedCard from "Components/Needs/Need/NeedCard";
import Api from "Api";
import Loading from "Components/Tools/Loading";
// import {Helmet} from "react-helmet";
import "./Home.scss";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: "",
      listProjects: [],
      listCommunities: [],
      loading: true,
      needs: [],
      program: [],
    };
  }

  updateInputValue(e) {
    this.setState({ inputValue: e.target.value, errors: "" });
  }

  displayNeeds(needs) {
    if (needs.length !== 0) {
      needs = needs.filter((need) => need.project);
      return needs.map((need, index) => <NeedCard need={need} cardFormat="compact" key={index} />);
    }
  }

  componentDidMount() {
    // get projects
    Api.get("/api/projects?items=4&order=desc")
      .then((res) => {
        var listProjects = res.data;
        this.setState({ listProjects, loading: false });
      })
      .catch((error) => {
        this.setState({ loading: false });
      });

    // get groups
    Api.get("/api/communities?items=4&order=desc")
      .then((res) => {
        var listCommunities = res.data;
        this.setState({ listCommunities, loading: false });
      })
      .catch((error) => {
        this.setState({ loading: false });
      });

    // get needs
    Api.get("/api/needs?items=3&order=desc").then((res) => {
      this.setState({ needs: res.data });
    });

    // get covid19 program
    Api.get("/api/programs/2").then((res) => {
      this.setState({ program: res.data });
    });
  }

  render() {
    let userContext = this.context;
    const { listProjects, listCommunities, needs, program, loading } = this.state;
    const signUp = {
      pathname: "/signup",
      email_value: this.state.inputValue,
    };
    var { intl } = this.props;
    var isTabActive = window.screen.width <= "768" ? "" : "active";
    var userLang = navigator.language || navigator.userLanguage;
    const lang =
      (localStorage.getItem("language") && localStorage.getItem("language") === "fr") ||
      (!localStorage.getItem("language") && (userLang === "fr-FR" || userLang === "fr"))
        ? "fr"
        : "en";
    if (userContext.connected) {
      return (
        <div className="connectedHome d-flex container-fluid">
          <nav className="nav nav-tabs">
            <a className="nav-item nav-link active" href="#feed" data-toggle="tab">
              <h5>
                <FormattedMessage id="feed.title" defaultMessage="Your Feed" />
              </h5>
            </a>
            <a className="nav-item nav-link" href="#latest" data-toggle="tab">
              <h5>
                <FormattedMessage id="home.latest" defaultMessage="What's new" />
              </h5>
            </a>
          </nav>

          <div className="tabContainer">
            <div className="tab-content row">
              <div className="tab-pane active col-12 col-md-8 col-lg-6" id="feed">
                <MyFeed displayCreate={true} feedId={this.context.user.feed_id} />
              </div>
              <div className={`tab-pane ${isTabActive} col-12 col-md-4 col-lg-6`} id="latest">
                <Loading active={loading} height="300px">
                  <div className="justify-content-center">
                    <h3>
                      <FormattedMessage id="home.featuredProgram" defaultMessage="Featured program" />
                    </h3>
                    <Link to={`/program/${program.short_title}`} className="featuredProgramLink">
                      <div className="featuredProgram">
                        <img src={program.banner_url} />
                        <div className="featuredProgram--content">
                          <h5>{lang === "fr" && program.title_fr ? program.title_fr : program.title}</h5>
                          <p>
                            {lang === "fr" && program.short_description_fr
                              ? program.short_description_fr
                              : program.short_description}
                          </p>
                        </div>
                      </div>
                    </Link>
                    <div className="latestNeeds">
                      <h3>
                        <FormattedMessage id="needs.latest" defaultMessage="Latest needs" />
                      </h3>
                      {this.displayNeeds(needs)}
                      <Link to="/needs" className="viewMoreRecents viewNeeds">
                        <FormattedMessage id="home.viewMoreRecent" defaultMessage="View more" />
                      </Link>
                    </div>
                    <ProjectList
                      searchBar={false}
                      listProjects={listProjects}
                      colMax={2}
                      displayHome={true}
                      cardFormat="compact"
                    />
                    <CommunityList
                      searchBar={false}
                      listCommunities={listCommunities}
                      colMax={2}
                      displayHome={true}
                      cardFormat="compact"
                    />
                  </div>
                </Loading>
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      // if users is connected
      return (
        <div className="Home">
          <div className="heading d-flex justify-content-center">
            <div className="container-fluid aboutJOGL">
              <h1>Just One Giant Lab</h1>
              <div className="aboutText">
                <p>
                  {intl.formatMessage({ id: "home.intro.1" })}{" "}
                  <strong>{intl.formatMessage({ id: "home.intro.2" })}</strong>{" "}
                  {intl.formatMessage({ id: "home.intro.3" })}
                </p>
                <p>
                  {intl.formatMessage({ id: "home.intro.4" })}
                  <br />
                  {intl.formatMessage({ id: "home.intro.5" })}{" "}
                  <strong>{intl.formatMessage({ id: "home.intro.6" })}</strong>
                </p>
                <p>
                  {intl.formatMessage({ id: "home.intro.7" })}
                  <strong>{intl.formatMessage({ id: "home.intro.8" })}</strong>
                  <br />
                  {intl.formatMessage({ id: "home.intro.9" })}{" "}
                  <strong>{intl.formatMessage({ id: "home.intro.10" })}</strong>
                </p>
                <p className="aboutJoin">
                  {intl.formatMessage({ id: "home.intro.11" })}
                  <span>
                    <Link to={signUp}>
                      <button className="btn btn-primary btn-home text-light font-weight-bold">
                        {intl.formatMessage({ id: "home.intro.join" })}
                      </button>
                    </Link>
                  </span>
                </p>
              </div>
            </div>
          </div>

          <div className="container-fluid d-block content text-center">
            <hr />
            <h2>
              <FormattedMessage id="home.join.title" defaultMessage="Join the movement" />
            </h2>
            <p className="joinMouv">
              <FormattedMessage
                id="home.join.text"
                defaultMessage="Come challenge yourself by fostering humanity’s open knowledge and developing solutions to the Sustainable Development Goals."
              />
            </p>
            <div className="cards row text-center justify-content-around">
              <div className="singleCard col-12 col-sm-6 col-md-4 col-xl-3">
                <div className="cardContent">
                  <img src={require("assets/img/default/default-project.jpg")} alt="Projects" />
                  <h3>
                    <FormattedMessage id="general.projects" defaultMessage="Projects" />
                  </h3>
                  <p>
                    <FormattedMessage
                      id="home.projects.text"
                      defaultMessage="You want to change the world? Create your project or join existing ones."
                    />
                  </p>
                  <Link to="/search/?i=Projects">
                    <button className="btn btn-primary">
                      <FormattedMessage id="home.projects.Btn" defaultMessage="Browse" />
                    </button>
                  </Link>
                </div>
              </div>
              <div className="singleCard col-12 col-sm-6 col-md-4 col-xl-3">
                <div className="cardContent">
                  <img src={require("assets/img/default/default-user.jpg")} alt="People" />
                  <h3>
                    <FormattedMessage id="general.members" defaultMessage="People" />
                  </h3>
                  <p>
                    <FormattedMessage
                      id="home.people.text"
                      defaultMessage="Meet wonderful people that will help you breaking challenges."
                    />
                  </p>
                  <Link to="/search/?i=Members">
                    <button className="btn btn-primary">
                      <FormattedMessage id="home.people.Btn" defaultMessage="Browse" />
                    </button>
                  </Link>
                </div>
              </div>
              <div className="singleCard col-12 col-sm-6 col-md-4 col-xl-3">
                <div className="cardContent">
                  <img src={require("assets/img/default/default-group.jpg")} alt="Communities" />
                  <h3>
                    <FormattedMessage id="communities" defaultMessage="Communities" />
                  </h3>
                  <p>
                    <FormattedMessage
                      id="home.community.text"
                      defaultMessage="Make your actions and needs visible and connect to inspiring communities."
                    />
                  </p>
                  <Link to="/search/?i=Groups">
                    <button className="btn btn-primary">
                      <FormattedMessage id="home.community.Btn" defaultMessage="Join" />
                    </button>
                  </Link>
                </div>
              </div>
              <div className="singleCard col-12 col-sm-6 col-md-4 col-xl-3">
                <div className="cardContent">
                  <img src={require("assets/img/default/default-challenge-old.jpg")} alt="Challenges" />
                  <h3>
                    <FormattedMessage id="challenges" defaultMessage="Challenges" />
                  </h3>
                  <p>
                    <FormattedMessage
                      id="home.challenges.text"
                      defaultMessage="Create, join teams and contribute to cracking challenges."
                    />
                  </p>
                  <Link to="/search/?i=Challenges">
                    <button className="btn btn-primary">
                      <FormattedMessage id="home.challenges.Btn" defaultMessage="Compete" />
                    </button>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}
export default injectIntl(Home);
Home.contextType = UserContext;
