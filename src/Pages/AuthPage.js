import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import ChangePwd from "Components/Authentification/ChangePwd";
import ForgotPwd from "Components/Authentification/ForgotPwd";
import NewPwd from "Components/Authentification/NewPwd";
import { Link } from "react-router-dom";
import Logo from "assets/img/logo.svg";
import "Components/Authentification/Auth.scss";

export default class AuthPage extends Component {
  render() {
    const rootURL = "/auth/";
    return (
      <div className="auth-form row align-items-center">
        <div className="col-12 col-lg-5 leftPannel d-flex align-items-center justify-content-center">
          <Link to="/">
            <img src={Logo} className="logo" alt="JoGL icon" />
          </Link>
        </div>
        <div className="col-12 col-lg-7 rightPannel">
          <Switch>
            <Route path={rootURL + "new-password"} exact={true} component={NewPwd} />
            <Route path={rootURL + "change-password"} exact={true} component={ChangePwd} />
            <Route path={rootURL + "forgot-password"} exact={true} component={ForgotPwd} />
          </Switch>
        </div>
      </div>
    );
  }
}
// render={(props) => <NewPwd user={this.state.user} pars={this.props.location}/>} />
// ?:access-token&:client&:client_id&:config&:expiry&:reset_password&:token&:uid
// http://localhost:3000/auth/new-password? access-token= vQycH-ZUtWeyGFzhIym-kw & client=CT0dJPHhK9SCZWatqcaohA & client_id=CT0dJPHhK9SCZWatqcaohA & config=default & expiry=1559034874 & reset_password=true & token=vQycH-ZUtWeyGFzhIym-kw & uid=test%40test.com
