import React, { useState, useRef } from "react";
import ReactDOM from "react-dom";
import SearchAll from "Components/Search/SearchAll";
import "./SearchPage.scss";
import { UserContext } from "UserProvider";
import { FormattedMessage, injectIntl } from "react-intl";

const App = (props) => {
  const containerRef = useRef(null);
  const headerRef = useRef(null);
  const { intl, location, history } = props;

  return <SearchAll search={props.location.search} />;
};

export default injectIntl(App);
