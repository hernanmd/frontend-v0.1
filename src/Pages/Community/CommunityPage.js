import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import CommunityCreate from "Components/Communities/CommunityCreate";
import CommunityDetails from "Components/Communities/CommunityDetails";
import CommunityEdit from "Components/Communities/CommunityEdit";

export default class CommunityPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultURL: "/community",
    };
  }

  render() {
    return (
      <div>
        <Switch>
          <Route path={this.state.defaultURL + "/create"} exact={true} component={CommunityCreate} />
          <Route path={this.state.defaultURL + "/:id"} exact={true} component={CommunityDetails} />
          <Route path={this.state.defaultURL + "/:id/edit"} exact={true} component={CommunityEdit} />
        </Switch>
      </div>
    );
  }
}
