import React, { Component, Fragment } from "react";
import Api from "Api";
import { injectIntl } from "react-intl";
import CommunityList from "Components/Communities/CommunityList";
import CommunitiesHeader from "../../Components/Communities/CommunitiesHeader";
import Loading from "../../Components/Tools/Loading";
import Filter from "Components/Tools/Filter";
import { Link } from "react-router-dom";
import { UserContext } from "UserProvider";
import { infiniteLoader } from "Components/Tools/utils/utils.js";

class CommunitiesPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listCommunities: [],
      loading: true,
      loadingBtn: false,
      loadBtnCount: 1,
      hideLoadBtn: false,
    };
  }

  myCallback = (dataFromChild) => {
    this.setState({ listDataFromChild: dataFromChild }); // send data to filter component (child)
  };

  loadGroups(currentPage) {
    var itemsPerQuery = 12; // number of users per query calls (load more btn click)
    if (currentPage > 1) this.setState({ loadingBtn: true });
    Api.get(`/api/communities?items=${itemsPerQuery}&page=${currentPage}&order=desc`)
      .then((res) => {
        const totalPages = res.headers["total-pages"]; // get total pages from response headers
        var listCommunities = this.state.listCommunities;
        var communities = res.data;
        communities.map((community) => {
          // map
          return listCommunities.push(community);
        });
        this.setState({ listCommunities, loading: false, loadingBtn: false, loadBtnCount: (currentPage += 1) });
        currentPage <= totalPages && infiniteLoader(); // relaunch function at each call, if we haven't reached last page
        if (currentPage == totalPages) this.state.hideLoadBtn = true; // hide btn when the last "page" has been called
      })
      .catch((error) => {
        this.setState({ loading: false, loadingBtn: false });
      });
  }

  componentDidMount() {
    this.setState({ loading: true });
    this.loadGroups(this.state.loadBtnCount);
  }

  render() {
    const { listCommunities, loading, loadingBtn, hideLoadBtn } = this.state;
    const { intl } = this.props;
    var btnHiddenClass = hideLoadBtn ? "d-none" : "";
    var showFilterClass = !hideLoadBtn ? "d-none" : "";
    let userContext = this.context;
    return (
      <div className="container-fluid">
        <CommunitiesHeader />
        {/* start - Filter and create button */}
        <div className="row justify-content-around communitySearch">
          <div className="col-6">
            <div className={`${showFilterClass}`}>
              <Filter list={listCommunities} callback={this.myCallback} />
            </div>
          </div>
          <div className="col-6 text-right">
            {userContext.connected && (
              <Link to="/community/create">
                <button className="btn btn-sm btn-primary btn-action">
                  {intl.formatMessage({ id: "communities.searchBar.btnAddCommunity" })}
                </button>
              </Link>
            )}
          </div>
        </div>
        {/* end - Filter and create button */}
        <Loading active={loading} height="300px">
          <div className="justify-content-center">
            <CommunityList searchBar={true} listCommunities={listCommunities} />
            <button
              type="button"
              className={`btn btn-primary loadBtn ${btnHiddenClass}`}
              onClick={() => this.loadGroups(this.state.loadBtnCount)}
            >
              {loadingBtn && (
                <Fragment>
                  <span
                    className="spinner-border spinner-border-sm text-center"
                    role="status"
                    aria-hidden="true"
                  ></span>
                  &nbsp;
                </Fragment>
              )}
              {intl.formatMessage({ id: "general.load" })}
            </button>
          </div>
        </Loading>
      </div>
    );
  }
}
export default injectIntl(CommunitiesPage);
CommunitiesPage.contextType = UserContext;
