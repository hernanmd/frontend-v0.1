import React, { Component, Fragment } from "react";
import { UserContext } from "UserProvider";
import Api from "Api";
import Loading from "Components/Tools/Loading";
import NeedDisplay from "Components/Needs/Need/NeedDisplay";
import { FormattedMessage } from "react-intl";
import { infiniteLoader } from "Components/Tools/utils/utils.js";
import "./NeedsPage.scss";

export default class NeedsPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      needs: [],
      loading: true,
      loadingBtn: false,
      loadBtnCount: 1,
      hideLoadBtn: false,
    };
  }

  loadNeeds(currentPage) {
    var itemsPerQuery = 10; // number of users per query calls (load more btn click)
    if (currentPage > 1) this.setState({ loadingBtn: true });
    Api.get(`/api/needs?items=${itemsPerQuery}&page=${currentPage}&order=desc`)
      // Api.get(`/api/needs`)
      .then((res) => {
        const totalPages = res.headers["total-pages"]; // get total pages from response headers
        var needs = this.state.needs;
        var newneeds = res.data;
        newneeds.map((need) => {
          // map
          return needs.push(need);
        });
        this.setState({ needs, loading: false, loadingBtn: false, loadBtnCount: (currentPage += 1) });
        currentPage <= totalPages && infiniteLoader(); // relaunch function at each call, if we haven't reached last page
        if (currentPage == totalPages) this.state.hideLoadBtn = true; // hide btn when the last "page" has been called
      })
      .catch((error) => {
        // console.log(error);
        this.setState({ loading: false, loadingBtn: false });
      });
  }

  getNeedsApi() {
    Api.get("/api/needs?items=10&order=desc")
      .then((res) => {
        this.setState({ needs: res.data });
      })
      .catch((error) => {
        // console.log(error);
      });
  }

  componentDidMount() {
    this.setState({ loading: true });
    this.loadNeeds(this.state.loadBtnCount);
  }

  refresh() {
    // on refresh, get 5 latest needs
    // this.getNeedsApi();
  }

  displayNeeds(needs) {
    if (needs.length !== 0) {
      return needs
        .filter((need) => need.project)
        .map((need, index) => (
          <div className="col-12 col-lg-6" key={index}>
            <NeedDisplay need={need} refresh={this.refresh.bind(this)} />
          </div>
        ));
    }
  }

  render() {
    const { needs, loading, loadingBtn, hideLoadBtn } = this.state;
    var btnHiddenClass = hideLoadBtn ? "d-none" : "";
    // var btnHiddenClass = hideLoadBtn ? "d-none" : "d-none"
    // var results = needs.reduce(function(results, org) {
    // 	(results[org.project.id] = results[org.project.id] || []).push(org);
    // 	return results;
    // }, {})
    return (
      <div className="container-fluid needsPage">
        <div className="projectsHeader">
          <h1>
            <FormattedMessage id="needsPage.header.title" defaultMessage="Explore needs" />
          </h1>
          <p>
            <FormattedMessage
              id="needsPage.header.description"
              defaultMessage="Explore all the needs of the platform"
            />
          </p>
          <hr />
        </div>
        <Loading active={loading} height="300px">
          <div className="row needs">{this.displayNeeds(needs)}</div>
          <button
            type="button"
            className={`btn btn-primary loadBtn ${btnHiddenClass}`}
            onClick={() => this.loadNeeds(this.state.loadBtnCount)}
          >
            {loadingBtn && (
              <Fragment>
                <span className="spinner-border spinner-border-sm text-center" role="status" aria-hidden="true"></span>
                &nbsp;
              </Fragment>
            )}
            <FormattedMessage id="general.load" defaultMessage="Load more" />
          </button>
        </Loading>
      </div>
    );
  }
}

NeedsPage.contextType = UserContext;
