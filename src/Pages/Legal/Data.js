import React, { Component } from "react";
import "./legal.scss";

export default class Data extends Component {
  render() {
    var userLang = navigator.language || navigator.userLanguage;
    // if website or browser is in french
    if (
      (localStorage.getItem("language") && localStorage.getItem("language") === "fr") ||
      (!localStorage.getItem("language") && (userLang === "fr-FR" || userLang === "fr"))
    )
      return (
        <div className="legal data container-fluid">
          <h1>Politique en matière des données de l’utilisateur</h1>
          <p>Cette section est dédiée aux informations que nous traitons pour le fonctionnement de JOGL.</p>
          <h2>Contact</h2>
          <p>
            Just One Giant Lab est une organisation à but non lucratif, association régie par la loi du 1er juillet
            1901. Numéro SIRET : 82974635300020
            <br />
            Adresse du siège social de JOGL : Just One Giant Lab, MVAC, 23 Rue Greneta 75002, PARIS, FRANCE.
            <br />
            Pour toute question relative à la collecte, au traitement, à la conservation et/ou à la sécurité de vos
            données personnelles dont vous ne trouvez pas la réponse dans les rubriques ci-dessous, veuillez adresser
            votre demande à <a href="mailto:data@jogl.io">data[at]jogl.io</a>
          </p>
          <h2>Caractère obligatoire et optionnel des données personnelles de l’utilisateur collectées par JOGL</h2>

          <p>
            Des informations personnelles à caractère obligatoire marquées par le symbole * vous sont demandées afin de
            vous garantir la meilleure expérience possible sur JOGL, personnaliser le contenu et les interactions que
            nous pouvons vous proposer sur la plateforme.
            <br />
            Pour les informations personnelles à caractère optionnel, vous conservez à tout moment le choix de fournir
            ou supprimer ces informations.
            <br />
            <br />
            Voici la liste des données personnelles d’un utilisateur recueillies par JOGL :{" "}
          </p>

          <table className="table">
            <thead>
              <tr>
                <th scope="col">Donnée</th>
                <th scope="col">Caractère</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>email</td>
                <td>obligatoire</td>
              </tr>
              <tr>
                <td>date de naissance</td>
                <td>optionnel</td>
              </tr>
              <tr>
                <td>nom</td>
                <td>obligatoire</td>
              </tr>
              <tr>
                <td>prénom</td>
                <td>obligatoire</td>
              </tr>
              <tr>
                <td>pseudonyme</td>
                <td>obligatoire</td>
              </tr>
              <tr>
                <td>adresse</td>
                <td>optionnel</td>
              </tr>
              <tr>
                <td>ville</td>
                <td>optionnel</td>
              </tr>
              <tr>
                <td>pays</td>
                <td>optionnel</td>
              </tr>
              <tr>
                <td>avatar</td>
                <td>optionnel</td>
              </tr>
              <tr>
                <td>téléphone</td>
                <td>optionnel</td>
              </tr>
              <tr>
                <td>occupation principale</td>
                <td>obligatoire</td>
              </tr>
              <tr>
                <td>intérêts</td>
                <td>obligatoire</td>
              </tr>
              <tr>
                <td>compétences</td>
                <td>obligatoire</td>
              </tr>
              <tr>
                <td>société/école</td>
                <td>obligatoire</td>
              </tr>
            </tbody>
          </table>

          <h2>Finalités des données personnelles de l’utilisateur collectées par JOGL</h2>

          <p>
            Voici la liste des données personnelles de l’utilisateur recueillie par JOGL et la finalité de chaque donnée
            collectée :
          </p>

          <table className="table">
            <thead>
              <tr>
                <th scope="col">Donnée</th>
                <th scope="col">Finalité</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>email</td>
                <td>garantit l’inscription de l’utilisateur sur la plateforme et garantit l’unicité du compte</td>
              </tr>
              <tr>
                <td>date de naissance</td>
                <td>garantit un contrôle restrictif au mineur de moins de 18 ans</td>
              </tr>
              <tr>
                <td>nom</td>
                <td>affichage d’une entité unique et personnalisable sur la plateforme</td>
              </tr>
              <tr>
                <td>prénom</td>
                <td>affichage d’une entité unique et personnalisable sur la plateforme</td>
              </tr>
              <tr>
                <td>pseudonyme</td>
                <td>garantit l’exclusivité du nom choisi et permet le référencement par marquage mécanique</td>
              </tr>
              <tr>
                <td>adresse</td>
                <td>le niveau le plus détaillé de l’emplacement de l’utilisateur sur les cartes de JOGL</td>
              </tr>
              <tr>
                <td>ville</td>
                <td>niveau intermédiaire de la position de l’utilisateur sur les cartes de JOGL</td>
              </tr>
              <tr>
                <td>pays</td>
                <td>niveau minimum de localisation de l’utilisateur sur les cartes de JOGL</td>
              </tr>
              <tr>
                <td>avatar</td>
                <td>augmenter la personnalisation du profil de l’utilisateur</td>
              </tr>
              <tr>
                <td>téléphone</td>
                <td>permet d’être contacté par ce moyen de communication</td>
              </tr>
              <tr>
                <td>occupation principale</td>
                <td>
                  permet d’orienter l’utilisateur vers des projets, des programmes, des communautés similaires et/ou
                  complémentaires à son occupation
                </td>
              </tr>
              <tr>
                <td>intérêts</td>
                <td>
                  orienter l’utilisateur vers des projets, des programmes et des communautés qui partagent des intérêts
                  similaires
                </td>
              </tr>
              <tr>
                <td>compétences</td>
                <td>
                  proposer des projets, des programmes, des besoins, des communautés pour apprendre, acquérir de
                  l’expérience, revoir ou encadrer les autres
                </td>
              </tr>
              <tr>
                <td>société/école</td>
                <td>
                  aide à donner de la crédibilité, de la légitimité à l’utilisateur en fonction de la réputation de
                  l’établissement
                </td>
              </tr>
            </tbody>
          </table>

          <h2>Durée de conservation des données personnelles de l’utilisateur recueillies par JOGL</h2>
          <p>
            Les données personnelles de l’utilisateur collectées par JOGL sont conservées jusqu’à la suppression de son
            compte par l’utilisateur.
          </p>

          <h2>Destinataires des données personnes de l’utilisateur collectées par JOGL</h2>
          <p>Les données personnelles de l’utilisateur recueillies peuvent être consultées au sein de JOGL par :</p>
          <ul>
            <li>
              La direction et le service informatique : dans le but d’assurer et de corriger tout dysfonctionnement.
            </li>
            <li>
              La personne déléguée à la protection des données : dans le cadre et si nécessaire d’une Analyse d’Impact
              sur la Protection des Données.
            </li>
            <li>
              La personne responsable de la communication: si nécessaire, concernant les adresses mails dans le cadre de
              l’envoi de publications (newsletter) à destination des utilisateurs qui ont consenti à les recevoir.
            </li>
          </ul>
          <p>
            Également par le sous-traitant HEROKU, qui héberge notre serveur informatique en Europe et a la capacité
            d’accéder aux données comme tout fournisseur.
          </p>

          <h2>Vos droits en tant qu’utilisateur concernant vos données personnelles</h2>

          <p>
            Pour toute demande relative à l’exercice de vos droits concernant le traitement de vos données personnelles,
            adressez vous directement par mail à <a href="mailto:data@jogl.io">data[at]jogl.io</a>
          </p>

          <h5>ACCÈS</h5>

          <p>Vous avez le droit de connaître les informations qui sont détenues sur vous.</p>

          <h5>OPPOSITION</h5>

          <p>
            Le droit d’opposition vous permet de vous opposer à ce que vos données soient utilisées par un organisme
            pour un objectif précis. Vous devez mettre en avant “des raisons tenant à votre situation particulière”,
            sauf en cas de prospection commerciale, à laquelle vous pouvez vous opposer sans motif.
          </p>

          <h5>RÉCTIFICATION</h5>

          <p>
            Le droit de rectification permet de corriger des données inexactes vous concernant (âge ou adresse erronés)
            ou de compléter des données (adresse sans le numéro de l’appartement) en lien avec la finalité du
            traitement.
          </p>

          <h5>ÉFFACEMENT</h5>

          <p>
            Qu’il s’agisse d’une photo gênante sur un site internet ou d’une information collectée par un organisme que
            vous jugez inutile, vous pouvez obtenir son effacement si au moins une de ces situations correspond à votre
            cas :
          </p>
          <ul>
            <li>Vos données sont utilisées à des fins de prospection;</li>
            <li>
              Les données ne sont pas ou plus nécessaires au regard des objectifs pour lesquelles elles ont été
              initialement collectées ou traitées;
            </li>
            <li>Vous retirez votre consentement à l’utilisation de vos données;</li>
            <li>Vos données font l’objet d’un traitement illicite (par exemple : publication de données piratées);</li>
            <li>
              Vos données ont été collectées lorsque que vous étiez mineur dans le cadre de la société de l’information
              (blog, forum, réseau social, site web...);
            </li>
            <li>Vos données doivent être effacées pour respecter une obligation légale;</li>
            <li>
              Vous vous êtes opposé au traitement de vos données et le responsable du fichier n’a pas de motif légitime
              ou impérieux de ne pas donner suite à cette demande
            </li>
          </ul>

          <h5>LIMITATION</h5>

          <p>
            Le droit à la limitation de vos données est un droit qui complète vos autres droits (rectification,
            opposition…). Si vous contestez l’exactitude des données utilisées par l’organisme ou que vous vous opposez
            à ce que vos données soient traitées, la loi autorise l’organisme à procéder à une vérification ou un examen
            de votre demande pendant un certain délai. Pendant ce délai, vous avez la possibilité de demander à
            l’organisme de geler l’utilisation de vos données. Concrètement, il ne devra plus utiliser les données mais
            devra les conserver.
            <br />
            Inversement, vous pouvez demander directement la limitation de certaines données dans le cas où l’organisme
            souhaite lui-même les effacer. Cela vous permettra de conserver les données par exemple afin d’exercer un
            droit.
          </p>

          <h5>PORTABILITÉ</h5>

          <p>
            Le droit à la portabilité offre aux personnes la possibilité de récupérer une partie de leurs données dans
            un format ouvert et lisible par machine. Elles peuvent ainsi les stocker ou les transmettre facilement d’un
            système d’information à un autre, en vue de leur réutilisation à des fins personnelles.
            <br />
            Ce droit s’applique si ces trois conditions sont toutes réunies :
          </p>
          <ul>
            <li>
              1. Le droit à la portabilité est limité aux données personnelles fournies par la personne concernée.
            </li>
            <li>
              2. Il ne s’applique que si les données sont traitées de manière automatisée (les fichiers papiers ne sont
              donc pas concernés) et sur la base du consentement préalable de la personne concernée ou de l’exécution
              d’un contrat conclu avec la personne concernée.
            </li>
            <li>
              3. L’exercice du droit à la portabilité ne doit pas porter atteinte aux droits et libertés de tiers, dont
              les données se trouveraient dans les données transmises suite à une demande de portabilité.
            </li>
          </ul>
          <p>
            Source et plus d’informations sur le site{" "}
            <a href="https://www.cnil.fr/professionnel" target="_blank">
              CNIL
            </a>
          </p>
        </div>
      );
    else {
      // else, english
      return (
        <div className="legal data container-fluid">
          <h1>User data policy</h1>
          <p>This section is dedicated to the information we process for the functioning of JOGL.</p>
          <h2>Contact</h2>
          <p>
            Just One Giant Lab is a non-profit organization governed by the French law of July 1, 1901.
            <br />
            SIRET number: 82974635300020
            <br />
            JOGL head office address: Just One Giant Lab, MVAC, 23 Rue Greneta 75002, PARIS, FRANCE.
            <br />
            If you have any questions regarding the collection, processing, storage and/or security of your personal
            data that are not answered in the sections below, please send your request to{" "}
            <a href="mailto:data@jogl.io">data[at]jogl.io</a>
          </p>
          <h2>Mandatory and optional nature of the user's personal data collected by JOGL</h2>

          <p>
            Mandatory personal information, signaled by the symbol *, is requested from you in order to guarantee the
            best possible JOGL experience, personalize content and interactions we can offer on the platform.
            <br />
            For personal information of an optional nature, you conserve at any time the choice to provide or remove
            this information. <br />
            <br />
            Here is the list collected by JOGL of the user’s personal data:
          </p>

          <table className="table">
            <thead>
              <tr>
                <th scope="col">Data</th>
                <th scope="col">Nature</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>email</td>
                <td>mandatory</td>
              </tr>
              <tr>
                <td>date of birth</td>
                <td>optional</td>
              </tr>
              <tr>
                <td>name</td>
                <td>mandatory</td>
              </tr>
              <tr>
                <td>first name</td>
                <td>mandatory</td>
              </tr>
              <tr>
                <td>alias</td>
                <td>mandatory</td>
              </tr>
              <tr>
                <td>address</td>
                <td>optional</td>
              </tr>
              <tr>
                <td>city</td>
                <td>optional</td>
              </tr>
              <tr>
                <td>country</td>
                <td>optional</td>
              </tr>
              <tr>
                <td>avatar</td>
                <td>optional</td>
              </tr>
              <tr>
                <td>phone</td>
                <td>optional</td>
              </tr>
              <tr>
                <td>main occupation</td>
                <td>mandatory</td>
              </tr>
              <tr>
                <td>interests</td>
                <td>mandatory</td>
              </tr>
              <tr>
                <td>skills</td>
                <td>mandatory</td>
              </tr>
              <tr>
                <td>company/school</td>
                <td>mandatory</td>
              </tr>
            </tbody>
          </table>

          <h2>Reasons behind the collection of the user's personal data by JOGL</h2>

          <p>Here is the list of the user's personal data collected by JOGL and the purpose of each data collected: </p>

          <table className="table">
            <thead>
              <tr>
                <th scope="col">Data</th>
                <th scope="col">Purpose</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>email</td>
                <td>guarantees the user's registration on the platform</td>
              </tr>
              <tr>
                <td>date of birth</td>
                <td>guarantees restrictive access control to minors under 18 years of age </td>
              </tr>
              <tr>
                <td>name</td>
                <td>display of a unique and customizable identity on the platform</td>
              </tr>
              <tr>
                <td>first name</td>
                <td>display of a unique and customizable identity on the platform</td>
              </tr>
              <tr>
                <td>alias</td>
                <td>guarantees the chosen name exclusivity and allows for referencing by tagging mechanics</td>
              </tr>
              <tr>
                <td>address</td>
                <td>the most detailed level of the user’s location on JOGL’s maps</td>
              </tr>
              <tr>
                <td>city</td>
                <td>intermediate level of the user’s location on JOGL’s maps</td>
              </tr>
              <tr>
                <td>country</td>
                <td>minimum level of the user’s location on JOGL’s maps</td>
              </tr>
              <tr>
                <td>avatar</td>
                <td>increase personalization of the user’s profile</td>
              </tr>
              <tr>
                <td>phone</td>
                <td>allows the user to be contacted by this means of communication</td>
              </tr>
              <tr>
                <td>main occupation</td>
                <td>
                  to direct users towards projects, programs, communities similar and/or complementary to his occupation
                </td>
              </tr>
              <tr>
                <td>interests</td>
                <td>to direct the user towards projects, programs, communities, which share similar interests</td>
              </tr>
              <tr>
                <td>skills</td>
                <td>
                  propose projects, programs, needs, communities to learn, gain experience, review or mentor others
                </td>
              </tr>
              <tr>
                <td>company/school</td>
                <td>
                  to help provide credibility, legitimacy to the user according to the reputation of the establishment
                </td>
              </tr>
            </tbody>
          </table>

          <h2>Duration of storage of the user's personal data collected by JOGL</h2>
          <p>
            The user's personal data collected by JOGL is kept until it is deleted by the action of the user on its
            account.
          </p>

          <h2>Recipients of personal data collected by JOGL</h2>
          <p>The user’s personal data collected can be consulted within JOGL by:</p>
          <ul>
            <li>Management and IT Department: in order to ensure and correct any malfunction.</li>
            <li>
              The Data Protection Officer: within the framework, and if necessary, of an Impact Assessment on data
              protection
            </li>
            <li>
              The Communication Manager: if necessary the email addresses, first and last names in the context of
              sending publications (newsletters) to users who have consented to receive them.
            </li>
          </ul>
          <p>
            Furthermore the subcontractor HEROKU, which hosts our IT server in Europe has the capacity to access the
            data as any provider does.
          </p>

          <h2>Your rights regarding your personal data</h2>

          <p>
            For any request relating to the exercise of your rights concerning the processing of your personal data,
            please contact us directly by email at <a href="mailto:data@jogl.io">data[at]jogl.io</a>
          </p>

          <h5>ACCESS</h5>

          <p>You have the right to know what information is held about you.</p>

          <h5>OPPOSITION</h5>

          <p>
            The right of opposition allows you to object to the use of your data by an organization for a specific
            purpose. You must highlight "reasons for your specific situation and request", except in the case of
            commercial prospecting, to which you may object without cause.
          </p>

          <h5>RECTIFICATION</h5>

          <p>
            The right of rectification makes it possible to correct any inaccurate data about you (an incorrect age or
            address for example) or to complete data (address without an apartment number) in relation with the
            processing purposes.
          </p>

          <h5>DELETION</h5>

          <p>
            Whether it is an embarrassing photo on a website or information collected by an organization that you
            consider unnecessary, you can obtain its deletion if at least one of these situations corresponds to your
            case:
          </p>
          <ul>
            <li>Your data is used for prospecting purposes;</li>
            <li>
              The data is not or no longer necessary for the purposes for which it is used, was initially collected or
              processed;
            </li>
            <li>You withdraw your consent to the use of your data;</li>
            <li>Your data is unlawfully processed (e. g. publication of pirated data);</li>
            <li>
              Your data was collected when you were a minor in the context of information society(blog, forum, social
              network, website...);
            </li>
            <li>Your data must be deleted to comply with a legal obligation;</li>
            <li>
              You have objected to the processing of your data and the person responsible for the file has no legitimate
              or compelling reason not to comply with this request
            </li>
          </ul>

          <h5>LIMITATION</h5>

          <p>
            The right to limit your data is a right that complements your other rights (rectification, opposition...).
            If you question the accuracy of the data used by the organization or if you object to the processing of your
            data, the law authorizes the organization to carry out an audit or to review your application for a certain
            period of time. During this period, you have the opportunity to ask the organization to freeze the use of
            your data. In practice, it will no longer have to use the data but will have to keep them. <br />
            Conversely, you can directly request the limitation of certain data in the event that the organization
            itself wishes to delete them. This will allow you to keep the data, for example in order to exercise a
            right.
          </p>

          <h5>PORTABILITY</h5>

          <p>
            The right to portability offers people the possibility to recover part of their data in an open and
            machine-readable format. They can thus store or transmit them easily from one information system to another,
            for personal use.
            <br />
            This right applies if all three conditions are met:
          </p>
          <ul>
            <li>1. The right to portability is limited to personal data provided by the data subject.</li>
            <li>
              2. It only applies if the data are processed automatically (paper files are not therefore not concerned)
              and on the basis of the prior consent of the data subject or the execution of a contract concluded with
              the person concerned.
            </li>
            <li>
              3. The exercise of the right to portability must not infringe the rights and freedoms of third parties,
              including data would be found in the data transmitted following a request for portability.
            </li>
          </ul>
          <p>
            Source and more information on the site{" "}
            <a href="https://www.cnil.fr/professionnel" target="_blank">
              CNIL
            </a>
          </p>
        </div>
      );
    }
  }
}
