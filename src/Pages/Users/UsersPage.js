import React, { useRef } from "react";
import PeopleCard from "Components/People/PeopleCard";
import { FormattedMessage, injectIntl } from "react-intl";
import {
  InstantSearch,
  RefinementList,
  SortBy,
  Pagination,
  ClearRefinements,
  Hits,
  HitsPerPage,
  Panel,
  SearchBox,
} from "react-instantsearch-dom";
import algoliasearch from "algoliasearch/lite";
import { ClearFiltersMobile, NoResults, ResultsNumberMobile, SaveFiltersMobile } from "./widgets";
import withURLSync from "./URLSync";
import "../../Components/Main/AlgoliaResultsPages.scss";
import $ from "jquery";
const searchClient = algoliasearch(process.env.REACT_APP_ALGOLIA_APP_ID, process.env.REACT_APP_ALGOLIA_TOKEN);

const Hit = ({ hit }) => <PeopleCard user={hit} />;

const App = (props) => {
  const containerRef = useRef(null);
  const headerRef = useRef(null);

  function openFilters() {
    document.querySelector(".UsersPage").classList.add("filtering");
    window.scrollTo(0, 0);
    window.addEventListener("keyup", onKeyUp);
    window.addEventListener("click", onClick);
  }

  function closeFilters() {
    document.querySelector(".UsersPage").classList.remove("filtering");
    containerRef.current.scrollIntoView();
    window.removeEventListener("keyup", onKeyUp);
    window.removeEventListener("click", onClick);
  }

  function onKeyUp(event) {
    if (event.key !== "Escape") {
      return;
    }

    closeFilters();
  }

  function onClick(event) {
    if (event.target !== headerRef.current) {
      return;
    }

    closeFilters();
  }

  function stickyFilterBtn() {
    var filtersBtn = $(".filters-button"),
      pagination = $(".ais-Pagination"),
      pagiOffset = pagination.offset().top, // element distance from top of page
      btnH = filtersBtn.outerHeight(), // height of button element
      pagiH = pagination.outerHeight(), // height of pagination element
      offsetFix = 95, // offset distance between pagi and button elements
      windH = $(window).height(); // window height
    $(window).scroll(function () {
      var currScrollPost = $(window).scrollTop(); // current y scroll position in the page (in px)
      if (currScrollPost > pagiOffset + pagiH + (offsetFix + btnH) - windH) {
        // execute only if we get pass the pagination element
        filtersBtn.addClass("sticky"); // add sticky class so filter btn is sticked to bottom of page
      } else {
        filtersBtn.removeClass("sticky"); // remove sticky class
      }
    });
  }

  const { intl } = props;
  $(window).on("load", function () {
    stickyFilterBtn(); // launch function when document has loaded
  });
  return (
    <div className="UsersPage AlgoliaResulstsPages container-fluid">
      <InstantSearch
        searchClient={searchClient}
        indexName="User"
        searchState={props.searchState}
        createURL={props.createURL}
        onSearchStateChange={props.onSearchStateChange}
      >
        <header className="header peopleHeader" ref={headerRef}>
          <h1>
            <FormattedMessage id="people.header.title" defaultMessage="Explore people" />
          </h1>
          <SearchBox
            translations={{
              placeholder: intl.formatMessage({ id: "algolia.search.placeholder" }),
            }}
            submit={
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 18 18">
                <g
                  fill="none"
                  fillRule="evenodd"
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="1.67"
                  transform="translate(1 1)"
                >
                  <circle cx="7.11" cy="7.11" r="7.11" />
                  <path d="M16 16l-3.87-3.87" />
                </g>
              </svg>
            }
          />
        </header>

        <main className="d-flex" ref={containerRef}>
          <div className="container-wrapper">
            <section className="container-filters" onKeyUp={onKeyUp}>
              <div className="container-header">
                <p className="filters">{intl.formatMessage({ id: "algolia.filters" })}</p>

                <div className="clear-filters" data-layout="desktop">
                  <ClearRefinements
                    translations={{
                      reset: (
                        <>
                          <svg xmlns="http://www.w3.org/2000/svg" width="11" height="11" viewBox="0 0 11 11">
                            <g fill="none" fillRule="evenodd" opacity=".4">
                              <path d="M0 0h11v11H0z" />
                              <path
                                fill="#000"
                                fillRule="nonzero"
                                d="M8.26 2.75a3.896 3.896 0 1 0 1.102 3.262l.007-.056a.49.49 0 0 1 .485-.456c.253 0 .451.206.437.457 0 0 .012-.109-.006.061a4.813 4.813 0 1 1-1.348-3.887v-.987a.458.458 0 1 1 .917.002v2.062a.459.459 0 0 1-.459.459H7.334a.458.458 0 1 1-.002-.917h.928z"
                              />
                            </g>
                          </svg>
                          {intl.formatMessage({ id: "algolia.filters.clear" })}
                        </>
                      ),
                    }}
                  />
                </div>

                <div className="clear-filters">
                  <ResultsNumberMobile />
                </div>
              </div>

              <div className="container-body">
                <Panel header={intl.formatMessage({ id: "user.profile.skills" })}>
                  <RefinementList
                    attribute="skills"
                    searchable={true}
                    translations={{
                      placeholder: intl.formatMessage({ id: "algolia.skills.placeholder" }),
                      noResults: intl.formatMessage({ id: "algolia.noResult" }),
                    }}
                  />
                </Panel>
                <Panel header={intl.formatMessage({ id: "algolia.interests" })}>
                  <RefinementList
                    attribute="interests"
                    searchable={true}
                    translations={{
                      placeholder: intl.formatMessage({ id: "algolia.interests.placeholder" }),
                      noResults: intl.formatMessage({ id: "algolia.noResult" }),
                    }}
                  />
                </Panel>
              </div>
            </section>

            <div className="container-filters-footer" data-layout="mobile">
              <div className="container-filters-footer-button-wrapper">
                <ClearFiltersMobile containerRef={containerRef} />
              </div>

              <div className="container-filters-footer-button-wrapper">
                <SaveFiltersMobile onClick={closeFilters} />
              </div>
            </div>
          </div>

          <section className="container-results">
            <header className="container-header container-options">
              <SearchBox
                translations={{
                  placeholder: intl.formatMessage({ id: "algolia.search.placeholder" }),
                }}
                submit={
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 18 18">
                    <g
                      fill="none"
                      fillRule="evenodd"
                      stroke="currentColor"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="1.67"
                      transform="translate(1 1)"
                    >
                      <circle cx="7.11" cy="7.11" r="7.11" />
                      <path d="M16 16l-3.87-3.87" />
                    </g>
                  </svg>
                }
              />
              <div className="d-flex">
                <SortBy
                  className="container-option"
                  defaultRefinement="User"
                  items={[
                    {
                      label: intl.formatMessage({ id: "general.filter.pop" }),
                      value: "User",
                    },
										{
											label: intl.formatMessage({ id: "general.filter.people.date2" }),
											value: "User_id_des",
										},
                    {
                      label: intl.formatMessage({ id: "general.filter.people.date1" }),
                      value: "User_id_asc",
                    },
                    {
                      label: intl.formatMessage({ id: "general.filter.people.alpha1" }),
                      value: "User_fname_asc",
                    },
                    {
                      label: intl.formatMessage({ id: "general.filter.people.alpha2" }),
                      value: "User_fname_desc",
                    },
                  ]}
                />

                <HitsPerPage
                  className="container-option"
                  items={[
                    {
                      label: `16 ${intl.formatMessage({ id: "users.hits" })}`,
                      value: 16,
                    },
                    {
                      label: `32 ${intl.formatMessage({ id: "users.hits" })}`,
                      value: 32,
                    },
                    {
                      label: `64 ${intl.formatMessage({ id: "users.hits" })}`,
                      value: 64,
                    },
                  ]}
                  defaultRefinement={16}
                />
              </div>
            </header>

            <Hits hitComponent={Hit} />
            <NoResults />

            <footer className="container-footer">
              <Pagination
                padding={2}
                showFirst={false}
                showLast={false}
                translations={{
                  previous: (
                    <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 10 10">
                      <g
                        fill="none"
                        fillRule="evenodd"
                        stroke="#000"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="1.143"
                      >
                        <path d="M9 5H1M5 9L1 5l4-4" />
                      </g>
                    </svg>
                  ),
                  next: (
                    <svg xmlns="http://www.w3.org/2000/svg" width="10" height="10" viewBox="0 0 10 10">
                      <g
                        fill="none"
                        fillRule="evenodd"
                        stroke="#000"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        strokeWidth="1.143"
                      >
                        <path d="M1 5h8M5 9l4-4-4-4" />
                      </g>
                    </svg>
                  ),
                }}
              />
            </footer>
          </section>
        </main>

        <aside data-layout="mobile">
          <button className="filters-button" data-action="open-overlay" onClick={openFilters}>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 14">
              <path
                d="M15 1H1l5.6 6.3v4.37L9.4 13V7.3z"
                stroke="#fff"
                strokeWidth="1.29"
                fill="none"
                fillRule="evenodd"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>
            {intl.formatMessage({ id: "algolia.filters" })}
          </button>
        </aside>
      </InstantSearch>
    </div>
  );
};

export default withURLSync(injectIntl(App));
