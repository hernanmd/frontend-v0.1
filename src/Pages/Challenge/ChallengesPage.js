import React, { Component } from "react";
import Api from "Api";
import ChallengeList from "Components/Challenges/ChallengeList";
import ChallengesHeader from "../../Components/Challenges/ChallengesHeader";
import Filter from "Components/Tools/Filter";
import Loading from "../../Components/Tools/Loading";

export default class ChallengesPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listChallenges: [],
      loading: true,
      listDataFromChild: null,
    };
  }

  myCallback = (dataFromChild) => {
    this.setState({ listDataFromChild: dataFromChild }); // send data to filter component (child)
  };

  componentDidMount() {
    Api.get("/api/challenges")
      .then((res) => {
        var listChallenges = [];
        res.data.map((challenge) => {
          return listChallenges.push(challenge);
        });
        listChallenges.sort((a, b) => b.id - a.id); // sort from newest to oldest
        this.setState({ listChallenges, loading: false });
      })
      .catch((error) => {
        // console.log(error);
        this.setState({ loading: false });
      });
  }

  render() {
    const { listChallenges, loading } = this.state;
    return (
      <div className="container-fluid">
        <ChallengesHeader />
        <div className="row challengeSearch">
          <div className="col-12">
            {/* need to add callback to get data from filter component, but don't fully understand why */}
            <Filter list={listChallenges} callback={this.myCallback} />
          </div>
        </div>
        <Loading active={loading} height="300px">
          <div className="justify-content-center">
            <ChallengeList searchBar={true} listChallenges={listChallenges} />
          </div>
        </Loading>
      </div>
    );
  }
}
