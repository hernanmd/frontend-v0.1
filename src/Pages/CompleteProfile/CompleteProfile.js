import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { FormattedMessage, injectIntl } from "react-intl";
import Slider from "react-slick";
import Api from "Api";
import Loading from "Components/Tools/Loading";
import FormDefaultComponent from "Components/Tools/Forms/FormDefaultComponent";
import FormSkillsComponent from "Components/Tools/Forms/FormSkillsComponent";
import FormInterestsComponent from "Components/Tools/Forms/FormInterestsComponent";
import FormSelectComponent from "Components/Tools/Forms/FormSelectComponent";
import "./CompleteProfile.scss";

class CompleteProfile extends Component {
  constructor(props) {
    super(props);
    var loading = this.props.user.id !== null ? false : true;

    this.state = {
      jobIndustry: "",
      jobFunction: "",
      errors: "",
      loading: loading,
      redirectURL: "",
      activeSlide: 0,
      user: this.props.user,
      userUpdated: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.next = this.next.bind(this);
    /* this.skip = this.skip.bind(this); */
  }

  static get defaultProps() {
    return {
      user: {
        id: null,
        category: "",
        affiliation: "",
        sign_in_count: 0,
        interests: [],
        skills: [],
        short_bio: "",
      },
    };
  }

  componentWillReceiveProps(nextProps) {
    const user = nextProps.user;
    this.setState({ loading: false, user: user });
  }

  componentDidMount() {
    if (!this.defaultSlide()) {
      document.querySelector(".slide2").style.display = "none";
    }
    const userId = this.state.user.id;
    Api.get("/api/users/" + userId)
      .then((res) => {
        this.setState({ user: res.data, userLoaded: true });
      })
      .catch((error) => {
        console.log("ERR : ", error);
      });
  }

  handleChange(key, content) {
    var newUser = this.state.user;
    newUser[key] = content;
    // console.log(newUser);
    this.setState({ user: newUser });
  }

  handleSubmit(event, redirectURL) {
    if (event) {
      event.preventDefault();
    }
    const user = this.state.user;
    Api.patch("/api/users/" + user.id, { user })
      .then((res) => {
        // console.log(res);
        this.setState({ userUpdated: true });
        if (redirectURL) {
          this.setState({ redirectURL });
        }
      })
      .catch((error) => {
        // console.log(error);
      });
  }

  defaultSlide() {
    const user = this.state.user;
    if (user.affiliation && user.skills.length > 0 && user.category && user.short_bio) {
      return 1;
    } else {
      return 0;
    }
  }

  next() {
    this.slider.slickNext();
    document.querySelector(".slide2").style.display = "block";
  }

  /* skip() {
		this.slider.slickGoTo(1)
		document.querySelector('.slide2').style.display = "block"
	}; */

  completeProfile() {
    this.handleSubmit(null, "/user/" + this.state.user.id + "/edit");
  }

  goToProjects() {
    this.handleSubmit(null, "/projects");
  }

  render() {
    // console.log("render");

    var settings = {
      dots: false,
      arrows: false,
      infinite: true,
      initialSlide: this.defaultSlide(),
      fade: true,
      lazyload: true,
      speed: 500,
      swipeToSlide: false,
      draggable: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      afterChange: (current) => this.setState({ activeSlide: current }),
    };
    const user = this.state.user;

    //Disabled button
    // const disabledNext = this.defaultSlide() ? false : false;
    const disabledNext = this.defaultSlide() ? false : true;
    const disabledEnd = user.interests.length > 0 ? false : true;
    // console.log("user.interests : ", user.interests);
    // console.log("DisabledEnd : ", disabledEnd);

    if (this.state.redirectURL) {
      return <Redirect push to={this.state.redirectURL} />;
    }

    const { intl } = this.props;

    return (
      <div className="container-fluid CompleteProfile">
        <div className="align-items-center content">
          <Slider ref={(c) => (this.slider = c)} {...settings}>
            <div className="form-content slide1">
              <div className="form-header">
                <h2 className="form-title" id="signModalLabel">
                  <FormattedMessage id="completeProfile.title" defaultMessage="Welcome aboard" />
                </h2>
                <p>
                  <FormattedMessage
                    id="completeProfile.description"
                    defaultMessage="First, a few ice-breakers questions."
                  />
                </p>
              </div>
              <div className="form-body">
                <Loading active={this.state.loading} height="200px">
                  <form onSubmit={this.handleSubmit}>
                    <FormDefaultComponent
                      id="short_bio"
                      placeholder={intl.formatMessage({ id: "user.profile.bioShort.placeholder" })}
                      title={intl.formatMessage({ id: "user.profile.bioShort" })}
                      content={user.short_bio}
                      onChange={this.handleChange}
                    />
                    <FormSelectComponent
                      id="category"
                      title={intl.formatMessage({ id: "user.profile.category" })}
                      content={user.category}
                      onChange={this.handleChange}
                    />
                    <FormDefaultComponent
                      id="affiliation"
                      title={intl.formatMessage({ id: "user.profile.affiliation" })}
                      content={user.affiliation}
                      placeholder={intl.formatMessage({ id: "user.profile.affiliation.placeholder" })}
                      onChange={this.handleChange}
                    />
                    <FormSkillsComponent
                      id="skills"
                      type="user"
                      title={intl.formatMessage({ id: "user.profile.skills" })}
                      content={user.skills}
                      placeholder={intl.formatMessage({ id: "general.skills.placeholder" })}
                      onChange={this.handleChange}
                    />
                    <div className="buttonsContainer">
                      <button className="btn btn-primary next" onClick={this.next} disabled={disabledNext}>
                        <FormattedMessage id="completeProfile.btn.next" defaultMessage="Next" />
                      </button>
                    </div>
                  </form>
                  {/* <button className="btn btn-light prev" onClick={this.skip}>Skip</button> */}
                </Loading>
              </div>
            </div>

            <div className="form-content slide2">
              <div className="form-header">
                <h2 className="form-title" id="signModalLabel">
                  <FormattedMessage id="completeProfile.title2" defaultMessage="Ready to build a better future?" />
                </h2>
                <p>
                  <FormattedMessage
                    id="completeProfile.description2"
                    defaultMessage="Choose at least 1 on the 17 sustainable development goals defined by the United Nations. This will help us show you projects you’ll love."
                  />
                </p>
              </div>
              <div className="form-body">
                <form onSubmit={this.handleSubmit}>
                  <Loading active={this.state.loading} height="200px">
                    <FormInterestsComponent
                      id="interests"
                      title={intl.formatMessage({ id: "user.profile.interests" })}
                      content={user.interests}
                      showAll={true}
                      onChange={this.handleChange}
                    />
                  </Loading>
                </form>
                <div className="buttonsContainer">
                  <button className="btn btn-light prev" onClick={this.goToProjects.bind(this)} disabled={disabledEnd}>
                    <FormattedMessage id="projects.header.title" defaultMessage="Explore projects" />
                  </button>
                  <button
                    className="btn btn-primary next"
                    onClick={this.completeProfile.bind(this)}
                    disabled={disabledEnd}
                  >
                    <FormattedMessage id="completeProfile.btn.complete" defaultMessage="Complete my profile" />
                  </button>
                </div>
              </div>
            </div>
          </Slider>
        </div>
      </div>
    );
  }
}
export default injectIntl(CompleteProfile);
