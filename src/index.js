import React from "react";
import ReactDOM from "react-dom";
import "./index.scss";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import "bootstrap";
import { IntlProvider } from "react-intl";
import messages_en from "./translations/en";
import messages_fr from "./translations/fr";
import messages_de from "./translations/de";
import HttpsRedirect from "react-https-redirect";

if (!Intl.PluralRules) {
  require("@formatjs/intl-pluralrules/polyfill");
  require("@formatjs/intl-pluralrules/dist/locale-data/en"); // Add locale data for en
  require("@formatjs/intl-pluralrules/dist/locale-data/de"); // Add locale data for de
  require("@formatjs/intl-pluralrules/dist/locale-data/fr"); // Add locale data for fr
}

if (!Intl.RelativeTimeFormat) {
  require("@formatjs/intl-relativetimeformat/polyfill");
  require("@formatjs/intl-relativetimeformat/dist/locale-data/en"); // Add locale data for en
  require("@formatjs/intl-relativetimeformat/dist/locale-data/de"); // Add locale data for de
  require("@formatjs/intl-relativetimeformat/dist/locale-data/fr"); // Add locale data for fr
}

const messages = {
  en: messages_en,
  fr: messages_fr,
  de: messages_de
};
const language = getLanguage();

function getLanguage() {
  let lang;
  if (localStorage.getItem("language") !== null) {
    lang = localStorage.getItem("language");
  } else {
    lang = navigator.language.split(/[-_]/)[0];
  }
  return lang;
}
var element;
if (window.location.hostname !== "beta.jogl.io") {
  // if website is not beta.jogl.io, redirect to https (because beta don't support https)
  element = (
    <HttpsRedirect>
      <IntlProvider locale={language} key={language} messages={messages[language]}>
        <App />
      </IntlProvider>
    </HttpsRedirect>
  );
} else {
  // else don't redirect
  element = (
    <IntlProvider locale={language} key={language} messages={messages[language]}>
      <App />
    </IntlProvider>
  );
}

ReactDOM.render(element, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
