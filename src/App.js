import React, { Component } from "react";
import RouterMain from "./Components/RouterMain";
import UserProvider from "./UserProvider";
import ReactGA from "react-ga";
import TagManager from "react-gtm-module";
import "./App.scss";
import { hotjar } from "react-hotjar";

export default class App extends Component {
  componentDidMount() {
    // Cookie consent options
    var message, dismissText, knowMoreText, cookieLink;
    var userLang = navigator.language || navigator.userLanguage;
    if (
      (localStorage.getItem("language") && localStorage.getItem("language") === "fr") ||
      (!localStorage.getItem("language") && (userLang === "fr-FR" || userLang === "fr"))
    ) {
      message = "En poursuivant votre navigation sur ce site, vous acceptez l’utilisation de cookies";
      dismissText = "Accepter";
      knowMoreText = "En savoir plus";
      cookieLink = "https://jogl.io/terms-fr#cookie-policy";
    } else {
      message = "This website uses cookies to ensure you get the best experience on our website.";
      dismissText = "Got it";
      knowMoreText = "Learn more";
      cookieLink = "https://jogl.io/fr/terms#cookie-policy";
    }

    if (window.cookieconsent) {
      window.cookieconsent.initialise({
        palette: {
          popup: {
            background: "#252e39",
          },
          button: {
            background: "#2A6BFD",
          },
        },
        theme: "classic",
        content: {
          href: cookieLink,
          message: message,
          dismiss: dismissText,
          link: knowMoreText,
        },
      });
    }
    // dynamic og:url
    document.querySelector('meta[property="og:url"]').setAttribute("content", window.location.href);
  }

  render() {
    // google analytics
    if (window.location.hostname !== "app.jogl.io") {
      ReactGA.initialize("UA-133189603-3");
      ReactGA.pageview(window.location.pathname + window.location.search);
      const tagManagerArgs = {
        gtmId: "GTM-KQGQDKV",
      };
      TagManager.initialize(tagManagerArgs);
      hotjar.initialize("1739675");
    }
    if (window.location.hostname !== "beta.jogl.io") {
      hotjar.initialize("1739772");
    }
    if (window.location.hostname === "app.jogl.io") {
      ReactGA.initialize("UA-133189603-4");
      ReactGA.pageview(window.location.pathname + window.location.search);
      const tagManagerArgs = {
        gtmId: "GTM-WS6R9V9",
      };
      TagManager.initialize(tagManagerArgs);
      hotjar.initialize("1739651");
    }
    return (
      <UserProvider>
        <div className="App changeWidth">
          <RouterMain />
        </div>
      </UserProvider>
    );
  }
}
