# Contribute

We welcome contributions !

Here is the philosophy:

1) API first, head to our API documentation for a current view of the API https://documenter.getpostman.com/view/8688524/SWE84xMg?version=latest
2) If what you need is missing, then you'll have to add it to our backend: https://gitlab.com/JOGL/backend-v0.1
3) If you want to add code, fork from the branch `develop` and merge request to the branch `develop` ! Any request to master or staging will be automatically rejected.
4) DRY !
5) Object Oriented thinking, if you think you should make a new component for this, then you should indeed! Modular code is the way forward!

Some technical aspect:
1) The code is supported by React
2) The framework for rendering is Bootstrap 4.0
3) If you need an API to test things you can point to: https://jogl-backend-dev.herokuapp.com
4) If you prefer to work localy, test our dockerstack: https://gitlab.com/JOGL/JOGL

check out CONTRIBUTING guidelines here: https://gitlab.com/JOGL/JOGL/-/blob/master/CONTRIBUTING.md

# Getting started

We highly recommend you setup a SSH key with your GitLab account (https://docs.gitlab.com/ee/ssh/), but this is up to you.

Clone the repository
SSH
`git clone git@gitlab.com:JOGL/frontend-v0.1.git`

HTTPS
`git clone https://gitlab.com/JOGL/frontend-v0.1.git`

Navigate to the repository

`cd frontend-v0.1`

Make sure you are onto the develop branch

`git checkout develop`

Create a new branch for your feature or bug correction

`git branch MyNewBranch` <- Change MyNewBranch to some sensible name

When you finished

# Running the stack

You will need npm, follow the instalation guide for your computer (https://www.npmjs.com/get-npm)

at the root first `npm install` to install all required libraries followed by `npm run start` to start the app locally.

## Available Scripts

In the project directory, you can run:

### `npm run test`

Not working for now, if you know how to write React Tests please help!

### `npm run start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

# Some more info

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
